<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>

<div class="top-links">
        <div class="container">
          	<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","breadcrumb",Array(
			        "START_FROM" => "0",
			        "PATH" => "",
			        "SITE_ID" => "s1"
			    )
			);?>
        </div>
      </div>

      <section id="contacts">
        <div class="title-wrap">
          <h1>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/include/contacts/title.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>

          </h1>
          <p>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/include/contacts/text.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>
          </p>
        </div>

        <div class="container">
          <div class="map row">

            <div class="map-left">
              	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/include/contacts/contacts.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          	);?>
            </div>

            <div class="map-right">
              <div id="map-wrap">


              </div>
            </div>

          </div>
        </section>

        <section id="contacts-form">
            <div class="container">
                <div class="vacancy-form">
                    <?$APPLICATION->IncludeComponent("bitrix:form.result.new","feedback",Array(
                        "IS_OK" => $_REQUEST['formresult']=='addok',
                        "SEF_MODE" => "Y",
                        "WEB_FORM_ID" => '4',
                        "EDIT_URL" => $APPLICATION->GetCurPage(),
                        "LIST_URL" => "",
                        "SUCCESS_URL" => "",
                        "CHAIN_ITEM_TEXT" => "",
                        "CHAIN_ITEM_LINK" => "",
                        "IGNORE_CUSTOM_TEMPLATE" => "Y",
                        "USE_EXTENDED_ERRORS" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "SEF_FOLDER" => "/",
                        "VARIABLE_ALIASES" => Array()
                        )
                    );?>

                </div>
            </div>
        </section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
