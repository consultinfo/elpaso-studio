<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказы");
?>

<?global $USER;
$USER_ID = CUser::GetID(); // Получаем ID текущего пользователя
$getGroup = CUser::GetUserGroupList($USER_ID);
while ($arGroup = $getGroup->Fetch()){
	$group[]=$arGroup["GROUP_ID"];
}
asort($group);
$group_last = array_pop($group);
?>

<?
CModule::IncludeModule('iblock');
$rsGROUPBANNER = CIBlockElement::GetList (
   Array("SORT"=>"ASC"), // сортировка
   Array("IBLOCK_ID" => 25, "SORT" => $group_last), //ID инфоблока
   false,
   false,
   Array("IBLOCK_ID", "ID", "SORT", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL")  //список полей. IBLOCK_ID и ID - обязательны.
);
?>
<?while($banners = $rsGROUPBANNER-> GetNext()):?>
	<?
		$img=CFile::GetPath($banners["PREVIEW_PICTURE"]);
		$text = $banners["PREVIEW_TEXT"];
	?>
<?endwhile;?>

<section id="profile">

        <div class="profile-wrap" style="background-image: url('<?=$img;?>');">

          <div class="container">
            <div class="top-links">
              <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","breadcrumb",Array(
			        "START_FROM" => "0",
			        "PATH" => "",
			        "SITE_ID" => "s1"
			    )
			);?>
            </div>
          </div>

          <h1>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/en/include/profile/title.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>
          </h1>
          <p>
          	<?=$text;?>
          </p>
        </div>

        <div class="container">

          <div class="menu-left">

<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"profile",
	array(
		"ROOT_MENU_TYPE" => "profile",
		"MENU_CACHE_TYPE" => "Y",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "profile",
		"DELAY" => "N"
	),
	false
);?>

          </div>
          <div class="right-block">

            <h1>
            	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
            		"AREA_FILE_SHOW" => "file",
            		"PATH" => "/en/include/profile/order_title.php",
            		"EDIT_TEMPLATE" => ""
            		),
            		false
            	);?>
            </h1>



			<div class="compare-table myorders-table">
              <table>

                <tr>
                  <td>Статус</td>
                  <td>№ заказа</td>
                  <td>Дата</td>
                  <td>Доставка</td>
                  <td>Сумма заказа</td>
                </tr>

<?

$arFilter = Array(
   "USER_ID" => $USER_ID
   );
$db_sales = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter);
while ($ar_sales = $db_sales->Fetch()):?>

<?$arStatus = CSaleStatus::GetByID($ar_sales["STATUS_ID"]);?>
<tr>
  <td><?=$arStatus["NAME"];?></td>
  <td><?=$ar_sales["ID"];?></td>
  <td><?=$ar_sales["DATE_INSERT"];?></td>
  <td><?if($ar_sales["RICE_DELIVERY"]):?><?=$ar_sales["RICE_DELIVERY"];?> р<?else:?> 0 р<?endif;?></td>
  <td><?=$ar_sales["PRICE"];?> р</td>
</tr>
<?endwhile;?>


              </table>
            </div>

          </div>

        </div>
      </section>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
