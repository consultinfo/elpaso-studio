<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Этапы работы");
?>

<section id="work-steps">

        <div class="work-steps-wrap">
<div class="container">
<div class="top-links">

			<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","breadcrumb",Array(
			        "START_FROM" => "0",
			        "PATH" => "",
			        "SITE_ID" => "s1"
			    )
			);?>
</div>
</div>

          <h1>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/eng/include/step/title.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>
          </h1>
          <p>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/eng/include/step/text.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>
          </p>

        </div>

        <div class="steps-top-wrap">

          <div class="steps-top">

            <h3>
            	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/eng/include/step/top_header.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          	);?>
            </h3>
            <h2>
            	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/eng/include/step/top_title.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          	);?>
            </h2>
            <p>
            	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/eng/include/step/top_text.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          	);?>
            </p>

          </div>




<div class="steps">
   <?
   CModule::IncludeModule('iblock');
   $rsSTEP = CIBlockElement::GetList (
      Array("SORT"=>"ASC"), // сортировка
      Array("IBLOCK_ID" => 33), //ID инфоблока
      false,
      false,
      Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "SORT", "CODE")  //список полей. IBLOCK_ID и ID - обязательны.
   );
   $page = $APPLICATION->GetCurDir();
   //preg_match("([a-zA-Z]+)", $page, $page_type);
   $page_type = explode("/", $page);
   ?>
   <?$k="1";?>

   <?while($step = $rsSTEP-> GetNext()):?>
			   	<?if($k != "1"):?>
			   	<hr>
			   	<?endif;?>
				<div class="step <?if($page_type["2"]==$step["CODE"]):?>active<?endif;?>">
	              <a href="<?=$step["DETAIL_PAGE_URL"];?>">
	                <div class="step-number"><strong><?=$step["SORT"]?></strong></div>
	                <h2><?=$step["NAME"];?></h2>
	              </a>
	            </div>
	    		<?$k++;?>
   <?endwhile;?>

</div>

</div>


<div class="container">
    <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/en_step3.php"
	)
);?>
</div>
      </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
