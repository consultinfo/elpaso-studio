<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Contacts");
?>

<div class="top-links">
        <div class="container">
          	<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","breadcrumb",Array(
			        "START_FROM" => "0",
			        "PATH" => "",
			        "SITE_ID" => "s1"
			    )
			);?>
        </div>
      </div>

      <section id="contacts">
        <div class="title-wrap">
          <h1>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/eng/include/contacts/title.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>

          </h1>
          <p>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/eng/include/contacts/text.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>
          </p>
        </div>

        <div class="container">
          <div class="map row">

            <div class="map-left">
              	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/eng/include/contacts/contacts.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          	);?>
            </div>

            <div class="map-right">
              <div id="map-wrap">


              </div>
            </div>

          </div>
        </section>

        <section id="contacts-form">
          <div class="container">
 <div class="vacancy-form">
			<?$APPLICATION->IncludeComponent("bitrix:form.result.new","feedback",Array(
				  "IS_OK" => $_REQUEST['formresult']=='addok',
	              "SEF_MODE" => "Y",
	              "WEB_FORM_ID" => '4',
	              "EDIT_URL" => $APPLICATION->GetCurPage(),
	              "LIST_URL" => "",
	              "SUCCESS_URL" => "",
	              "CHAIN_ITEM_TEXT" => "",
	              "CHAIN_ITEM_LINK" => "",
	              "IGNORE_CUSTOM_TEMPLATE" => "Y",
	              "USE_EXTENDED_ERRORS" => "Y",
	              "CACHE_TYPE" => "A",
	              "CACHE_TIME" => "3600",
	              "SEF_FOLDER" => "/",
	              "VARIABLE_ALIASES" => Array()
	              )
	          );?>
            <!--<form>
              <input type="text" required placeholder="Представьтесь">
              <input type="text" required placeholder="+7 (___) - ___ - __ - __">
              <input type="text" required placeholder="Email для ответа">
              <select>
                <option>Выберите тему сообщения</option>
                <option>1</option>
                <option>1</option>
                <option>1</option>
              </select>
              <textarea placeholder="Ваше сообщение"></textarea>
              <button class="btn-vacancy-form"><img src="img/send.png" alt="">Отправить</button>
              <p>Заполните форму на сайте или напишите нам на <a href="#">work@elpaso.com</a> или позвоните по номеру  <span>8&nbsp;800&nbsp;233&nbsp;21&nbsp;11</span></p>
            </form>-->

          </div></div>
        </section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
