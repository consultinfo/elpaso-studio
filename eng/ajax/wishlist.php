<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
	CModule::IncludeModule('iblock');
	$USER_ID = CUser::GetID(); // Получаем ID текущего пользователя
	$rsUser = CUser::GetByID($USER_ID); //Вызываем пользователя по его ID
	$arUser = $rsUser->Fetch(); //Превращаем в массив его значения
	$user = new CUser;

    // если форма без ошибок
    if($_POST["type"] == "1"){          
		//Активен (УДАЛЯЕМ)
		
		foreach($arUser["UF_WISHLIST"] as $key => $value) {
		    if ($_POST["id"] == $value) {
		        unset($arUser["UF_WISHLIST"][$key]);
		    }
		}
		
		$user->Update($USER_ID, array("UF_WISHLIST"=>$arUser["UF_WISHLIST"]));
		
		$msg_box = "Удалено";
		
    }else{
        //Не активен (ДОБАВЛЯЕМ)
        $arUser["UF_WISHLIST"][] = $_POST["id"];

        $user->Update($USER_ID, array("UF_WISHLIST"=>$arUser["UF_WISHLIST"]));
		
		$msg_box = "Добавлен";
    }
 

    // делаем ответ на клиентскую часть в формате JSON
    echo json_encode(array(
        'result' => $msg_box
    ));
 
?>
<?
// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>