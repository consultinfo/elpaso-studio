<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
    $msg_box = ""; // в этой переменной будем хранить сообщения формы
    $errors = array(); // контейнер для ошибок
    // проверяем корректность полей
    if($_POST['name'] == "")    $errors[] = "Поле 'Ваше имя' не заполнено!";
    if($_POST['phone'] == "")   $errors[] = "Поле 'Телефон' не заполнено!";
 
    // если форма без ошибок
    if(empty($errors)){          

		// массив значений ответов
		$arValues = array (
		    "form_text_5"  => $_POST['name'],    
		    "form_text_6"  => $_POST['phone']     
		);	
		
		
		CModule::IncludeModule("form");		
		if (CFormResult::Add("2", $arValues)){
		    $msg_box =  "Сообщение отправлено.";
		}else{
		    global $strError;
		    $msg_box = $strError;
		}	
		
		
		
    }else{
        // если были ошибки, то выводим их
        $msg_box = "";
        foreach($errors as $one_error){
            $msg_box .= "<span style='color: red;'>".$one_error."</span><br/>";
        }
    }
 
 




 
    // делаем ответ на клиентскую часть в формате JSON
    echo json_encode(array(
        'result' => $msg_box
    ));
     
    
     
?>


<?
// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>