<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О магазине");
?>
<section id="about">

        <div class="about-wrap">

          	<div class="top-links">
        			<div class="container">
						<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","breadcrumb",Array(
						        "START_FROM" => "0",
						        "PATH" => "",
						        "SITE_ID" => "s1"
						    )
						);?>
			        </div>
			</div>

          <h1>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/en/include/about/title.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>
          </h1>
          <p>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/en/include/about/text.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>
          </p>

        </div>

        <div class="container">
          <div class="about-descr">
	             <h2>
		          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
		          		"AREA_FILE_SHOW" => "file",
		          		"PATH" => "/en/include/about/descr_title.php",
		          		"EDIT_TEMPLATE" => ""
		          		),
		          		false
		          	);?>
		          </h2>
		          <p>
		          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
		          		"AREA_FILE_SHOW" => "file",
		          		"PATH" => "/en/include/about/descr_text.php",
		          		"EDIT_TEMPLATE" => ""
		          		),
		          		false
		          	);?>
		          </p>
          </div>

          <div class="about-img-wrap">

	          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/en/include/about/descr_photos.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          	);?>

          </div>

          <h3>
	          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/en/include/about/descr_info.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          	);?>
          </h3>

        </div>
      </section>

      <section id="mission">
        <div class="container">
          <div class="row">

            <div class="mission-left">

              <h2>
              	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/en/include/about/mission_left_header.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          	);?>
              </h2>
              <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/en/include/about/mission_left_photo.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          );?>
              <h3>
              	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/en/include/about/mission_left_title.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          	);?>
              </h3>
              <p>
              	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/en/include/about/mission_left_text.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          	);?>
              </p>

            </div>

            <div class="mission-right">

               <h2>
              	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/en/include/about/mission_right_header.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          	);?>
              </h2>
              <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/en/include/about/mission_right_photo.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          );?>
              <h3>
              	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/en/include/about/mission_right_title.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          	);?>
              </h3>
              <p>
              	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	          		"AREA_FILE_SHOW" => "file",
	          		"PATH" => "/en/include/about/mission_right_text.php",
	          		"EDIT_TEMPLATE" => ""
	          		),
	          		false
	          	);?>
              </p>
            </div>

          </div>
        </div>
      </section>

      <section id="service">
        <div class="container">

          		<h2>
		          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
		          		"AREA_FILE_SHOW" => "file",
		          		"PATH" => "/en/include/about/service_title.php",
		          		"EDIT_TEMPLATE" => ""
		          		),
		          		false
		          	);?>
		          </h2>
		          <p>
		          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
		          		"AREA_FILE_SHOW" => "file",
		          		"PATH" => "/en/include/about/service_text.php",
		          		"EDIT_TEMPLATE" => ""
		          		),
		          		false
		          	);?>
		          </p>

          <div class="service-block-wrap row">



			<?
			CModule::IncludeModule('iblock');
			$rsSTEP = CIBlockElement::GetList (
			   Array("SORT"=>"ASC"), // сортировка
			   Array("IBLOCK_ID" => 35), //ID инфоблока
			   false,
			   Array ("nTopCount" => 6), // количество
			   Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL")  //список полей. IBLOCK_ID и ID - обязательны.
			);
			?>
			<?$k="1";?>
			<?while($all = $rsSTEP-> GetNext()):?>
				 	<div class="service-block">
		              <img src="<?=CFile::GetPath($all["PREVIEW_PICTURE"]);?>" alt="">
		              <div class="service-block-descr">
		                <p class="number">0<?=$k;?></p>
		                <h3><?=$all["NAME"];?></h3>
		                <p class="descr"><?=$all["PREVIEW_TEXT"];?></p>
		              </div>
		            </div>
		    <?$k++;?>
			<?endwhile;?>




          </div>

          <div class="owl-service">

            <?
			CModule::IncludeModule('iblock');
			$rsSTEP = CIBlockElement::GetList (
			   Array("SORT"=>"ASC"), // сортировка
			   Array("IBLOCK_ID" => 35), //ID инфоблока
			   false,
			   Array ("nTopCount" => 6), // количество
			   Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL")  //список полей. IBLOCK_ID и ID - обязательны.
			);
			?>
			<?$k="1";?>
			<?while($all = $rsSTEP-> GetNext()):?>
				 	<div class="service-block">
		              <img src="<?=CFile::GetPath($all["PREVIEW_PICTURE"]);?>" alt="">
		              <div class="service-block-descr">
		                <p class="number">0<?=$k;?></p>
		                <h3><?=$all["NAME"];?></h3>
		                <p class="descr"><?=$all["PREVIEW_TEXT"];?></p>
		              </div>
		            </div>
		    <?$k++;?>
			<?endwhile;?>

          </div>

        </div>
      </section>

      <section id="about-comments">
        <div class="container">


			<?
			CModule::IncludeModule('iblock');
			$rsREWRITE = CIBlockElement::GetList (
			   Array("SORT"=>"ASC"), // сортировка
			   Array("IBLOCK_ID" => 34), //ID инфоблока
			   false,
			   Array ("nTopCount" => 3), // количество
			   Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT" , "DETAIL_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL")  //список полей. IBLOCK_ID и ID - обязательны.
			);
			?>
			<?while($rew = $rsREWRITE-> GetNext()):?>
					 <div class="about-comments-block">
			            <img src="<?=CFile::GetPath($rew["PREVIEW_PICTURE"]);?>" alt="">
			            <h2><?=$rew["NAME"];?><br><span><?=$rew["PREVIEW_TEXT"];?></span></h2>
			            <p><?=$rew["DETAIL_TEXT"];?></p>
			            <img class="left-quote" src="<?=SITE_TEMPLATE_PATH;?>/img/left-quote.png" alt="">
			            <img class="right-quote" src="<?=SITE_TEMPLATE_PATH;?>/img/right-quote.png" alt="">
			          </div>
			<?endwhile;?>


        </div>
      </section>

      <section id="about-also">
        <div class="container">

          <h2>
          			<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
		          		"AREA_FILE_SHOW" => "file",
		          		"PATH" => "/en/include/more.php",
		          		"EDIT_TEMPLATE" => ""
		          		),
		          		false
		          	);?>
          </h2>

			<?$APPLICATION->IncludeComponent("bitrix:menu", "more", Array(
					"ROOT_MENU_TYPE" => "more",	// Тип меню для первого уровня
					"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
					"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
					"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
					"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
					"MAX_LEVEL" => "1",	// Уровень вложенности меню
					"CHILD_MENU_TYPE" => "more",	// Тип меню для остальных уровней
					"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
					"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
				),
				false
			);?>


        </div>
      </section>

      <section id="social">
        <div class="container">

          <h2>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/en/include/social_title.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>
          </h2>

			<?$APPLICATION->IncludeComponent("bitrix:menu", "social", Array(
				"ROOT_MENU_TYPE" => "social",	// Тип меню для первого уровня
					"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
					"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
					"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
					"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
					"MAX_LEVEL" => "1",	// Уровень вложенности меню
					"CHILD_MENU_TYPE" => "social",	// Тип меню для остальных уровней
					"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
					"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
				),
				false
			);?>




        </div>
      </section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
