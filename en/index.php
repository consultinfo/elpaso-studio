<?
$page_index = "Y";
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Дизайн студия Эльпасо");
?>
<section id="banner" class="big-banner">
    <div class="container">
        <div class="row">

            <div class="banner-left">
                <div class="owl-banner">
                  <?
                  CModule::IncludeModule('iblock');

                  $arSelect = Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
                  $arFilter = Array("IBLOCK_ID" => 26, "SECTION_ID" => 85, "ACTIVE"=>"Y");
                  $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>15), $arSelect);

                  while($ob = $res->GetNextElement()):
                      $arFields = $ob->GetFields();
                      $arProps = $ob->GetProperties();
                      ?>

                      <div class="item" style="position: relative;">
                          <a href="<?=$arProps["LINK"]["VALUE"]?>">
                              <div class="text"><?=$arFields["PREVIEW_TEXT"];?></div>
                              <img src="<?=CFile::GetPath($arFields["PREVIEW_PICTURE"]);?>" alt="">
                          </a>
                      </div>

                      <?endwhile;?>
                </div>
            </div>

            <!--<div class="banner-right">
            	<?/*
				CModule::IncludeModule('iblock');
				$rsBANNERS = CIBlockElement::GetList (
				   Array("SORT"=>"ASC"), // сортировка
				   Array("IBLOCK_ID" => 26, "SECTION_ID" => 87), //ID инфоблока
				   false,
				   Array ("nTopCount" => 2), // количество
				   Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_LINK")  //список полей. IBLOCK_ID и ID - обязательны.
				);
				*/?>

				<?/*while($banners = $rsBANNERS-> GetNext()):*/?>

	                <a href="<?/*=$banners["PROPERTY_LINK_VALUE"];*/?>"><div class="banner-right-block row" style="background-image: url(<?/*=CFile::GetPath($banners["PREVIEW_PICTURE"]);*/?>);">
		                <div class="black-bg"></div>
		                <h2><?/*=$banners["NAME"];*/?></h2>
		                <p><?/*=$banners["PREVIEW_TEXT"];*/?></p>
		            </div></a>

				<?/*endwhile;*/?>

            </div>-->

        </div>
    </div>
</section>

<section id="portfolio" class="portfolio-index">
<div class="container">
  <h3>
    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/en/include/main/portfolio_title.php",
        "EDIT_TEMPLATE" => ""
        ),
        false
    );?>
  </h3>
  <h2>
    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/en/include/main/portfolio_mintitle.php",
        "EDIT_TEMPLATE" => ""
        ),
        false
    );?>
  </h2>
  <p>
    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/en/include/main/portfolio_text.php",
        "EDIT_TEMPLATE" => ""
        ),
        false
    );?>
  </p>
</div>
<?$APPLICATION->IncludeComponent(
    "addeo:list",
    ".default",
    array(
        "COUNT" => "6",
        "IBLOCK_ID" => "24",
        "IBLOCK_TYPE" => "content",
        "SECTIONS" => array(
        ),
        "COMPONENT_TEMPLATE" => ".default"
    ),
    false
);?>
<div class="container">
    <a href="/portfolio/">
        <button class="btn-portfolio">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/en/include/main/portfolio_more.php",
                "EDIT_TEMPLATE" => ""
                ),
                false
            );?>
            <img src="<?=SITE_TEMPLATE_PATH;?>/img/arrow-portfolio-right.png" alt="">
        </button>
    </a>
</div>
</section>

<section id="steps">
  <div class="owl-steps">
    <?
    CModule::IncludeModule('iblock');
    $rsSTEPS = CIBlockElement::GetList (
       Array("SORT"=>"ASC"), // сортировка
       Array("IBLOCK_ID" => 26, "SECTION_ID" => 86), //ID инфоблока
       false,
       Array ("nTopCount" => 5), // количество
       Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_LINK")  //список полей. IBLOCK_ID и ID - обязательны.
    );
    $k="1";
    ?>

    <?while($step = $rsSTEPS-> GetNext()):?>
        <?
        $URL[] = $k;
        ?>
        <div class="item" data-hash="step-<?=$k;?>" style="background-image: url(<?=CFile::GetPath($step["PREVIEW_PICTURE"]);?>);">
          <div class="container">
            <p class="steps-more">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => "/en/include/main/step_url.php",
                    "EDIT_TEMPLATE" => ""
                    ),
                    false
                );?>
            </p>
            <h2><?=$step["NAME"]?></h2>
            <p><?=$step["PREVIEW_TEXT"]?></p>
          </div>
        </div>
        <?$k++;?>
    <?endwhile;?>

  </div>

  <div class="steps-dots">
    <?$count = count($URL);?>
    <?foreach($URL as $url): ?>
      <?if($url < $count):?>
          <a <?if($url==1):?>class="active"<?endif;?> href="#step-<?=$url;?>"><div class="step"><?=$url;?></div></a>
          <div class="steps-line"></div>
      <?else:?>
          <a href="#step-<?=$url;?>"><div class="step"><?=$url;?></div></a>
      <?endif;?>
    <?endforeach;?>
  </div>

  <div class="steps-bottom"></div>
</section>

<section id="hits">
  <div class="container">
    <h2>Hits</h2>
    <p>Most popular goods</p>

    <div class="owl-hits">

      <?
      CModule::IncludeModule('iblock');
      $rsHIT = CIBlockElement::GetList (
         Array("SORT"=>"ASC"), // сортировка
         Array("IBLOCK_ID" => 22, "PROPERTY_HIT_VALUE" => "Да"), //ID инфоблока
         false,
         Array ("nTopCount" => 15), // количество
         Array("IBLOCK_ID", "ID", "IBLOCK_SECTION_ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_HIT")  //список полей. IBLOCK_ID и ID - обязательны.
      );
      ?>
      <?while($arProduct = $rsHIT-> GetNext()):?>
          <div class="item">
              <a href="<?=$arProduct["DETAIL_PAGE_URL"]?>">
                  <div class="look-book-block">
                    <!--<img class="look-book-hover" src="img/trend-like.png" alt="">-->
                    <div class="bg-img-div" style="background-image: url(<?=CFile::GetPath($arProduct["PREVIEW_PICTURE"]);?>);"></div>
                    <h3><?=$arProduct["NAME"]?></h3>
                    <?$get_SECT = CIBlockSection::GetByID($arProduct["IBLOCK_SECTION_ID"]);
                    $obRes1 = $get_SECT->GetNext();
                    $arProduct["SECTION"] = $obRes1["NAME"];?>
                    <p><?=$arProduct["SECTION"]?></p>
                    <?
                    /*$Price = CPrice::GetBasePrice($arProduct["ID"]);
                    $arProduct["PRICE"] = $Price["PRICE"];*/
                    $Price = GetCatalogProductPrice($arProduct["ID"], 1);
                    $arProduct["PRICE"] = CCurrencyRates::ConvertCurrency($Price["PRICE"], "EUR", "RUB");
                    ?>
                    <h4><?=ceil($arProduct["PRICE"]);?> <span>Р</span></h4>
                  </div>
              </a>
            </div>
      <?endwhile;?>



    </div>

  </div>
</section>

<section id="trend">
  <div class="container">

    <h2>Trands in 2017</h2>

    <div class="row">


    <?
    CModule::IncludeModule('iblock');
    $rsTREND = CIBlockElement::GetList (
       Array("SORT"=>"ASC", "ID"=>"ASC"), // сортировка
       Array("IBLOCK_ID" => 22, "PROPERTY_TRAND_VALUE" => "Да"), //ID инфоблока
       false,
       Array ("nTopCount" => 10), // количество
       Array("IBLOCK_ID", "ID", "IBLOCK_SECTION_ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_TRAND")  //список полей. IBLOCK_ID и ID - обязательны.
    );
    $k="1";
    ?>
    <?while($trend = $rsTREND-> GetNext()):?>

    <?
    if(($k == "1") || ($k == "6")){
        $big = "yes";
        $t = CFile::ResizeImageGet($trend["PREVIEW_PICTURE"], array("width" => "600", "height" => "300"), BX_RESIZE_IMAGE_EXACT);
        $picture = $t["src"];
    }else{
        $big = "no";
        $picture = CFile::GetPath($trend["PREVIEW_PICTURE"]);
    }
    ?>
        <a href="<?=$trend["DETAIL_PAGE_URL"]?>"><div class="trend-block <?if($big == "yes"):?>trend-block-big<?endif;?>">
            <div class="bg-img-div" style="background-image: url(<?=$picture;?>);"></div>
            <h3><?=$trend["NAME"]?></h3>
            <?$get_SECT = CIBlockSection::GetByID($trend["IBLOCK_SECTION_ID"]);
            $obRes1 = $get_SECT->GetNext(); ?>
            <p><?=$obRes1["NAME"];?></p>
            <?
            $Price = CPrice::GetBasePrice($trend["ID"]);
            /*$Price = GetCatalogProductPrice($trend["ID"], 1);*/
            $arProduct["PRICE"] = CCurrencyRates::ConvertCurrency($Price["PRICE"], "EUR", "RUB");

            ?>
            <h4><?=ceil($arProduct["PRICE"]);?> <span>Р</span></h4>
        </div></a>
        <?$k++;?>
    <?endwhile;?>


    </div>

    <div class="row">
      <a href="/en/catalog/"><button class="btn-trend">
        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => "/en/include/main/trend_more.php",
            "EDIT_TEMPLATE" => ""
            ),
            false
        );?>
        <img src="<?=SITE_TEMPLATE_PATH;?>/img/arrow-btn-trend.png" alt="">
          <span class="color_white">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/en/include/main/trend_info.php",
                "EDIT_TEMPLATE" => ""
                ),
                false
            );?>
          </span>
      </button></a>
    </div>

  </div>
</section>

<section id="top">
  <div class="container">
    <h2>
        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => "/en/include/main/top_title.php",
            "EDIT_TEMPLATE" => ""
            ),
            false
        );?>
    </h2>

    <div class="row">
      <div class="top-left">
        <div class="row">
          <div class="ltl">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/en/include/main/top_1.php",
                "EDIT_TEMPLATE" => ""
                ),
                false
            );?>
          </div>
          <div class="ltr">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/en/include/main/top_2.php",
                "EDIT_TEMPLATE" => ""
                ),
                false
            );?>
          </div>
        </div>
        <div class="lb">
          <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/en/include/main/top_3.php",
                "EDIT_TEMPLATE" => ""
                ),
                false
            );?>
        </div>
      </div>

      <div class="top-right">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/en/include/main/top_4.php",
                "EDIT_TEMPLATE" => ""
                ),
                false
            );?>
      </div>
    </div>

  </div>

</section>

<section id="news">
    <div class="container">
        <div class="news-title row">
            <h2>
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/en/include/main/news_title.php"
                    )
                );?>
            </h2>
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => "/en/include/main/news_all.php"
                )
            );?>
        </div>
        <div class="owl-news">

            <?
            CModule::IncludeModule('iblock');
            $rsNEWS = CIBlockElement::GetList (
                Array("SORT"=>"ASC", "ID"=>"DESK"), // сортировка
                Array("IBLOCK_ID" => 30), //ID инфоблока
                false,
                Array ("nTopCount" => 10), // количество
                Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "PROPERTY_LINK", "PROPERTY_TYPE")  //список полей. IBLOCK_ID и ID - обязательны.
            );
            ?>
            <?while($news = $rsNEWS-> GetNext()):?>
                <div class="item">
                    <a href="<?=$news["PROPERTY_LINK_VALUE"];?>">
                        <img src="<?=CFile::GetPath($news["PREVIEW_PICTURE"]);?>" alt="">
                        <div class="owl-news-txt">
                            <h3><?=$news["NAME"];?></h3>
                            <p><?=$news["PROPERTY_TYPE_VALUE"];?></p>
                        </div>
                    </a>
                </div>
            <?endwhile;?>

        </div>

        <div class="news-bottom">
            <div class="mobile-a">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => "/en/include/main/news_all.php",
                    "EDIT_TEMPLATE" => ""
                ),
                    false
                );?>

            </div>
            <img src="<?=SITE_TEMPLATE_PATH;?>/img/fb-news.png" alt="">
            <p>
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => "/en/include/main/news_text.php",
                    "EDIT_TEMPLATE" => ""
                ),
                    false
                );?>
            </p>
        </div>

    </div>
</section>

<section id="social">
    <div class="container">

      <h2>
        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => "/en/include/social_title.php",
            "EDIT_TEMPLATE" => ""
            ),
            false
        );?>
      </h2>

        <?$APPLICATION->IncludeComponent("bitrix:menu", "social", Array(
            "ROOT_MENU_TYPE" => "social",	// Тип меню для первого уровня
                "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
                "MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                "CHILD_MENU_TYPE" => "social",	// Тип меню для остальных уровней
                "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
            ),
            false
        );?>




    </div>
  </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
