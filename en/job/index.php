<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Вакансии");
?>

<div class="top-links">
        <div class="container">
          <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","breadcrumb",Array(
			        "START_FROM" => "0",
			        "PATH" => "",
			        "SITE_ID" => "s1"
			    )
			);?>
        </div>
      </div>

      <section id="vacancies">
        <div class="title-wrap">

          <h1>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/en/include/job/title.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>
          </h1>
          <p>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/en/include/job/text.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>
          </p>
        </div>

        <div class="container">



			<?
			CModule::IncludeModule('iblock');
			$rsJOB = CIBlockElement::GetList (
			   Array("SORT"=>"ASC"), // сортировка
			   Array("IBLOCK_ID" => 37), //ID инфоблока
			   false,
			   false,
			   Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT", "DETAIL_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "DATE_ACTIVE_FROM", "DATE")  //список полей. IBLOCK_ID и ID - обязательны.
			);
			?>
			<?while($job = $rsJOB-> GetNext()):?>
				<div class="vacancy-block">
<?
// FORMAT_DATETIME - константа с форматом времени сайта
$arDate = ParseDateTime($job["DATE_ACTIVE_FROM"], FORMAT_DATETIME);
$min_data = ToLower(GetMessage("MONTH_".intval($arDate["MM"])."_S"));
?>
		            <p class="date">Размещено <?echo $arDate["DD"]." ".$min_data;?></p>
		            <h2><?=$job["NAME"];?></h2>
		            <?=$job["PREVIEW_TEXT"];?>
		            <div class="vacancy-hidden">
		            	<?=$job["DETAIL_TEXT"];?>
		            </div>
		            <button class="btn-vacancy">Подробнее</button>
		          </div>
			<?endwhile;?>







<div class="vacancy-form">
	<?$APPLICATION->IncludeComponent(
		"bitrix:form",
		"",
		Array(
			"AJAX_MODE" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "N",
			"CACHE_TIME" => "3600",
			"CACHE_TYPE" => "N",
			"CHAIN_ITEM_LINK" => "",
			"CHAIN_ITEM_TEXT" => "",
			"EDIT_ADDITIONAL" => "N",
			"EDIT_STATUS" => "Y",
			"IGNORE_CUSTOM_TEMPLATE" => "N",
			"NOT_SHOW_FILTER" => array("", ""),
			"NOT_SHOW_TABLE" => array("", ""),
			"RESULT_ID" => $_REQUEST[RESULT_ID],
			"SEF_MODE" => "N",
			"SHOW_ADDITIONAL" => "N",
			"SHOW_ANSWER_VALUE" => "N",
			"SHOW_EDIT_PAGE" => "N",
			"SHOW_LIST_PAGE" => "N",
			"SHOW_STATUS" => "Y",
			"SHOW_VIEW_PAGE" => "N",
			"START_PAGE" => "new",
			"SUCCESS_URL" => "",
			"USE_EXTENDED_ERRORS" => "N",
			"VARIABLE_ALIASES" => Array(
				"action" => "action"
			),
			"WEB_FORM_ID" => "1"
		)
	);?>
<p style="margin-top:10px;font-size: 15px;font-family: 'AcromLight';color: #7c7c7c;line-height: 22px;">Нажимая кнопку Отпраить, Вы соглашаетесь с <a  target="_blank" href="/include/personal.docx" style="font-size:14px;font-family: 'AcromBold';color: #42304f;">Политикой конфиденциальности</a></p>

</div>






        </div>
      </section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
