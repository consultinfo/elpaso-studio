<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
    $msg_box = ""; // в этой переменной будем хранить сообщения формы
    $errors = array(); // контейнер для ошибок
    // проверяем корректность полей

// Выведем форму для ввода свойств заказа для группы свойств с кодом 5, которые входят в профиль покупателя, для типа плательщика с кодом 2
CModule::IncludeModule('sale');
$db_props = CSaleOrderProps::GetList(
        array("SORT" => "ASC"),
        array(
                "PERSON_TYPE_ID" => 1,
                "USER_PROPS" => "Y"
            ),
        false,
        false,
        array()
    );
while ($props = $db_props->Fetch()){
	if($props["REQUIED"] != "N"){
		if($_POST[$props["CODE"]] == "")    $errors[] = "Поле '".$props["NAME"]."' не заполнено!";
	}
}
    
    
    

    // если форма без ошибок
    if(empty($errors)){
    	
         
		global $USER;
		$USER_ID = CUser::GetID(); // Получаем ID текущего пользователя
		
		if(!$USER_ID){
			$filter = Array
			(
			    "LOGIN"               => $_POST["EMAIL"]
			);
			$rsUsers = CUser::GetList(($by="id"), ($order="desc"), $filter); // выбираем пользователей
			if($rsUsers->NavNext(true, "f_")) {
				$USER_ID = $f_ID;
			}else{
				
				
				$pass = randString(7, array(
				  "abcdefghijklnmopqrstuvwxyz",
				  "ABCDEFGHIJKLNMOPQRSTUVWXYZ",
				  "0123456789",
				));
				$user = new CUser;
				$arFields = Array(
				  "NAME"              => $_POST['FIO'],
				  "EMAIL"             => $_POST['EMAIL'],
				  "LOGIN"             => $_POST['EMAIL'],
				  "ACTIVE"            => "Y",
				  "GROUP_ID"          => array(2,3),
				  "PASSWORD"          => $pass,
				  "CONFIRM_PASSWORD"  => $pass
				);
		
				$USER_ID = $user->Add($arFields);
				$msg_boxs = $USER_ID;
				$USER->Authorize($USER_ID);
				
				CModule::IncludeModule('iblock');
				$new_user = array(
					"EMAIL" => $_POST['EMAIL'],
					"PASS" => $pass
				);
				CEvent::Send("NEW_USER", SITE_ID, $new_user, "N", 78);
				
			}

		}
		
		$rsUser = CUser::GetByID($USER_ID); //Вызываем пользователя по его ID
		$arUser = $rsUser->Fetch();

		$arFields = array(
		   "LID" => "s1",
		   "PERSON_TYPE_ID" => 1,
		   "PAYED" => "N",
		   "CANCELED" => "N",
		   "STATUS_ID" => "N",
		   "USER_ID" => $USER_ID,
		   "PAY_SYSTEM_ID" => $_POST["PAY"],
		   "DELIVERY_ID" => $_POST["DELIVERY"],
		   "PRICE" => 1,
		   "CURRENCY" => "EUR",
		   "PHONE" => $_POST["PHONE"]
		   
		);
		
		CModule::IncludeModule('sale'); 
		$ORDER_ID = CSaleOrder::Add($arFields);
		CSaleBasket::OrderBasket($ORDER_ID, $_SESSION["SALE_USER_ID"], SITE_ID); 
		CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
		$props = array(
			"ORDER_ID" => $ORDER_ID,
		    "ORDER_PROPS_ID" => 2,
		    "NAME" => "E-MAIL",
		    "CODE" => "EMAIL",
		    "VALUE" => $_POST["EMAIL"]
		);
		CSaleOrderPropsValue::Add($props);	
		
		$props = array(
			"ORDER_ID" => $ORDER_ID,
		    "ORDER_PROPS_ID" => 2,
		    "NAME" => "Телефон",
		    "CODE" => "PHONE",
		    "VALUE" => $_POST["PHONE"]
		);
		CSaleOrderPropsValue::Add($props);			
		
		$props = array(
			"ORDER_ID" => $ORDER_ID,
		    "ORDER_PROPS_ID" => 1,
		    "NAME" => "Покупатель",
		    "CODE" => "FIO",
		    "VALUE" => $_POST["FIO"]
		);
		CSaleOrderPropsValue::Add($props);
		$props = array(
			"ORDER_ID" => $ORDER_ID,
		    "ORDER_PROPS_ID" => 7,
		    "NAME" => "Адрес",
		    "CODE" => "ADDRESS",
		    "VALUE" => $_POST["ADDRESS"]
		);
		CSaleOrderPropsValue::Add($props);
		
		
		$arEventFields = array(
		"EMAIL" => $arUser["EMAIL"],
		"ORDER_ID" => $ORDER_ID
		);
		
		CModule::IncludeModule('iblock');
		CEvent::Send("SALE_NEW_ORDER", SITE_ID, $arEventFields, "N", 77);
		CEvent::Send("SALE_NEW_ORDER", SITE_ID, $arEventFields, "N", 76);
		
		
		$msg_box = "<h2>Заказ №".$ORDER_ID." оформлен.</h2>";
		$yes = "1";
			

    }else{
        // если были ошибки, то выводим их
        $msg_box = "";
        foreach($errors as $one_error){
            $msg_box .= "<span style='color: red;'>".$one_error."</span><br/>";
        }
    }
 

 
    // делаем ответ на клиентскую часть в формате JSON
    echo json_encode(array(
        'result' => $msg_box,
        'results' => $msg_boxs,
        'yes' => $yes
    ));
     
    
     
?>


<?
// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>