<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require($_SERVER["DOCUMENT_ROOT"]."/cart/function.php");
$APPLICATION->SetTitle("Оформление заказа");
?>



<?if($arBasketItems):?>
<section id="order">
        <div class="container">



            <div class="top-links">
              <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","breadcrumb",Array(
					        "START_FROM" => "0",
					        "PATH" => "",
					        "SITE_ID" => "s1"
					    )
					);?>
            </div>

            <h1><?$APPLICATION->ShowTitle()?></h1>

			<div class="all_page">
				<div class="order-left">
					<form action="/ajax/order.php" method="post" id="OrderIsSite">
						<div class="profile-form row">
							<div class="error_form"></div>
							<div class="error_forms"></div>
							<?
							// Выведем форму для ввода свойств заказа для группы свойств с кодом 5, которые входят в профиль покупателя, для типа плательщика с кодом 2
							CModule::IncludeModule('sale');
							$db_props = CSaleOrderProps::GetList(
							        array("SORT" => "ASC"),
							        array(
							                "PERSON_TYPE_ID" => 1,
							                "USER_PROPS" => "Y"
							            ),
							        false,
							        false,
							        array()
							    );
							?>
							   <?while ($props = $db_props->Fetch()):?>
							   	<?if($props["TYPE"] == "TEXT"):?>
									<label <?if($props["REQUIED"] == "N"):?>class="not-required"<?endif;?> placeholder="<?=$props["DEFAULT_VALUE"];?>"><?=$props["NAME"];?></label>
							    	<input type="text" name="<?=$props["CODE"];?>" id="<?=$props["CODE"];?>">
								<?elseif($props["TYPE"] == "TEXTAREA"):?>
									<label <?if($props["REQUIED"] == "N"):?>class="not-required"<?endif;?>><?=$props["NAME"];?></label>
							    	<textarea name="<?=$props["CODE"];?>"></textarea>
								<?endif;?>
								<?endwhile;?>

							    <label>Способ оплаты</label>
							    <select id="form_pay" name="PAY">
									<?
									CModule::IncludeModule('sale');
									$db_ptype = CSalePaySystem::GetList(Array("SORT"=>"ASC",), Array("LID"=>SITE_ID, "ACTIVE"=>"Y"));
									while ($ptype = $db_ptype->Fetch()){?>
									   <option value="<?=$ptype["ID"];?>"><?=$ptype["NAME"];?></option>
									<?}?>
								</select>
							    <label>Получение</label>
							    <?
							    $db_d = CSaleDelivery::GetList(
							    array(
							            "SORT" => "ASC"
							        ),
							    array(
							        ),
							    false,
							    false,
							    array()
							);
							$N = "1";
							while ($pty = $db_d->Fetch()){?>
								<input type="radio" name="DELIVERY" value="<?=$pty["ID"];?>" id="radio-<?=$pty["ID"];?>" <?if($N=="1"):?>checked <?endif;?>/>
							    <label for="radio-<?=$pty["ID"];?>"><?=$pty["NAME"];?></label>
							<?
							$N++;
							}?>
						</div>
			           	<div class="order-price row">
				                <h2>Итого: 1 товар на сумму <span><?=$PRICE;?> Р</span></h2>
				                <div class="price-left">
				                  <p>Подытог: <?=$PRICE;?> Р<br><span>Итого: <?=$PRICE;?> Р</span></p>
				                </div>
				                <button type="submit" class="btn-order">Отправить заказ</button>
		                </div>
					</form>
        		</div>



	          <div class="order-right">

		            <div class="order-right-wrap">

		              <?
		              CModule::IncludeModule('iblock');
		              $rsPRIM = CIBlockElement::GetList (
		                 Array("SORT"=>"ASC"), // сортировка
		                 Array("IBLOCK_ID" => 17), //ID инфоблока
		                 false,
		                 Array ("nTopCount" => 5), // количество
		                 Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT". "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL")  //список полей. IBLOCK_ID и ID - обязательны.
		              );
		              ?>
                                <?while($all = $rsPRIM-> GetNext()):?>
		              	<div class="order-right-block row">
			                <img src="<?=CFile::GetPath($all["PREVIEW_PICTURE"]);?>" alt="">
			                <div class="order-right-block-descr">
			                  <h3><?=$all["NAME"]?></h3>
			                  <p><?=$all["PREVIEW_TEXT"]?></p>
			                </div>
			              </div>
		              <?endwhile;?>


		            </div>

		            <div class="owl-order">

		              <?
		              CModule::IncludeModule('iblock');
		              $rsPRIM = CIBlockElement::GetList (
		                 Array("SORT"=>"ASC"), // сортировка
		                 Array("IBLOCK_ID" => 31), //ID инфоблока
		                 false,
		                 Array ("nTopCount" => 5), // количество
		                 Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT". "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL")  //список полей. IBLOCK_ID и ID - обязательны.
		              );
		              ?>
		              <?while($all = $rsPRIM-> GetNext()):?>
		              	<div class="order-right-block row">
			                <img src="<?=CFile::GetPath($all["PREVIEW_PICTURE"]);?>" alt="">
			                <div class="order-right-block-descr">
			                  <h3><?=$all["NAME"]?></h3>
			                  <p><?=$all["PREVIEW_TEXT"]?></p>
			                </div>
			              </div>
		              <?endwhile;?>

		            </div>

	            </div>
          </div>
     </div>
</section>
<?else:?>
<?header("Location: /");?>
<?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
