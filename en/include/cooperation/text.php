<b><span style="font-size: 16pt; color: #450e61;">""Alone we can do so little; together we can do so much."</span></b>
<p>
</p>
<p style="text-align: center;">
 <img width="100%" alt="gori_2.jpg" src="/upload/medialibrary/fec/fecee87c9ab72c6b2484a09bbda4edcd.jpg" style="width:100%; margin:5px 0;" title="К вершинам стиля"> <br>
</p>
<p style="text-align: justify;">
	 To make clear the advantages of cooperating with us we will tell you about our professional experience, the way we think, about our partners whom we appreciate, and are grateful for the results.
</p>
<div>
	<div class="col-md-9 col-sm-6 col-xs-12" style="margin-top:14px;">
		<p style="text-align: center;">
 <b><span style="font-size: 16pt; color: #450e61;">The Elpaso logo is two mountains side by side, &nbsp;</span></b>
		</p>
		<p style="text-align: center;">
 <span style="color: #450e61;"> </span>
		</p>
 <span style="color: #450e61;"> </span>
		<p style="text-align: center;">
 <span style="color: #450e61;"> </span><b><span style="font-size: 16pt; color: #450e61;">
			and our motto is </span></b><span style="font-size: 16pt; color: #450e61;"> </span><span style="color: #450e61;"> </span>
		</p>
 <span style="color: #450e61;"> </span>
		<p style="text-align: center;">
 <span style="color: #450e61;"> </span><span style="font-size: 16pt; color: #450e61;"> </span><span style="color: #450e61;"> </span>
		</p>
 <span style="color: #450e61;"> </span>
		<p style="text-align: center;">
 <span style="color: #450e61;"> </span><span style="font-size: 16pt; color: #450e61;"> </span><b style="text-align: center;"><span style="font-size: 16pt; color: #450e61;">“TO STYLE HEIGHTS!” </span></b>
		</p>
		<p style="text-align: center;">
 <span style="color: #450e61;"> </span><span style="font-size: 16pt; color: #450e61;"> </span><b style="text-align: center;"><span style="font-size: 16pt; color: #450e61;">The main goal of our team is</span></b>
		</p>
		<p style="text-align: center;">
 <b style="text-align: center;"><span style="font-size: 16pt; color: #450e61;">to keep “climbing up’’ in what we do.</span></b>
		</p>
	</div>
 <img width="231" src="/upload/medialibrary/6b9/6b92b41b590f4a9776c334e4438999ab.jpg" class="col-md-3 col-sm-6 col-xs-12" style="display:block; margin:0;" alt="Логотип" title="Логотип">
</div>
<p style="text-align: justify;">
 <b><span style="color: #30004a;">Elpaso is a bridge that unites the interests of our clients in Russia and the CIS countries with international factories. After all, THE BEST VIEWS IS THE ONE FROM THE TOP OF THE MOUNTAIN. </span> </b>
</p>
 <b>
<div class="col-md-3 col-sm-4 col-xs-12" style="margin:5px 0;">
 <img width="100%" alt="Ольга Ефремова" src="/upload/medialibrary/254/254dbd27a54753832ac024c5fe78f53e.jpg" style="display:block; width:100%;" title="Ольга Ефремова">
</div>
<p style="text-align: justify;">
 <span style="color: #4b0048;"> Elpaso’s partners and friends are people with whom we have been through thick and thin; they are wise, talented, successful and most distinguished in their field, we can always count on them. Each business just like all of us has its own destiny. Through the years Elpaso acquired its key resource which is its partners who made crucial influence on our growth.</span>
</p>
 <br>
<div class="col-md-4 col-sm-4 col-xs-12" style="margin: 3px 0 3px 5px; float:right;">
 <img width="100%" alt="sotrudnichestvo_6.jpg" src="/upload/medialibrary/7d8/7d80a3408a99545b828904ffa70c58e4.jpg" title="Эмилио Реверт (Испания)и Ольга Ефремова (Россия)" style="display:block; width:100%;">
</div>
<p style="text-align: justify;">
 <span style="color: #4b0048;"> Since the year of its funding, 2005, each partner is like a planet in our solar system called Elpaso. “Your Sun is Elpaso.” That’s what our first driver used to say; he knew all the movements, meetings, goals and plans of Olga Efremova, our director. And that’s the way it is. We can attract right people only through our love for what we do, only through our actions and emotions that are like rays of that Sun. If we act correctly difficult times for every business become an opportunity to grow, to keep on going, to get stronger. Being a fighter is equally important for a mutually beneficial partnership and safe deals. We talk to our partners in an open and honest way, defending our point of view and our interests from any sort of manipulation and pressure.</span>
</p>
<p style="text-align: justify;">
 <br>
 <span style="color: #4b0048;"> <b> “WE SHOULD REPLACE THE MANIPULATION OF PEOPLE BY ACTIVE AND INTELLIGENT COOPERATION.” </b>We live by these words of Erich Fromm. We don’t let manipulate us. Neither we work with those who do that. </span>
</p>
<p style="text-align: justify;">
 <span style="color: #4b0048;"> When we see someone who comes from another country wanting to buy fast and a lot, open joint enterprises and showrooms at our expense we always consult with our legal team and say, “Either you play fair with Russia or don’t play at all!” We are patriots of our country. We try and find the best partners with the best goods in the world for Russia.</span>
</p>
<p>
</p>
<div class="col-md-12 col-sm-12 col-xs-12" style="margin:5px 0;">
 <img width="100%" alt="sotrudnichestvo_19f.jpg" src="/upload/medialibrary/6da/6dace25beb130742f8024fcbf85aa569.jpg" title="Под флагами Италии и России президент МВМbiliardi и команда Эльпасо на выставке Крокус Экспо 2017" style="display:block; width:100%;">
</div>
<p style="text-align: justify;">
 <span style="color: #4b0048;"> Elpaso learnt to choose, guard and appreciate partner relationship. We always expect it to be mutual. And disappointments don’t stop us.</span>
</p>
<p style="text-align: justify;">
 <span style="color: #4b0048;"> When creating a beautiful interior design it’s crucial to love what you do, be faithful to it no matter how hard it can be. It’s crucial to have a warm personality, to have both taste and intuition. These are important qualities to keep growing, to offer designs, facing, textures, furniture, lighting, beautiful fabrics, i. e., all the best for personal or work space to our clients. Cold people don’t stay too long at Elpaso. Those really passionate about design, order and beauty stay with us.</span>
</p>
<div class="row">
	<div style="margin: 20px 0px;" class="coop-img-1">
 <img alt="sotrudnichestvo_41.jpg" src="/upload/medialibrary/424/424ecaef3ecc081f2c4218457a15e375.jpg" title="Создаем интерьеры с Coleccion Alexandra и Tecni Nova">
	</div>
	<div style="margin: 20px 0px;" class="coop-img-2">
 <img alt="sotrudnichestvo_151.jpg" src="/upload/medialibrary/7e8/7e833dbbcf17e341c4699bda4211413f.jpg" title=" С коллегами в испанском городе Бурриана и Милане">
	</div>
</div>
<p style="text-align: justify;">
 <b><span style="color: #450e61;">Our partners are those who appreciate beautiful strategy and consecutive decisions.</span></b>
</p>
<p style="text-align: justify;">
 <span style="color: #4b0048;"> The first and fundamental ones are the CEO’s of factories and banks located in the capital of the Ural region. In 1999 Yekaterinburg saw the opening of our first showroom called “Delovaya mebel ” which exhibited managers’ offices manufactured by the Spanish factory Ofifran S. L. and the Ural factories “Karat-E” and “Lial”.</span>
</p>
<div class="tab-block" style="margin: 7px 3px;">
 <img width="100%" alt="sotrudnichestvo_111.jpg" src="/upload/medialibrary/83d/83d643e87b381579ea89d4bd10086cec.jpg" style="width:100%;" title="Руководитель отдела снабжения ЦБРФ со специалистами Эльпасо">
</div>
<div class="tab-block" style="margin: 7px 3px;">
 <img width="100%" alt="sotrudnichestvo_113.jpg" src="/upload/medialibrary/53b/53bec4c5ba07e483fabf16cb3236fd9d.jpg" style="width:100%;" title="Уральский губернатор Россель Э.Э. ">
</div>
<div class="tab-block" style="margin: 7px 3px;">
 <img width="100%" alt="sotrudnichestvo_112.jpg" src="/upload/medialibrary/2bb/2bbec747d01fe2e2d7dc1c01ebe717aa.jpg" style="width:100%;" title="Визит представителя ЦБРФ к руководству компаний Эльпасо и Ofifran">
</div>
<p style="text-align: justify;">
 <span style="color: #4b0048;"> The visit to our first Habitat Valencia trade fair in Spain in 2000, at the beginning of the new century, became the main direction for choosing a long term partner. <br>
	 It was there that we met and built a foundation for future partnership with Maria Tovkach, executive officer for supplying internal demands at PJSC Mosenergo’s Moscow region office;<br>
 </span>
</p>
<p>
</p>
<p style="text-align: justify; margin: 10px;">
 <img alt="Реализованные рекламные идеи Ольги Ефремовой" src="/upload/medialibrary/e76/e76dcf22356e451b27908caf1ffa8d5b.jpg" style="width:100%;" title="Реализованные рекламные идеи Ольги Ефремовой (реальные съёмки) для мебельной фабрики Карат Е с командой дрессировщиков Вольтера Запашного и Воробьёвых , 1998">
</p>
<p style="text-align: justify;">
 <span style="color: #4b0048;"> with María José Guinot and her sons, owners of Colección Alexandra; with Juan Bautista Franco Sánchez, owner of Ofifran S. L., and Patricio Toribio Villanueva, head of the Russia’s Ofifran S. L. office. Together with Ofifran S. L. we developed a number of projects in the Ural region and the Yamalo-Nenets autonomous district for JSC Uralelectromed, JSC Uraltransmash, JSC Severouralsk Pipe-Rolling Plant, PSC VSMPO-AVISMA Corporation, PJSC Gazprom Transgaz Yekaterinburg, the Administration and the Department of property of the city of Novy Urengoy, Novy Urengoy’s Recorder of Deeds, the Department of culture and other institutions of Novy Urengoy. Our professional career started off in the Ural Federal District and the very center of the Arctic circle, the city of Salekhard. It toughened us up and gave us some experience so we wouldn’t star our journey in Moscow from scratch.</span>
</p>
<p style="text-align: justify;">
 <span style="color: #4b0048;"> We still work together with our most wise and honest first partners. Some of them became our good friends. That goes in line with our other motto, <b>“A FRIENDSHIP FOUNDED ON BUSSINESS IS A GOOD DEAL BETTER THAN A BUSSINESS FOUNDED ON FRIENDSHIP”.</b></span><br>
</p>
<div class="tab-block" style="margin: 7px 3px;">
 <img width="100%" alt="sotrudnichestvo_332.jpg" src="/upload/medialibrary/fbb/fbb8cb748d9d289414b1519b00442134.jpg" style="width:100%;" title="Ольга Ефремова и владелец фабрики Vicent Montoro">
</div>
<div class="tab-block" style="margin: 7px 3px;">
 <img width="100%" alt="sotrudnichestvo_333.jpg" src="/upload/medialibrary/670/670791406ace9666cc178c5b9db2ff64.jpg" style="width:100%;" title="Ефремова Ольга Юрьевна и Товкач Мария Владимировна ТПП Энерготорг (ОАО Мосэнерго)">
</div>
<div class="tab-block" style="margin: 7px 3px;">
 <img alt="sotrudnichestvo_331.jpg" src="/upload/medialibrary/e8b/e8b99df57d386db8bccbecab5a974799.jpg" style="width:100%;" title="Зотова Татьяна Александровна ГУП глава АПУ.">
</div>
<p style="text-align: justify;">
 <span style="color: #4b0048;"> Our first Moscow partners were shareholders and managers from PJSC Mosenergo, namely Valery Saltykov and Maria Tovkach. We’d like to thank them for all that learning and toughening up from 2002 to 2007 while working together at Energotorg, a commercial and manufacturing company which is a subsidiary of PJSC Mosenergo. Saltykov and Tovkach were the ones who laid the foundation for a new partner relationship, business etiquette and business management for the Elpaso founder. She had priceless experience of combining two positions at the same time: that of the director of the ELITMEBEL showroom at 31 Zelenodolskaya Street, Moscow and that of the procurement manager at PJSC Mosenergo’s subsidiary Energotorg. ELITMEBEL saw a forming team whose results were evaluated by the executive officers of the subsidiary. In five years we provided furniture for PJSC Moscenergo’s Directorate-General in Raushskaya naberezhnaya, furniture for conference halls at JSC UES of Russia and PJSC Mosenergo; movie theatres, hotels, concert halls and offices at PJSC Mosenergo’s Directorate-General. We also developed design projects and provided furniture for public entities, such as the House of the Government of the Russian Federation, Moscow’s Eastern Administrative district prefecture, the Planning Authority in the South-Eastern Administrative district. Among our most valuable clients are talented managers, such as Tatyana Zotova, head of the Planning Authority in the South Eastern Administrative district at the time; Natalia Golovanova, head of the Ivanovskoe district council in the Eastern Administrative district; Valery Salikov, who served as deputy director of PJSC Mosenergo’s CHP 22 back then. We name these people because they influenced Elpaso’s growth in Moscow, shared their contacts and experience with our team, they let us get something more than just an order. There’s another motto of cooperation here,<b> “HE WHO SERVES PEOPLE IS BETTER THAN HE WHO IS FRIENDS WITH THEM”.</b> </span>
</p>
<p style="text-align: justify;">
 <br>
</p>
<p style="text-align: center;">
 <b><span style="font-size: 16pt; color: #450e61;"> We served and keep serving our clients as if we were creating interior designs for ourselves.</span></b>
</p>
<div class="row">
	<div style="margin: 20px 0px;" class="coop-img-1">
 <img alt="estvo_11.jpg" src="/upload/medialibrary/4af/4afa3f8afc5cdc9c58dd8e6b15592729.jpg" title="Барельеф Schuller. Валенсия 2017./ Руководитель строительной компании Конда - Федоренко В.Н (справа) с коллегами">
	</div>
	<div style="margin: 20px 0px;" class="coop-img-2">
 <img alt="3c6bbe7.jpg" src="/upload/medialibrary/d31/d316ca542305c0adc38599b9c7f4a814.jpg" title="Татьяна Сапешко (слева) и Ольга Ефремова (справа) с партнерами фабрики Schuller (Испания,2014) / Время на часах от Schuller,Валенсия 2017 год.">
	</div>
</div>
<p style="text-align: justify;">
 <span style="color: #4b0048;">Vasily Fedorenko, founder of the company KONDA, appreciated these values and is our strategic partner since 2005. Using his extensive experience in office administration and Elpaso’s knowledge of interior design we completed a number of orders for state corporations, namely Rostec, the Central Bank of the Russian federation, Perovo’s District council. </span>
</p>
<p style="text-align: justify;">
 <span style="color: #4b0048;">
	Vasily Fedorenko’s talented daughter Tatyana Sapeshko worked at our company from 2011 to 2016. During these years Tatyana managed retail orders.<br>
	 In five years of cooperation and accumulation of work experience at Elpaso another direction labelled KONDA was opened. Tatyana Sapeshko manages on her own all the online orders and shipping. </span>
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
 <span style="color: #4b0048;"> In this cooperation we can see another rule working, <b>“OUR REAL TEACHERS ARE EXPERIENCE AND EMOTIONS”.</b></span>
</p>
<div class="tab-block" style="margin: 7px 3px;">
 <img alt="sotrudnichestvo_221.jpg" src="/upload/medialibrary/0f7/0f71505402c8a4bc2aad1e40a869bde3.jpg" style="width:100%;" title="Арт-менеджер компании Эльпасо Влад Галузинский на съемке интервью с владельцами Coleccion Alexandra.">
</div>
<div class="tab-block" style="margin: 7px 3px;">
 <img width="880" alt="sotrudnichestvo_223.jpg" src="/upload/medialibrary/309/3092020bf3d8dd88378aecc1565f9414.jpg" style="width:100%;" title="Ирэн Кузнецова и Ольга Ефремова выбирают арт объекты от Boffi, Италия, 2017 год">
</div>
<div class="tab-block" style="margin: 7px 3px;">
 <img width="880" alt="sotrudnichestvo_222.jpg" src="/upload/medialibrary/7be/7be5d98f69ba890a84d84c662dceb698.jpg" style="width:100%;" title="Бильярдный стол Class закрытый крышками в студии интерьеров Эльпасо">
</div>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
 <span style="color: #4b0048;"> At <b>Elpaso</b>, we welcome young people who are able to work and grow while creating beautiful interior designs, elaborate design projects; we welcome artists and professionals who can participate in social media marketing of our websites, social media, blogs and other related web pages.</span>
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
 <span style="color: #4b0048;">Elpaso signed a contract with the architect studio “Na Smolenke” in Moscow. We work with its founder Svyatoslav Ivanchenko who is a professional and accomplished Architect of the Russian Federation. Together we worked on projects for housing estates Dominion, Mosfilmovsky, Solov’inaya Roscha, Novoyasenevsky; several projects for Rostec and the Central Bank of the Russian federation. Elpaso and “Na Smolenke” studio coincide in their taste, opinion and approach towards interior design. We schedule meetings with our clients at two locations (based on personal preferences): Smolenskaya ploschad (3-4 minutes from the Smolenskaya metro station to a mansion in pereulok Kamennaya Sloboda) or in Zelenodolskaya street (within walking distance from the Kuzminki metro station).</span><br>
 <span style="color: #450e61;"> </span>
</p>
 <span style="color: #450e61;"> </span>
<div class="row" style="margin:10px 0; ">
 <span style="color: #450e61;"> </span>
	<div class="col-md-4 col-sm-4 col-xs-12">
 <span style="color: #450e61;"> </span><img alt="sotrudnichestvo_21.jpg" src="/upload/medialibrary/042/04235c69a277031b2c9da25451630e9b.jpg" title="Благодарная клиентка - Диана С. обнимает директора компании Эльпасо" style="display:block; Width:100%;">
	</div>
	<div style="text-align: center;" class="col-md-4 col-sm-4 col-xs-12">
 <b><span style="font-size: 13pt;"><br>
 </span></b><b><span style="font-size: 14pt; font-family: 'AcromRegular'; color: #450e61;">
		Our clients highly evaluate our interior designs and our relationship goes beyond finishing a project. We appreciate that many of them come back for another one. <br>
 <br>
 </span></b>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
 <img alt="sotrudnichestvo_1.jpg" src="/upload/medialibrary/e55/e557aa3975771ea71f7155808800bd34.jpg" title="Руководители Арх бюро на Смоленке и студии дизайна Эльпасо в кабинете клиента" style="display:block; Width:100%;">
	</div>
</div>
<p style="text-align: justify;">
 <b><span style="color: #450e61;">Together with “Na Smolenke” studio Elpaso has a significant advantage as it is an authorized dealer of the Colección Alexandra manufacturer. </span></b><span style="color: #450e61;">&nbsp;</span>We present pieces of Colección Alexandra’s unique collections of furniture, such as designer chests of drawers, cushioned furniture, and company’s signature tables in Moscow.
</p>
<p style="text-align: justify;">
	 We create some of our projects in cooperation with talented architects, e. g., <b><span style="color: #450e61;">Elena Potyomkina</span></b> who started her professional career at our company, <b><span style="color: #450e61;">Ivan Panteleev</span></b> with whom we go back to PJSC Mosenergo, and <b><span style="color: #450e61;">Alexandra Petropavlovskaya</span></b> who nowadays has her our design studio and excels in interior design. <br>
</p>
<div class="row">
	<div style="margin: 20px 0px;" class="coop-img-1">
 <img alt="sotrudnichestvo_17.jpg" src="/upload/medialibrary/f4c/f4cbb08b77738a3f7e70582878440872.jpg" title="В переговорной студии интерьеров Эльпасо">
	</div>
	<div style="margin: 20px 0px;" class="coop-img-2">
 <img alt="sotrudnichestvo_9.jpg" src="/upload/medialibrary/f47/f473526941ccc229ab0b34b298559912.jpg" title="Мы открыты к бизнес диалогу с Вами - команда компании Эльпасо">
	</div>
</div>
<p style="text-align: justify;">
 <b><span style="color: #450e61;">Our partners are wonderful artists Olga Gavrisheva, Alexey Filatov, Rima Vladyuk and Zinaida Chernyshova. We thank all of you for working with us! Together we made the most unusual and creative projects for our clients adding a spark of your talent and soul.</span></b><br>
 <br>
</p>
<p style="text-align: center;">
 <span style="font-size: 14pt;"><span style="color: #450e61;">ELPASO works with and is an authorized dealer of the next Spanish manufacturers:</span></span><span style="color: #450e61;"> </span>
</p>
 <span style="color: #450e61;"> </span>
<p style="text-align: center;">
 <span style="color: #450e61;"> </span><span style="font-size: 14pt;"><b><span style="color: #450e61;">Ofifran, COLECCIÓN ALEXANDRA, LLASS, MARINER, SOHER, TECNI NOVA, VALENTÍ, SCHULLER, ARTE ROMERA.</span></b></span><b><span style="color: #450e61;"> </span><br>
 <span style="color: #450e61;"> </span><span style="font-size: 14pt; color: #450e61;"> </span></b><span style="color: #450e61;"> </span><br>
 <span style="color: #450e61;"> </span><span style="font-size: 14pt;"><b><span style="color: #450e61;"> </span></b><span style="color: #450e61;">We recommend and supply goods of several Italian manufacturers:</span></span><span style="color: #450e61;"> </span>
</p>
 <span style="color: #450e61;"> </span>
<p style="text-align: center;">
 <span style="color: #450e61;"> </span>
</p>
 <span style="color: #450e61;"> </span>
<p style="text-align: center;">
 <span style="color: #450e61;"> </span><span style="font-size: 14pt;"><b><span style="color: #450e61;">MASCHERONI, TURRI, FORMITALIA, ReDeco, SMANIA, MARIANI, VISMARA </span></b></span><span style="color: #450e61;"> </span>
</p>
 <span style="color: #450e61;"> </span>
<p style="text-align: center;">
 <span style="color: #450e61;"> </span>
</p>
 <span style="color: #450e61;"> </span>
<p style="text-align: center;">
 <span style="color: #450e61;"> </span><span style="font-size: 14pt; color: #450e61;">and more than 70 other excellent Italian factories.</span>
</p>
<p style="text-align: center;">
 <span style="font-size: 14pt;"><b> <br>
 </b></span>
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
 <span style="font-size: 13pt;"><b><span style="color: #42304f;">We are glad to collaborate with interior design professionals, studios, architects, and designers. We guarantee favourable terms of supply to our clients.</span></b></span>
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p>
</p>
 </b>