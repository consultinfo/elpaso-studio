<h2 style="margin: 0 3px 30px;"><span style="color: #4b0048;">What are the advantages of collaborating with Elpaso?</span></h2>
<div class="tab-block" style="margin: 0px 3px; text-align: center;">
 <img alt="1_stvo.jpg" src="/upload/medialibrary/b83/b83e6ccfd05fd8aee9108e91c40b194a.jpg" style="width:100%;" title="The Elpaso company is an authorized distributor of MBMbiliardi">
	<h3><span style="color: #4b0048;">Special terms for buying unique billiard tables</span><br>
 </h3>
	<div class="author-block" style="padding:0;">
		<p style="height: 300px; text-align: justify; width:100%; color: black;">
			 The Elpaso company is an authorized distributor of MBMbiliardi in Russia and the CIS which brings an opportunity to buy a billiard table at a good price. The factory’s Moscow office is the only one that offers the best terms for our clients in Russia and the neighboring countries. Our clients buy billiard tables directly from us and are offered the list price without any additional cost from intermediaries. We import the tables directly from Rome to Moscow at the manufacturer’s suggested retail price and deliver them with the help of experts in installing the tables. We have a partner programme for architects and interior designers. Billiard tables are exhibited at the manufacturer’s fairs in Moscow and Rome. We also have tables in stock. We use a whole range of facing for our tables, be it furniture polish, leather, glass or wood combined with aliminium. Our billiard tables are real transformers that adapt to your height. Both adults and children can play at the same time. There’s a remote control than helps you adapt the height of the table. Thus you have a coffee table thanks to a special folding device under the tabletop. MBMbiliardi has a design called <a href="http://youtu.be/PRA96r3ff3Y">Biliardo Revolution</a> for disabled people who use a wheelchair in their day-to-day life. MBMbiliardy and Elpaso make this game accessible to everyone!<br>
		</p>
 <a class="hide_more_text" href="#">Read more</a>
	</div>
</div>
<div class="tab-block" style="margin: 0px 3px; text-align: center;">
 <img alt="logo_stvo.jpg" src="/upload/medialibrary/c89/c89439cc939f3f4bb137d3c5a8206f73.jpg" style="width:100%;" title="Complete interior design">
	<h3><span style="color: #4b0048;">Complete interior<br>
	 design</span></h3>
	<div class="author-block" style="padding:0;">
		<p style="height: 300px; text-align: justify; width:100%;">
			 Elpaso’s experts have been creating complete interior designs for more than twelve years. Only the best Russian and European architects design properties, all the building and finishing works are supervised by a team of highly qualified professionals. We keep our projects on track which includes design implementation, quality control of services, materials, and deliveries. We abide by commercial confidentiality and strictly stick to the contract. Our clients appreciate the most how comfortably and calmly their design interiors come true. All our experts go through tests on treating people of different age, social status and life experience. We can provide reviews from our most distinguished clients from state or other institutions in Russia and Europe. Besides, Elpaso can always give you a tour of the finished projects. Our website has a factory and article number filter. Our catalog has more the 12,000 designer pieces, for example, furniture, lighting, billiard tables, fabrics, cornice mouldings, and materials for any interior design. We offer you the list price and direct supply from the manufacturer. Elpaso is an authorized dealer of many leading world factories and provides its clients with a wide range of services.
		</p>
 <a class="hide_more_text" href="#">Read more</a>
	</div>
</div>
<div class="tab-block" style="margin: 0px 3px; text-align: center;">
 <img alt="3_stvo.jpg" src="/upload/medialibrary/f1e/f1ec87578005a3fa9ecaaf74ebdbf11f.jpg" style="width:100%;" title="More than 8000 original interior items">
	<h3><span style="color: #4b0048;">Elpaso’s guarantees<br>
	 to you</span></h3>
	<div class="author-block" style="padding:0;">
		<p style="height: 300px; text-align: justify; width:100%;">
			 When you get an Elpaso interior, you don’t have to pay for a costly rent of commercial property. We have our own spaces; that is why you only pay for the goods and delivery plus your personal discount. Thanks to our supervisors, architects, and interior designers you will avoid useless corrections and will save time, trouble, and money. Elpaso was born in 2005 and since that moment you can find it at 36/2 Zelenodolskaya Street, Moscow. If necessary, we provide our clients with contract guarantees as well as the documents for deliveries including auction and tender purchases complying with the Budget Law and other laws of the Russian Federation. Elpaso LLC.’s founder and key partners have been the same for more than twelve years. <span style="color: #4b0048;"><b>Elpaso appreciates that our clients come back for more designs; for some of them we are a family studio as they recommend us to their children and grandchildren. </b></span>&nbsp;<b><span style="color: #4b0048;">THE MOST VALUABLE THINGS ARE A POSITIVE CONNECTION, GOOD NAME, YOUR TRUST AND AN INTERIOR DESIGN THAT REFLECTS YOU. WE ARE ALL FOR ACKNOWLEDGING AND BEING HELPFUL TO EACH OTHER! These are the best guarantees of cooperation. </span></b>
		</p>
 <a class="hide_more_text" href="#">Read more</a>
	</div>
</div>
<p>
</p>
<div class="tab-block" style="margin: 0px 3px; text-align: center;">
 <img alt="3YlXOf34M4Y.jpg" src="/upload/medialibrary/883/88391b370e1fc997724d337aed52fd45.jpg" style="width:100%;" title="The art is in the details. Or fabrics">
	<h3><span style="color: #4b0048;">The art is in the details.<br>
	 Or fabrics</span></h3>
	<div class="author-block" style="padding:0;">
		<p style="height: 300px;text-align: justify; width:100%;">
			 Our fabrics collections offer drapery, lace curtains, linen, cashmere, velvet, and other sorts of fabrics. We also decorate concert halls “dressing” the stage. For example, we did it for the Gubernsky theatre in Moscow. We made drawings and later manufactured curtains for its picture windows in the hall. Our sewing department makes drapes and textile goods, for example, furniture covers, bed covers, pillows and bolsters. All these can be customised. We decorate rooms in accordance with the interior design of hotels, health retreats, beauty salons, and campsites. Since 2005 we’ve brought cosiness to many houses and apartments in Moscow and other Russian cities. Thanks to our digital drawings our clients can see how their curtain will look in real life. We take our fabrics samples to our clients’ locations so that we can make the best possible choice. It’s a perfect way to see the colour of the fabric in natural light. Thanks to our perfectly organized process of fitting and well elaborated designs new drapes will complete your furniture, facing, the colour of the walls and doors. Our textile goods made by our craftsmen represent the true art in the details and the fluidity of an interior design united in style.
		</p>
 <a class="hide_more_text" href="#">Read more</a>
	</div>
 <span style="color: #4b0048;"> </span>
</div>
<div class="tab-block" style="margin: 0px 3px; text-align: center;">
 <img alt="tfreg6y5n8w.jpg" src="/upload/medialibrary/ddb/ddbcec92eefc32540ddfebf4be5a9d51.jpg" style="width:100%;" title="Exclusive doors and customised furniture">
	<h3><span style="color: #4b0048;">Exclusive doors and customised furniture</span></h3>
	<div class="author-block" style="padding:0;">
		<p style="height: 300px; text-align: justify; width:100%;">
			 We have been providing our customers with unique pieces for more than twelve years. Beautiful interior designs are our vocation. We can offer you everything for manufacturing any type of interior doors, luxury furniture, cabinets and bathtub aprons. We guarantee high quality and we have all the necessary means to keep the unique style of any project. We choose, recommend, control and take full responsibility for manufacturing all the best goods <b>“MADE IN RUSSIA”! </b><span style="color: #000000;"><span style="color: #4b0048;"><b>Elpaso is a partner of major construction companies and is a part of the “GOLDEN RATIO” programme for the owners of real estate built by the KROST concern.</b> As such, we offer a 40% discount on services, materials, doors, lighting, and furniture. </span><span style="color: #4b0048;"><span style="color: #4b0048;"> </span><b><span style="color: #4b0048;"> If you buy real estate we offer you our special terms for interior design projects and decoration!&nbsp;</span></b></span></span>
		</p>
 <a class="hide_more_text" href="#">Read more</a>
	</div>
</div>
<div class="tab-block" style="margin: 0px 3px; text-align: center;">
 <span style="color: #4b0048;"><img alt="mmSQwuatI54.jpg" src="/upload/medialibrary/5ab/5ab46fd6230dffe11b532de9f6ceb536.jpg" style="width:100%;" title="Artwork"></span>
	<h3 style="padding: 13px;"><span style="color: #4b0048;">Artwork<br>
 </span></h3>
	<div class="author-block" style="padding:0;">
		<p style="height: 300px; text-align: justify; width:100%;">
			 As experts, we control our projects from the beginning to the very end. We provide a COMPLETE DECORATION for you that includes TV, picture and mirror frames. We make design for mosaic panel and tile them only with the most experienced professionals. You can make an order for unique artistic pieces such as paintings, bas-relief, matchless creations and skillful painting by our artists, sculptors, and designers from all over Russia and Europe. We create truly emotional interior designs thanks to unique pieces in accordance with your taste, style and point of view. <b>Elpaso aspires to style heights to make your ideas come true! We listen to our customers, we love and understand the beauty of interior designs and we combine this with individual approach to each and every project. We don’t accept mediocre solutions and we are always ready to amaze you!</b>
		</p>
 <a class="hide_more_text" href="#">Read more</a>
	</div>
 <span style="color: #000000;"> </span>
</div>
 <span style="color: #000000;"> </span> <br>