<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новая страница1");
?>

		<div class="catalog-filter"> 

		<div class="row">
            <p><img src="/bitrix/templates/addeo/img/filter.png" alt=""> Показать фильтр</p>
            <img src="/bitrix/templates/addeo/img/arrow-down.png" alt="">
		</div>

			<div class="catalog-filter-open">
				<div class="catalog-filter-open-block">
					<h2>Разделы каталога</h2>
					<hr>
					<ul>
						<li>
							<input type="checkbox" id="checkbox-4">
							<label for="checkbox-4">Диваны, кресла</label>
						</li>
						<li>
							<input type="checkbox" id="checkbox-5">
							<label for="checkbox-5">Банкетки и шезлонги</label>
						</li>
						<li>
							<input type="checkbox" id="checkbox-6">
							<label for="checkbox-6">Пуфы</label>
						</li>
					</ul>
				</div>
				<div class="catalog-filter-open-block catalog-filter-open-block-brand">
					<h2>Бренды</h2>
					<hr>
					<ul>
						<li>
							<input data-brand-main="1" type="checkbox" id="checkbox-7">
							<label for="checkbox-7">Vismara</label>
						</li>
						<li>
							<input data-brand-main="2" type="checkbox" id="checkbox-8">
							<label for="checkbox-8">Vin Disel</label>
						</li>
						<li>
							<input data-brand-main="3" type="checkbox" id="checkbox-9">
							<label for="checkbox-9">Leonardo</label>
						</li>
						<li>
							<input data-brand-main="4" type="checkbox" id="checkbox-10">
							<label for="checkbox-10">Kaprichi Belo</label>
						</li>
						<li>
							<input data-brand-main="5" type="checkbox" id="checkbox-11">
							<label for="checkbox-11">Kaprichi Belo</label>
						</li>
						<li>
							<input data-brand-main="6" type="checkbox" id="checkbox-12">
							<label for="checkbox-12">Kaprichi Belo</label>
						</li>
						<li>
							<input data-brand-main="7" type="checkbox" id="checkbox-13">
							<label for="checkbox-13">Kaprichi Belo</label>
						</li>
					</ul>
				</div>
				<div class="catalog-filter-open-block catalog-filter-open-block-collection">
					<h2>Коллекции</h2>
					<hr>
					<ul>
						<li class="brand-first">
							<p>Сначала выберите бренд</p>
						</li>
						<li data-brand="1,2,3">
							<input type="checkbox" id="checkbox-29">
							<label for="checkbox-29">Vismara</label>
						</li>
						<li data-brand="2">
							<input type="checkbox" id="checkbox-30">
							<label for="checkbox-30">Vin Disel</label>
						</li>
						<li data-brand="3">
							<input type="checkbox" id="checkbox-31">
							<label for="checkbox-31">Leonardo</label>
						</li>
						<li data-brand="4">
							<input type="checkbox" id="checkbox-32">
							<label for="checkbox-32">Kaprichi Belo</label>
						</li>
					</ul>
				</div>
				<div class="catalog-filter-open-block">
					<h2>Стиль</h2>
					<hr>
					<ul>
						<li>
							<input type="checkbox" id="checkbox-14">
							<label for="checkbox-14">Минимализм</label>
						</li>
						<li>
							<input type="checkbox" id="checkbox-15">
							<label for="checkbox-15">Классика</label>
						</li>
						<li>
							<input type="checkbox" id="checkbox-16">
							<label for="checkbox-16">Модерн</label>
						</li>
						<li>
							<input type="checkbox" id="checkbox-17">
							<label for="checkbox-17">Современный</label>
						</li>
						<li>
							<input type="checkbox" id="checkbox-18">
							<label for="checkbox-18">Современный</label>
						</li>
						<li>
							<input type="checkbox" id="checkbox-19">
							<label for="checkbox-19">Современный</label>
						</li>
						<li>
							<input type="checkbox" id="checkbox-20">
							<label for="checkbox-20">Современный</label>
						</li>
						<li>
							<input type="checkbox" id="checkbox-21">
							<label for="checkbox-21">Современный</label>
						</li>
					</ul>
				</div>
				<div class="catalog-filter-open-block">
					<h2>Помещение</h2>
					<hr>
					<ul>
						<li>
							<input type="checkbox" id="checkbox-22">
							<label for="checkbox-22">Гостиная</label>
						</li>
						<li>
							<input type="checkbox" id="checkbox-23">
							<label for="checkbox-23">Кабинет</label>
						</li>
						<li>
							<input type="checkbox" id="checkbox-24">
							<label for="checkbox-24">Спальня</label>
						</li>
						<li>
							<input type="checkbox" id="checkbox-25">
							<label for="checkbox-25">Улица</label>
						</li>
						<li>
							<input type="checkbox" id="checkbox-26">
							<label for="checkbox-26">Улица</label>
						</li>
					</ul>
				</div>
				<div class="catalog-filter-open-block">
					<h2>Предметы поиска</h2>
					<hr>
					<ul>
						<li>
							<input type="checkbox" id="checkbox-27">
							<label for="checkbox-27">Комплекты мебели</label>
						</li>
						<li>
							<input type="checkbox" id="checkbox-28">
							<label for="checkbox-28">Предметы интерьера</label>
						</li>
					</ul>
				</div>

				<div class="catalog-filter-open-block big">
					<h2>Стоимость</h2>
					<hr>
					<div class="filter-price row">
						<input type="text" value="0 Р" />
						<hr>
						<input type="text" value="50000 Р" />
						<div class="ebola"></div>
					</div>
				</div>

				<div class="catalog-filter-open-block big">
					<input type="submit" value="Показать 23 товара" />
					<div class="close-filter"><img src="img/close-filter.png" alt="" />Сбросить фильтр</div>
				</div>

			</div>

        </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>