<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Настройки пользователя");
?>
<?global $USER;
$USER_ID = CUser::GetID(); // Получаем ID текущего пользователя
$getGroup = CUser::GetUserGroupList($USER_ID);
while ($arGroup = $getGroup->Fetch()){
	$group[]=$arGroup["GROUP_ID"];
}
asort($group);
$group_last = array_pop($group);
?>

<?
CModule::IncludeModule('iblock');
$rsGROUPBANNER = CIBlockElement::GetList (
   Array("SORT"=>"ASC"), // сортировка
   Array("IBLOCK_ID" => 21, "SORT" => $group_last), //ID инфоблока
   false,
   false,
   Array("IBLOCK_ID", "ID", "SORT", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL")  //список полей. IBLOCK_ID и ID - обязательны.
);
?>
<?while($banners = $rsGROUPBANNER-> GetNext()):?>
	<?
		$img=CFile::GetPath($banners["PREVIEW_PICTURE"]);
		$text = $banners["PREVIEW_TEXT"];
	?>
<?endwhile;?> 


<section id="profile">

        <div class="profile-wrap" style="background-image: url('<?=$img;?>');">

          <div class="container">        
            <div class="top-links">
              <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","breadcrumb",Array(
			        "START_FROM" => "0", 
			        "PATH" => "", 
			        "SITE_ID" => "s1" 
			    )
			);?>
            </div>
          </div>

          <h1>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/include/profile/title.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>
          </h1>
          <p>
          	<?=$text;?> 
          </p>
        </div>

        <div class="container">

          <div class="menu-left">

<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"left", 
	array(
		"ROOT_MENU_TYPE" => "profile",
		"MENU_CACHE_TYPE" => "Y",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "profile",
		"DELAY" => "N"
	),
	false
);?>

          </div>
          <div class="right-block">

<?

$rsUser = CUser::GetByID($USER_ID); //Вызываем пользователя по его ID
$arUser = $rsUser->Fetch(); //Превращаем в массив его значения
?>
<?foreach ($arUser['UF_WISHLIST'] as $key => $arItems):?>
<?CModule::IncludeModule('iblock');
$get = CIBlockElement::GetByID($arItems); 
$arItem = $get->GetNext();
?>


			<div class="wishlist-block">

		    <?if ($arItem["ID"] == $arItems["ID"]):?>
		        <div data_id="<?=$arItem["ID"];?>" class="wishlist-hover wishlist">
					<svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" 
width="44px" height="44px" viewBox="0 0 44 44" enable-background="new 0 0 44 44" xml:space="preserve"> 
<circle fill="none" stroke="#433050" stroke-miterlimit="10" cx="22.376" cy="22.709" r="19.125"/> 
<g> 
<g> 
<path fill="#41314C" d="M29.306,18.351c-0.839-0.84-1.951-1.299-3.139-1.299c-1.186,0-2.302,0.463-3.141,1.302l-0.438,0.438 
l-0.445-0.445c-0.84-0.839-1.958-1.305-3.145-1.305c-1.183,0-2.298,0.462-3.135,1.298c-0.84,0.839-1.302,1.954-1.298,3.141 
c0,1.187,0.465,2.3,1.306,3.138l6.383,6.385c0.089,0.088,0.208,0.137,0.323,0.137c0.116,0,0.234-0.047,0.323-0.132l6.396-6.375 
c0.841-0.84,1.303-1.955,1.303-3.142C30.605,20.305,30.146,19.189,29.306,18.351z M28.653,23.982l-6.075,6.049l-6.061-6.06 
c-0.666-0.665-1.033-1.549-1.033-2.491c0-0.941,0.363-1.825,1.029-2.488c0.663-0.663,1.548-1.029,2.485-1.029 
c0.941,0,1.829,0.366,2.494,1.033l0.769,0.769c0.181,0.18,0.469,0.18,0.65,0l0.761-0.762c0.667-0.667,1.554-1.033,2.491-1.033 
c0.938,0,1.822,0.366,2.489,1.029c0.665,0.667,1.029,1.551,1.029,2.492C29.686,22.434,29.318,23.316,28.653,23.982z"/> 
</g> 
</g> 
<polygon fill="transparent" points="30.209,22.5 29.75,19.5 27.875,17.875 25.375,17.458 22.458,19.25 19.208,17.375 16.292,18.375 
15.208,20.833 15.042,22.708 16.375,24.417 22.542,30.709 28.584,24.75 "></polygon>
</svg>
		        </div>
		    <?else:?>
		    	<div data_id="<?=$arItem["ID"];?>" class="wishlist-hover wishlist  active">
					<svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" 
width="44px" height="44px" viewBox="0 0 44 44" enable-background="new 0 0 44 44" xml:space="preserve"> 
<circle fill="none" stroke="#433050" stroke-miterlimit="10" cx="22.376" cy="22.709" r="19.125"/> 
<g> 
<g> 
<path fill="#41314C" d="M29.306,18.351c-0.839-0.84-1.951-1.299-3.139-1.299c-1.186,0-2.302,0.463-3.141,1.302l-0.438,0.438 
l-0.445-0.445c-0.84-0.839-1.958-1.305-3.145-1.305c-1.183,0-2.298,0.462-3.135,1.298c-0.84,0.839-1.302,1.954-1.298,3.141 
c0,1.187,0.465,2.3,1.306,3.138l6.383,6.385c0.089,0.088,0.208,0.137,0.323,0.137c0.116,0,0.234-0.047,0.323-0.132l6.396-6.375 
c0.841-0.84,1.303-1.955,1.303-3.142C30.605,20.305,30.146,19.189,29.306,18.351z M28.653,23.982l-6.075,6.049l-6.061-6.06 
c-0.666-0.665-1.033-1.549-1.033-2.491c0-0.941,0.363-1.825,1.029-2.488c0.663-0.663,1.548-1.029,2.485-1.029 
c0.941,0,1.829,0.366,2.494,1.033l0.769,0.769c0.181,0.18,0.469,0.18,0.65,0l0.761-0.762c0.667-0.667,1.554-1.033,2.491-1.033 
c0.938,0,1.822,0.366,2.489,1.029c0.665,0.667,1.029,1.551,1.029,2.492C29.686,22.434,29.318,23.316,28.653,23.982z"/> 
</g> 
</g> 
<polygon fill="transparent" points="30.209,22.5 29.75,19.5 27.875,17.875 25.375,17.458 22.458,19.25 19.208,17.375 16.292,18.375 
15.208,20.833 15.042,22.708 16.375,24.417 22.542,30.709 28.584,24.75 "/> 
</svg>
		    	</div>
		    <?endif;?>
		<a href="<?=$arItem["DETAIL_PAGE_URL"];?>">
              <div class="bg-img-div" style="background-image: url(<?=CFile::GetPath($arItem["PREVIEW_PICTURE"]);?>);"></div>
              <h2><?=$arItem["NAME"];?></h2>
              
              
<?             	
$arProduct = GetCatalogProduct($arItems);
$res = GetCatalogProductPrice($arItems, "1")        
             	?>
              <p class="price"><?=$res["PRICE"];?> <span>Р</span></p>
              </a>
            </div>
		
		
<?endforeach;?>

          </div>

        </div>
      </section>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>