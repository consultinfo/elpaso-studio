<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Настройки пользователя");
?>
<?global $USER;
$USER_ID = CUser::GetID(); // Получаем ID текущего пользователя
$rsUser = CUser::GetByID($USER_ID); //Вызываем пользователя по его ID
$arUser = $rsUser->Fetch(); //Превращаем в массив его значения
$getGroup = CUser::GetUserGroupList($USER_ID);
while ($arGroup = $getGroup->Fetch()){
	$group[]=$arGroup["GROUP_ID"];
}
asort($group);
$group_last = array_pop($group);
?>

<?
CModule::IncludeModule('iblock');
$rsGROUPBANNER = CIBlockElement::GetList (
   Array("SORT"=>"ASC"), // сортировка
   Array("IBLOCK_ID" => 21, "SORT" => $group_last), //ID инфоблока
   false,
   false,
   Array("IBLOCK_ID", "ID", "SORT", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL")  //список полей. IBLOCK_ID и ID - обязательны.
);
?>
<?while($banners = $rsGROUPBANNER-> GetNext()):?>
	<?
		$img=CFile::GetPath($banners["PREVIEW_PICTURE"]);
		$text = $banners["PREVIEW_TEXT"];
	?>
<?endwhile;?> 



<section id="profile">

        <div class="profile-wrap" style="background-image: url('<?=$img;?>');">

          <div class="container">        
            <div class="top-links">
              <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","breadcrumb",Array(
			        "START_FROM" => "0", 
			        "PATH" => "", 
			        "SITE_ID" => "s1" 
			    )
			);?>
            </div>
          </div>

          <h1>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/include/profile/title.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>
          </h1>
          <p>
          	<?=$text;?> 
          </p>
        </div>

        <div class="container">

          <div class="menu-left">

<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"profile", 
	array(
		"ROOT_MENU_TYPE" => "profile",
		"MENU_CACHE_TYPE" => "Y",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "profile",
		"DELAY" => "N"
	),
	false
);?>

          </div>
          <div class="right-block">

            <h1>
            	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
            		"AREA_FILE_SHOW" => "file",
            		"PATH" => "/include/profile/form_title.php",
            		"EDIT_TEMPLATE" => ""
            		),
            		false
            	);?> 
            </h1>


            <div class="profile-form row">
			<div class="messages"></div>
				<input type="hidden" id="IDs" value="<?=$arUser["ID"];?>">
                <label>Ваше имя</label><input type="text" id="name" value="<?=$arUser["NAME"];?>"><br>
                <label>Телефон</label><input type="text" id="phone" placeholder="+7 (___)-__-__" value="<?=$arUser["PERSONAL_PHONE"];?>"><br>
                <label>Электронная почта</label><input type="text" id="email" value="<?=$arUser["EMAIL"];?>"><br>
                <label>Адрес</label><input type="text" id="address" placeholder="Город, улица, дом, квартира" value="<?=$arUser["UF_ADDRESS"];?>"><br>
                <label>Пароль</label><input id="pass" type="password"><br>
                <button class="btn-profile" id="btn-profile" style="float:none; margin-left: 170px;">Сохранить</button>

            </div>

          </div>

        </div>
      </section>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>


<script>

            $('#btn-profile').click(function(){
                // собираем данные с формы

                var name    = $('#name').val();
                var email   = $('#email').val();
                var phone    = $('#phone').val();
                var address   = $('#address').val();
                var pass    = $('#pass').val();
                var id    = $('#IDs').val();
                // отправляем данные
                $.ajax({
                    url: "/ajax/edit_profile.php", // куда отправляем
                    type: "post", // метод передачи
                    dataType: "json", // тип передачи данных
                    data: { // что отправляем
                        "name":    name,
                        "email":   email,
                        "address":    address,
                        "pass":   pass,
                        "id":   id,
                        "phone":    phone
                    },
                    // после получения ответа сервера
                    success: function(data){
                        $('.messages').html(data.result);
                    }
                });
            });

</script>