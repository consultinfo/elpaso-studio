<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
    $msg_box = ""; // в этой переменной будем хранить сообщения формы
    $errors = array(); // контейнер для ошибок
    // проверяем корректность полей
    if($_POST['name'] == "")    $errors[] = "Поле 'Ваше имя' не заполнено!";
    if($_POST['phone'] == "")   $errors[] = "Поле 'Телефон' не заполнено!";

    // если форма без ошибок
    if(empty($errors)){

		// массив значений ответов
		$arValues = array (
		    "form_text_15"  => $_POST['name'],
		    "form_text_16"  => $_POST['phone'],
        "form_textarea_17"  => $_POST['comment']
		);


		CModule::IncludeModule("form");
		if (CFormResult::Add("5", $arValues)){
		    $msg_box =  "Сообщение отправлено.";
        $date = array(
                'SIMPLE_QUESTION_103' => $_POST['name'],
                'SIMPLE_QUESTION_727' => $_POST['phone'],
                'SIMPLE_QUESTION_999' => $_POST['comment']
              );
        CEvent::SendImmediate("SIMPLE_FORM_5", SITE_ID, $date, "N", 81);
		}else{
		    global $strError;
		    $msg_box = $strError;
		}



    }else{
        // если были ошибки, то выводим их
        $msg_box = "";
        foreach($errors as $one_error){
            $msg_box .= "<span style='color: red;'>".$one_error."</span><br/>";
        }
    }







    // делаем ответ на клиентскую часть в формате JSON
    echo json_encode(array(
        'result' => $msg_box
    ));



?>


<?
// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>
