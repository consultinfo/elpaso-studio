<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
    $msg_box = ""; // в этой переменной будем хранить сообщения формы
    $errors = array(); // контейнер для ошибок
    // проверяем корректность полей
    if($_POST['name'] == "")    $errors[] = "Поле 'Ваше имя' не заполнено!";
    if($_POST['email'] == "")   $errors[] = "Поле 'Ваш e-mail' не заполнено!";
    if($_POST['pass'] == "") 	$errors[] = "Поле 'Пароль' не заполнено!";
 
    // если форма без ошибок
    if(empty($errors)){          

		$user = new CUser;
		$arFields = Array(
		  "NAME"              => $_POST['name'],
		  "EMAIL"             => $_POST['email'],
		  "LOGIN"             => $_POST['email'],
		  "ACTIVE"            => "Y",
		  "GROUP_ID"          => array(2,3,6),
		  "PASSWORD"          => $_POST['pass'],
		  "CONFIRM_PASSWORD"  => $_POST['pass']
		);

		$ID = $user->Add($arFields);
				
				
				
if (intval($ID) > 0){
    $msg_box =  "Пользователь успешно зарегестрирван.";
	
	global $USER;
	if ($USER->Authorize($ID)){
	    $reload = 'Вы авторизованы!';
	}

}else{
    $msg_box = $user->LAST_ERROR;
}	
		
		
		
    }else{
        // если были ошибки, то выводим их
        $msg_box = "";
        foreach($errors as $one_error){
            $msg_box .= "<span style='color: red;'>".$one_error."</span><br/>";
        }
    }
 
 




 
    // делаем ответ на клиентскую часть в формате JSON
    echo json_encode(array(
        'result' => $msg_box,
        'reload' => $reload
    ));
     
    
     
?>


<?
// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>