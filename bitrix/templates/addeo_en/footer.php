<?if((empty($page_type)) && (empty($page_index))):?>

            <div class="q-block">
            		<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                		"AREA_FILE_SHOW" => "file",
                		"PATH" => "/eng/include/questions_title.php",
                		"EDIT_TEMPLATE" => ""
                		),
                		false
                	);?>

              <div class="row">
                <button class="btn-q-block questions_button"><img src="<?=SITE_TEMPLATE_PATH;?>/img/phone.png" alt="">Обратный звонок</button>
                <h4>
                	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                		"AREA_FILE_SHOW" => "file",
                		"PATH" => "/eng/include/phone.php",
                		"EDIT_TEMPLATE" => ""
                		),
                		false
                	);?>
                </h4>
                <h5>
                	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                		"AREA_FILE_SHOW" => "file",
                		"PATH" => "/eng/include/email.php",
                		"EDIT_TEMPLATE" => ""
                		),
                		false
                	);?>
                </h5>
              </div>
            </div>

          </div>

        </div>
      </section>

<?endif;?>

<!--margin-content-->
</div>

<footer>

    <div class="footer-top row">
        <div class="container">

            <div class="footer-block">
                <h2>Elpaso</h2>
                <?$APPLICATION->IncludeComponent("bitrix:menu", "footer", Array(
                    "ROOT_MENU_TYPE" => "footer",	// Тип меню для первого уровня
                    "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
                    "MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "footer",	// Тип меню для остальных уровней
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                ),
                false);?>
            </div>

            <div class="footer-block">
                <h2>Authors</h2>
                <?
                CModule::IncludeModule('iblock');
                $rsLINK = CIBlockSection::GetList (
                    Array("SORT"=>"ASC"), // сортировка
                    Array("IBLOCK_ID" => 23), //ID инфоблока
                    true, // считать ли количество вложенных элементов
                    Array("IBLOCK_ID", "ID", "NAME", "SECTION_PAGE_URL"), // фильтр
                    false
                );
                ?>
                <?while($all = $rsLINK-> GetNext()):?>
                <a href="<?=$all["SECTION_PAGE_URL"];?>"><?=$all["NAME"];?></a>
                <?endwhile;?>
            </div>

            <div class="footer-block">
                <h2>Catalog</h2>
                <?
                CModule::IncludeModule('iblock');
                $rsLINK = CIBlockSection::GetList (
                    Array("SORT"=>"ASC"), // сортировка
                    Array("IBLOCK_ID" => 22, "DEPTH_LEVEL" => 1), //ID инфоблока
                    true, // считать ли количество вложенных элементов
                    Array("IBLOCK_ID", "ID", "NAME", "SECTION_PAGE_URL"), // фильтр
                    false
                );
                ?>
                <?while($all = $rsLINK-> GetNext()):?>
                <a href="<?=$all["SECTION_PAGE_URL"];?>"><?=$all["NAME"];?></a>
                <?endwhile;?>
            </div>

            <div class="footer-block">
                <h2>Portfolio</h2>
                <?
                CModule::IncludeModule('iblock');
                $rsLINK = CIBlockSection::GetList (
                    Array("SORT"=>"ASC"), // сортировка
                    Array("IBLOCK_ID" => 24), //ID инфоблока
                    true, // считать ли количество вложенных элементов
                    Array("IBLOCK_ID", "ID", "NAME", "SECTION_PAGE_URL"), // фильтр
                    false
                );
                ?>
                <?while($all = $rsLINK-> GetNext()):?>
                <a href="<?=$all["SECTION_PAGE_URL"];?>"><?=$all["NAME"];?></a>
                <?endwhile;?>
            </div>

            <div class="footer-block-right">
                <p class="phone">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/eng/include/phone.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    false);?>
                </p>
                <p class="date">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/eng/include/time_work.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    false);?>
                </p>
                <button class="btn-consult questions_button">Get consultation</button>
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => "/eng/include/footer_info.php",
                    "EDIT_TEMPLATE" => ""
                ),
                false);?>
            </div>
        </div>

    </div>

    <div class="footer-bottom">
        <div class="container">
            <?$APPLICATION->IncludeComponent("bitrix:menu", "info_footer", Array(
                "ROOT_MENU_TYPE" => "info",	// Тип меню для первого уровня
                "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
                "MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                "CHILD_MENU_TYPE" => "info",	// Тип меню для остальных уровней
                "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
            ),
            false);?>
        </div>
    </div>

    <div class="footer-after">
        <div class="container">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/eng/include/footer_banner.php",
                "EDIT_TEMPLATE" => ""
            ),
            false);?>
        </div>
    </div>

</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
<script src="<?=SITE_TEMPLATE_PATH;?>/js/jquery-1.12.3.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH;?>/js/bootstrap.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH;?>/js/owl.carousel.js"></script>
<script src="<?=SITE_TEMPLATE_PATH;?>/js/script.js"></script>

<script>

function initMap() {
    var map = new google.maps.Map(document.getElementById('map-wrap'), {
        center: {lat: 55.709532, lng: 37.764571},
        zoom: 17,
        mapTypeControl: false
    });

    var beachMarker = new google.maps.Marker({
        position: {lat: 55.709532, lng: 37.764571},
        map: map,
        icon: '/bitrix/templates/addeo/img/map-mark.png'
    });
}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnkqctvd8L36pSXpRLV6LNOE8w5eG1jSM&callback=initMap"
async defer></script>


</body>
