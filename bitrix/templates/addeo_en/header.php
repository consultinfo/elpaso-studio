<? IncludeTemplateLangFile(__FILE__);?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<?$APPLICATION->ShowHead();?>
	<title><?$APPLICATION->ShowTitle()?></title>

	<!-- Bootstrap -->
	<link href="<?=SITE_TEMPLATE_PATH;?>/css/bootstrap.css" rel="stylesheet">
	<link href="<?=SITE_TEMPLATE_PATH;?>/css/owl.carousel.css" rel="stylesheet">
	<link href="<?=SITE_TEMPLATE_PATH;?>/css/style.css" rel="stylesheet">
	<link href="<?=SITE_TEMPLATE_PATH;?>/css/media.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(61998061, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        ecommerce:"dataLayer"
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/61998061" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>
<body>

	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	<?require($_SERVER["DOCUMENT_ROOT"]."/cart/function.php");?>

	<div class="modal-wrap modal-wrap-login">
		<div class="modal-login">
			<img class="modal-close" src="<?=SITE_TEMPLATE_PATH;?>/img/close-modal.png" alt="">

			<?$APPLICATION->IncludeComponent(
				"bitrix:system.auth.form",
				"auth",
				Array(
					"FORGOT_PASSWORD_URL" => "",
					"PROFILE_URL" => "",
					"REGISTER_URL" => "",
					"SHOW_ERRORS" => "Y"
				), false
			);?>

		</div>
	</div>

	<div class="modal-wrap modal-wrap-registration">
		<div class="modal-registration">
			<img class="modal-close" src="<?=SITE_TEMPLATE_PATH;?>/img/close-modal.png" alt="">
			<h2>Registration</h2>
			<p>It only takes 15 seconds</p>
			<div class="messages"></div>

			<div class="form">
				<div class="input-wrap">
					<input id="user_name" type="text" placeholder="Your Name"><span>*</span>
				</div>
				<div class="input-wrap">
					<input id="user_email" type="text" placeholder="<?=GetMessage("EMAIL");?>"><span>*</span>
				</div>
				<input id="user_pass" type="password" placeholder="E-mail">
				<button class="btn-modal registration_ajax">password</button>
				<p>or <a href="#" class="login_in">sign in</a></p>
			</div>
		</div>
	</div>

	<div class="modal-wrap modal-wrap-callback">
		<div class="modal-callback">
			<img class="modal-close" src="<?=SITE_TEMPLATE_PATH;?>/img/close-modal.png" alt="">
			<h2>Do you need a call back?</h2>
			<div class="messages2"></div>
			<div class="form">
				<div class="input-wrap">
					<input id="form_name" type="text" placeholder="Your name"><span>*</span>
				</div>
				<div class="input-wrap">
					<input id="form_phone" type="text" placeholder="enter your phone number"><span>*</span>
				</div>
				<button class="btn-modal form_ajax"><img src="<?=SITE_TEMPLATE_PATH;?>/img/send.png" alt="">Send</button>
				<p style="margin-bottom:10px;">By clicking the Submit button, you agree to <a href="/include/personal.docx" style="font-size:14px;">the Privacy Policy.</a></p>
				<p>
					Fill out the form on the website or write to us on
					<span>russia@elpaso-studio.ru</span>
					or call us on
					<span>
						<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => "/eng/include/phone.php",
							"EDIT_TEMPLATE" => ""
						),
						false);?>
					</span>
				</p>
			</div>
		</div>
	</div>
	<?
	$page = $APPLICATION->GetCurDir();
	preg_match("([a-zA-Z]+)", $page, $page_type);
	if(!$page_type["0"]){
		$main = "Y";
	}

	CModule::IncludeModule('iblock');
	$res = CIBlockSection::GetByID(84);
	if($ar_res = $res->GetNext());
	if($ar_res["ACTIVE"] == "Y"){
		$active = "Y";
	}
	?>

	<?if((isset($active)) && (isset($main))):?>
	<header style="display: block;">
		<div class="owl-header">

			<?
			CModule::IncludeModule('iblock');
			$rsSLIDER = CIBlockElement::GetList (
				Array("SORT"=>"ASC"), // сортировка
				Array("IBLOCK_ID" => 26, "SECTION_ID" => 84), //ID инфоблока
				false,
				Array ("nTopCount" => 5), // количество
				Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL")  //список полей. IBLOCK_ID и ID - обязательны.
			);
			?>

			<?while($slider = $rsSLIDER-> GetNextElement()):?>
			<?
			$slider_props = $slider->GetProperties(); //получение свойств
			$slider_arFields = $slider->GetFields(); //получение полей
			?>
			<div class="item" style="background-image: url(<?=CFile::GetPath($slider_arFields["PREVIEW_PICTURE"]);?>);">
				<div class="container">

					<img class="logo" src="<?=SITE_TEMPLATE_PATH;?>/img/logo.svg" width="125" alt="">

					<h1><?=$slider_arFields["NAME"];?></h1>
					<h2><?=$slider_arFields["PREVIEW_TEXT"];?></h2>

					<div class="header-links">
						<?foreach($slider_props["PORTFOLIO"]["VALUE"] as $portfolio):?>
						<?CModule::IncludeModule('iblock');
						$res1 = CIBlockSection::GetByID($portfolio); //получим всё об элементе, который рулит слайдером
						$obRes1 = $res1->GetNext();
						?>
						<a href="<?=$obRes1["SECTION_PAGE_URL"]?>"><?=$obRes1["NAME"]?></a>
						<?endforeach;?>
					</div>

					<a href="/eng/portfolio/"><button class="btn-header"><?=GetMessage("SLIDER_MORE");?></button></a>
				</div>
			</div>
			<?endwhile;?>

		</div>
	</header>
	<?endif;?>

	<nav class="navbar">

		<div class="dropdown">
			<button class="navbar-toggle dropdown-toggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<div class="mobile-menu">

				<img class="close-menu" src="<?=SITE_TEMPLATE_PATH;?>/img/close-menu-dark.png" alt="">

				<div class="mobile-menu-top">

					<p class="phone">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => "/eng/include/phone.php",
							"EDIT_TEMPLATE" => ""
						),
						false);?>
					</p>
					<div class="navbar-top-search">
						<input id="mobile_search_value" type="search" placeholder="Search...">
						<input id="mobile_search_send" type="submit">
						<div class="search-block" id="mobile_search_block"></div>
					</div>
					<button class="btn-mobile-menu login_in"><img src="<?=SITE_TEMPLATE_PATH;?>/img/login-mobile.png" alt=""><?=GetMessage("LOGIN");?></button>
					<p class="contacts"><a href="/eng/contacts/"><?=GetMessage("CONTACTS");?></a></p>

					<hr />

					<ul>
						<?$APPLICATION->IncludeComponent("bitrix:menu", "mobile", Array(
							"ROOT_MENU_TYPE" => "mobile",	// Тип меню для первого уровня
							"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
							"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"CHILD_MENU_TYPE" => "mobile",	// Тип меню для остальных уровней
							"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
						),
						false);?>

					</ul>

				</div>

			</div>
		</div>

		<div class="collapse navbar-collapse">
			<div class="nav-top">
				<div class="container">
					<ul class="navbar-top-left">
						<li class="phone visible-md">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => "/eng/include/phone.php",
								"EDIT_TEMPLATE" => ""
							),
							false);?>
						</li>
                    </ul>
                    <div class="navbar-top-search visible-md">
                        <input id="desk_search_value" type="search" placeholder="Search...">
                        <input id="desk_search_send" type="submit">
                        <div class="search-block" id="desk_search_block"></div>
                    </div>
					<div class="navbar-top-right">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "about", Array(
							"ROOT_MENU_TYPE" => "about",	// Тип меню для первого уровня
							"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
							"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"CHILD_MENU_TYPE" => "about",	// Тип меню для остальных уровней
							"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
						),
						false);?>

                        <li class="phone visible-lg font_bold">
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => "/include/phone.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                                false);?>
                        </li>

                        <li><a href="/" class="none_color">RU</a></li>

						<li class="profile">
							<? global $USER; ?>
							<?if ($USER->IsAuthorized()): ?>
							<a href="/eng/personal/profile/">
								<img src="<?=SITE_TEMPLATE_PATH;?>/img/login.png" alt="">
								<?=CUser::GetFirstName();?>
							</a>
							<?else:?>
							<a href="#" class="login_in">
								<img src="<?=SITE_TEMPLATE_PATH;?>/img/login.png" alt="">
								<?echo 'Login';?>
							</a>
							<?endif;?>
						</li>
					</div>
				</div>
			</div>
			<ul class="nav-bottom nav-bottom_white <?if($APPLICATION->GetCurPage() != "/eng/index.php"){echo "nav-bottom-small";}?>">
				<div class="container">
					<li class="nav-bottom-logo"><a href="/eng/"><img src="<?=SITE_TEMPLATE_PATH;?>/img/logo.svg" width="125" alt=""></a></li>
					<div class="menu-img-wrap">
						<li class="menu-img">
							<a href="/eng/personal/wishlist/">
								<svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="35px" height="35px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
                                    <circle fill="none" stroke="#83314D" stroke-width="1.4" stroke-miterlimit="10" cx="24.958" cy="24.958" r="23.833"/>
                                    <g>
                                        <path fill="#42304F" d="M32.444,20.578c-0.907-0.908-2.111-1.403-3.396-1.403c-1.285,0-2.492,0.498-3.398,1.408l-0.475,0.474
                                        l-0.481-0.481c-0.909-0.908-2.119-1.412-3.402-1.412c-1.28,0-2.487,0.5-3.391,1.404c-0.908,0.909-1.409,2.117-1.405,3.399
                                        c0,1.285,0.504,2.488,1.413,3.396l6.908,6.908c0.095,0.096,0.225,0.147,0.35,0.147c0.125,0,0.254-0.048,0.352-0.143l6.922-6.899
                                        c0.906-0.907,1.408-2.114,1.408-3.397C33.85,22.694,33.35,21.487,32.444,20.578z M31.736,26.672l-6.572,6.55l-6.56-6.56
                                        c-0.721-0.723-1.118-1.676-1.118-2.696c0-1.019,0.393-1.975,1.116-2.692c0.716-0.719,1.673-1.116,2.689-1.116
                                        c1.02,0,1.979,0.397,2.699,1.119l0.832,0.831c0.196,0.195,0.507,0.195,0.703,0l0.824-0.823c0.721-0.723,1.681-1.119,2.695-1.119
                                        c1.016,0,1.973,0.398,2.691,1.116c0.724,0.721,1.114,1.677,1.114,2.697C32.855,24.996,32.46,25.955,31.736,26.672z"/>
                                    </g>
                                </svg>
							</a>
						</li>
						<li class="menu-img">
							<a href="/eng/cart/">
                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="35px" height="35px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
                                    <circle fill="none" stroke="#83314D" stroke-width="1.4" stroke-miterlimit="10" cx="24.958" cy="24.958" r="23.833"/>
                                    <g>
                                        <g>
                                            <path fill="#42304F" d="M33.66,20.641h-2.225l-3.699-3.473l-0.643,0.605l3.162,2.867h-9.76l2.823-2.867l-0.645-0.605l-3.3,3.473
                                            H16.75c-0.839,0-1.518,0.681-1.518,1.518c0,0.838,0.679,1.518,1.518,1.518h0.319l0.765,6.071c0,1.198,0.971,2.169,2.168,2.169
                                            H30.41c1.195,0,2.168-0.971,2.168-2.169l0.764-6.071h0.318c0.838,0,1.518-0.68,1.518-1.518
                                            C35.178,21.321,34.498,20.641,33.66,20.641z M31.711,29.747c0,0.718-0.582,1.299-1.301,1.299H20.002
                                            c-0.719,0-1.301-0.581-1.301-1.299l-0.813-6.071h14.632L31.711,29.747z M33.66,22.81H16.75c-0.358,0-0.65-0.292-0.65-0.651
                                            c0-0.358,0.292-0.65,0.65-0.65h16.91c0.359,0,0.652,0.292,0.652,0.65C34.313,22.518,34.02,22.81,33.66,22.81z"/>
                                        </g>
                                    </g>
                                </svg>
								<div class="p-wrap"><p><?=$count;?></p></div>
							</a>
						</li>
					</div>
                    <div class="navbar-top-search visible-lg search-rigth">
                        <input id="desk_search_value" type="search" placeholder="Поиск...">
                        <input id="desk_search_send" type="submit">
                        <div class="search-block" id="desk_search_block"></div>
                    </div>
                    <div class="nav-bottom-menu">
					<?$APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
						"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
						"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
						"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
						"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
						"MAX_LEVEL" => "1",	// Уровень вложенности меню
						"CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
						"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
					),
					false);?>
					</div>
				</div>
			</ul>
		</div>

	</nav>

    <div class="margin-content">
        <?$page_type = $APPLICATION->GetProperty("type");?>
        <?if((empty($page_type)) && (empty($page_index))):?>

        <section id="content">
            <div class="container">

                <div class="menu-left">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "left",
                        array(
                            "ROOT_MENU_TYPE" => "left",
                            "MENU_CACHE_TYPE" => "Y",
                            "MENU_CACHE_TIME" => "36000000",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "left",
                            "USE_EXT" => "N",
                            "ALLOW_MULTI_SELECT" => "N",
                            "COMPONENT_TEMPLATE" => "left",
                            "DELAY" => "N"
                        ),
                        false
                    );?>
                </div>
                <div class="right-block">

                    <div class="top-links">
                        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","breadcrumb",Array(
                            "START_FROM" => "0",
                            "PATH" => "",
                            "SITE_ID" => "s1"
                            )
                        );?>
                    </div>

                    <h1><?$APPLICATION->ShowTitle()?></h1>


                    <?endif;?>
