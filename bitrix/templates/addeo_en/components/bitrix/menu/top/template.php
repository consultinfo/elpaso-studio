<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true); ?>

<?if (!empty($arResult)):?>

  <?foreach($arResult as $menu):?>

<?if ($menu["LINK"] == '/eng/catalog/'):?>

      <li class="dropdown">

        <a href="<?=$menu["LINK"]?>"><?=$menu["TEXT"]?></a>

        <ul class="dropdown_menu">
          <li class="menu_item sub_menu"><a href="/eng/catalog/decoration/">Decoration</a></li>
          <li class="menu_item sub_menu"><a href="/eng/catalog/doors/">Doors</a></li>
          <li class="menu_item sub_menu"><a href="/eng/catalog/furniture/">Furniture</a></li>
          <li class="menu_item sub_menu"><a href="/eng/catalog/media/">Media</a></li>
          <li class="menu_item sub_menu"><a href="/eng/catalog/light/">Light</a></li>
          <li class="menu_item sub_menu"><a href="/eng/catalog/decor/">Decor</a></li>
        </ul>

      </li>



    <?elseif ($menu["LINK"] == '/eng/catalog/billiards/'):?>

      <li class="dropdown">

        <a href="<?=$menu["LINK"]?>"><?=$menu["TEXT"]?></a>

        <ul class="dropdown_menu">
          <li class="menu_item sub_menu"><a href="/eng/catalog/mbmbiliardi/">MBMbiliardi</a></li>
          <li class="menu_item sub_menu"><a href="/eng/catalog/biliardi-italia/">Biliardi Italia</a></li>
        </ul>

      </li>



    <?else:?>

      <li><a href="<?=$menu["LINK"]?>"><?=$menu["TEXT"]?></a></li>

    <?endif?>

  <?endforeach?>

<?endif?>
