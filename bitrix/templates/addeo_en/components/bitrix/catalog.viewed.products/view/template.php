<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */

$frame = $this->createFrame()->begin();
?>



<section id="already-look">
        <div class="container">

          <h2><?=GetMessage("CVP_TPL_MESS_YOU_LOOKED");?></h2>

          <div class="already-look-wrap">
          
	          <?foreach($arResult["ITEMS"] as $arItem): ?>
	          		<a href="<?=$arItem["DETAIL_PAGE_URL"];?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="<?=$arItem["NAME"];?>"></a>
	          <?endforeach;?>
          
          
         </div>

        </div>
</section>

<?$frame->beginStub();?>
<?$frame->end();