<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arResult['FORM_HEADER'] = str_replace('<form ', '<form class="js-validation-form" ', $arResult['FORM_HEADER']);
?>
<?$this->setFrameMode(true);?>
<?if ($arParams['IS_OK']) {?>
    <div id="form_success">
        <?if ($_REQUEST['formresult']=='addok') {?>
            <div class="h2 form-block__title">СПАСИБО! Наш специалист свяжется с Вами в ближайшее время.</div>
        <?}?>
    </div>
<?
return;
}else {?>
	<h2>Напишите нам</h2>

<?}?>

        <?=$arResult['FORM_HEADER']?>
            <?if(!empty($arResult['FORM_ERRORS'])) {
                ShowError(implode('<br>', $arResult['FORM_ERRORS']));
            }?>
            <?foreach ($arResult['QUESTIONS'] as $code => $item) {
                $type = $item['STRUCTURE'][0]['FIELD_TYPE'];
                $inputName = 'form_' . $type . '_' . $item['STRUCTURE'][0]['ID'];

                $value = $arResult['arrVALUES'][$inputName];
                ?>
                <?if ($type != 'hidden') {?>
                        <?if ($type == 'text') {?>
                            <input type="text" name="<?=$inputName?>" placeholder="<?=$item['CAPTION']?>" value="<?=$value?>">
                        <?}elseif ($type == 'email') {?>
                            <input type="text" name="<?=$inputName?>" placeholder="<?=$item['CAPTION']?>" value="<?=$value?>">
                        <?}elseif ($type == 'textarea') {?>
                            <textarea name="<?=$inputName?>" class="<?=$item['REQUIRED'] == 'Y' ? 'required' : ''?>"><?=$value?></textarea>
                        <?}elseif ($type == 'dropdown') {
                            ?>
                            <select name="form_<?=$type?>_<?=$code?>" class="<?=$item['REQUIRED'] == 'Y' ? 'required' : ''?>">
                                <option value="">Не выбрано</option>
                                <?foreach ($item['STRUCTURE'] as $option) {
                                    $value = $arResult['arrVALUES']['form_' . $type . '_' . $code];
                                    $selected = $value == $option['ID'] ? 'selected' : '';
                                    ?>
                                    <option <?=$selected?> value="<?=$option['ID']?>"><?=$option['MESSAGE']?></option>
                                <?}?>
                            </select>
                        <?}?>
                <?}?>
            <?}?>

            <input type="hidden" name="web_form_submit" value="Y">
            <!--<button type="submit" name="web_form_submit" class="btn-vacancy-form"><img src="<?=SITE_TEMPLATE_PATH;?>/img/send.png" alt=""><?=$arResult['arForm']['BUTTON']?></button>
            --><input type="submit" name="web_form_submit" class="btn-vacancy-form" value="Send">
            <p style="margin-bottom:10px;">By clicking the Submit button, you agree to the<a target="_blank" href="/include/personal.docx" style="font-size:14px;"> Privacy Policy</a></p>
        <?=$arResult['FORM_FOOTER']?>
