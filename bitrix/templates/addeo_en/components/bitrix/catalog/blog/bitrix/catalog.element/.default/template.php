<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<style>
.header_not {
display:none;
}

</style>
<?
// FORMAT_DATETIME - константа с форматом времени сайта
$arDate = ParseDateTime($arResult["DATE_ACTIVE_FROM"], FORMAT_DATETIME);
$min_data = ToLower(GetMessage("MONTH_".intval($arDate["MM"])."_S"));
?>

<h2><?echo $arDate["DD"].".".$arDate["MM"].".".$arDate["YYYY"];?></h2>
          <h1><?=$arResult["NAME"];?></h1>
          <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"];?>" alt="">

          <div class="article-wrap">
            <?=$arResult["DETAIL_TEXT"];?>
          </div>

          <div class="article-like">
            <h4>Понравилась статья?</h4>
            <h5>Расскажите друзьям</h5>
            <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8" async="async"></script>
            <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8" async="async"></script>
            <div class="ya-share2" data-services="vkontakte,facebook" data-counter=""></div>
          </div>
          
<?if($arResult["PROPERTIES"]["MORE"]["VALUE"]):?>
<div class="container">

          <h2>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/include/more.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?> 
          </h2>


			<?foreach($arResult["PROPERTIES"]["MORE"]["VALUE"] as $arMore):?>
					<?CModule::IncludeModule('iblock');
					$get = CIBlockElement::GetByID($arMore); //получим всё об элементе, который рулит слайдером
					$MORE = $get->GetNext();
					?>
					  <a href="<?=$MORE["DETAIL_PAGE_URL"];?>"><div class="also-article-block">
			            <img src="<?=CFile::GetPath($MORE["PREVIEW_PICTURE"]);?>" alt="<?=$MORE["NAME"];?>">
			            <h3>
			              <?CModule::IncludeModule('iblock');
		                  $res1 = CIBlockSection::GetByID($MORE["~IBLOCK_SECTION_ID"]); //получим всё об элементе, который рулит слайдером
		                  $obRes1 = $res1->GetNext(); 
						  echo $obRes1["NAME"];
		                  ?>
			            
			            </h3>
			            <h4><?=$MORE["NAME"];?></h4>
			            <p><?=$MORE["PREVIEW_TEXT"];?></p>
			            <p class="date">
			            <?
						// FORMAT_DATETIME - константа с форматом времени сайта
						$arDate = ParseDateTime($arItem["DATE_ACTIVE_FROM"], FORMAT_DATETIME);
						$min_data = ToLower(GetMessage("MONTH_".intval($arDate["MM"])."_S"));
						?>
						<?echo $arDate["DD"].".".$arDate["MM"].".".$arDate["YYYY"];?>
			            
			            </p>
			          </div></a>
			
			<?endforeach;?>


        </div>
        
<?endif;?>