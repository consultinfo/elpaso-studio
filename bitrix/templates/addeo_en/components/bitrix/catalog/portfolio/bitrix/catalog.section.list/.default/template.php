<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
$page = $APPLICATION->GetCurDir();
?>
<ul class="nav nav-tabs">
<?$count = "1";?>
<?foreach($arResult["SECTIONS"] as $sect):?>

	<li <?if(($page == $sect["SECTION_PAGE_URL"]) || (($page == "/en/portfolio/") && ($count == "1"))):?>class="active"<?endif;?>>
		<a href="<?=$sect["SECTION_PAGE_URL"];?>">
			<?=$sect["NAME"];?>
		</a>
	</li>

<?$count++;?>
<?endforeach;?>
</ul>
