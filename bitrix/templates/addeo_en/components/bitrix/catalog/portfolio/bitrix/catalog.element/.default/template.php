<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<section id="portfolio-work">
        <div class="container">

          <h1><?=$arResult["NAME"];?></h1>
          <p><?=$arResult["PREVIEW_TEXT"];?></p>
			<?if($arResult["PROPERTIES"]["PHOTO"]["VALUE"]):?>
          <div class="owl-portfolio">

            
            <?foreach($arResult["PROPERTIES"]["PHOTO"]["VALUE"] as $arItem): ?>
            	<div class="item">
	              <img src="<?=CFile::GetPath($arItem);?>" alt="">
	            </div>
            <?endforeach;?>

          </div>
<?endif;?>
          <div class="about-project row">

            <h2>О проекте</h2>

            <div class="about-project-left">

              <h3><?=$arResult["PROPERTIES"]["Z1"]["NAME"]?></h3>
              <p><?=$arResult["PROPERTIES"]["Z1"]["VALUE"]["TEXT"]?></p>
              <hr>

              <h3><?=$arResult["PROPERTIES"]["Z2"]["NAME"]?></h3>
              <p><?=$arResult["PROPERTIES"]["Z2"]["VALUE"]["TEXT"]?></p>
            </div>

            <div class="about-project-right">
              <?if($arResult["PROPERTIES"]["RIGHT_PHOTO"]["VALUE"]):?>
              		<img src="<?=CFile::GetPath($arResult["PROPERTIES"]["RIGHT_PHOTO"]["VALUE"]);?>" alt="" />			
              <?else:?>
              	<!--<img src="<?=SITE_TEMPLATE_PATH;?>/img/about-project.jpg" alt="">	-->				
              <?endif;?>
              
            </div>

          </div>

          <div class="solution row">

            <h2><?=$arResult["PROPERTIES"]["Z3"]["NAME"]?></h2>
            <p><?=$arResult["PROPERTIES"]["Z3"]["VALUE"]["TEXT"]?></p>

          </div>
		
		

		<?if($arResult["PROPERTIES"]["BEFORE"]["VALUE"] && $arResult["PROPERTIES"]["AFTER"]["VALUE"]):?>

          <div class="js-stuff juxtapose">
            <img src="<?=CFile::GetPath($arResult["PROPERTIES"]["BEFORE"]["VALUE"]);?>" />
            <img src="<?=CFile::GetPath($arResult["PROPERTIES"]["AFTER"]["VALUE"]);?>" />
          </div>
          <script src="https://cdn.knightlab.com/libs/juxtapose/latest/js/juxtapose.min.js"></script>
          <link rel="stylesheet" href="https://cdn.knightlab.com/libs/juxtapose/latest/css/juxtapose.css">
		<?endif;?>

          <div class="comment">
				<?=$arResult["~DETAIL_TEXT"];?>
            
          </div>

          <!--<button class="btn-portfolio-work">Заказать дизайн-проект</button>-->

        </div>
      </section>
<?if($arResult["PROPERTIES"]["AUTORS"]["VALUE"]):?>
      <section id="team-project">
        <div class="container">

          <div class="team-project-wrap">
            <h2>Над проектом работали</h2>

            

            <?foreach($arResult["PROPERTIES"]["AUTORS"]["VALUE"] as $arAutors): ?>
            	<?CModule::IncludeModule('iblock');
            	$res1 = CIBlockElement::GetByID($arAutors); //получим всё об элементе, который рулит слайдером
            	$obRes1 = $res1->GetNext(); 
            	?>
            	
            	<div class="team-project-block">
	              <img src="<?=CFile::GetPath($obRes1["PREVIEW_PICTURE"]);?>" alt="">
	              <h3><?=$obRes1["NAME"];?></h3>
	              <h4><?=$obRes1["PREVIEW_TEXT"];?></h4>
	              <a href="#">@katyaelpaso</a>
	            </div>
            <?endforeach;?>

          </div>

        </div>
      </section>
      
<?endif;?>