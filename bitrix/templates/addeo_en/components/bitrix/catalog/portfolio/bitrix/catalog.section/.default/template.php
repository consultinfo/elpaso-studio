<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container">
	<div class="tab-content row">
		<div class="fade in">
			<?foreach ($arResult['ITEMS'] as $key => $arItem):?>
						<a href="<?=$arItem["DETAIL_PAGE_URL"];?>">
							<div class="portfolio-block">
								<div class="img-bac-wrap"> 
								<div class="img-bac" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>');"></div>
								</div> 


			                  <h2><?=$arItem["NAME"];?></h2>
			                  <p><?=$arItem["PREVIEW_TEXT"];?></p>
			                </div>
						</a>
			<?endforeach;?>
		</div>
	</div>
</div>
<div class="pagination">
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<br /><?=$arResult["NAV_STRING"]?>
	<?endif;?>
</div>