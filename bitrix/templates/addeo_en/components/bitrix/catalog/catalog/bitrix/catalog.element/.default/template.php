<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?
if (!isset($_COOKIE['PRODUCT_VIEW'])) {
	$value = $arResult['ID'];
	setcookie("PRODUCT_VIEW", $value, time()+86400, "/");
} else {
	if (!strstr($_COOKIE['PRODUCT_VIEW'], $arResult['ID'])) {
		if (substr_count($_COOKIE['PRODUCT_VIEW'], ",") <= 6) {
			$value = $arResult['ID'].",".$_COOKIE['PRODUCT_VIEW'];
		} else {
			$explode_id = explode(",", $_COOKIE['PRODUCT_VIEW']);
			$value = $arResult['ID'].",".$explode_id[0].",".$explode_id[1].",".$explode_id[2];
		}
		setcookie("PRODUCT_VIEW", $value,time()+86400, "/");
	}
}
?>

<section id="look">
	<div class="container">
		<div class="row">

			<div class="look-left">

				<div class="owl-look-big">
					<?if($arResult["PROPERTIES"]["PHOTO"]["VALUE"]):?>
					<?foreach($arResult["PROPERTIES"]["PHOTO"]["VALUE"] as $k=>$big):?>
					<div class="item" data-hash="look-<?=$k;?>">
						<img src="<?=CFile::GetPath($big);?>" alt="">
					</div>
					<?endforeach;?>
					<?else:?>
					<div class="item" data-hash="look-<?=$k;?>">
						<img src="<?=CFile::GetPath($arResult["DETAIL_PICTURE"]["SRC"]);?>" alt="">
					</div>
					<?endif;?>
				</div>
				<?if($arResult["PROPERTIES"]["PHOTO"]["VALUE"]):?>
				<div class="look-left-small">
					<?foreach($arResult["PROPERTIES"]["PHOTO"]["VALUE"] as $k=>$small):?>
					<a <?if($k=="0"):?>class="active"<?endif;?> href="#look-<?=$k;?>">
						<img src="<?=CFile::GetPath($small);?>" alt="">
					</a>
					<?endforeach;?>
				</div>
				<?endif;?>

			</div>

			<div class="look-right">

				<?
				if($arResult["PROPERTIES"]["BRAND"]["VALUE"]){
					$brand_url = "/catalog/".$arResult["SECTION"]["PATH"]["0"]["CODE"]."/filter/brand-is-".strtolower($arResult["PROPERTIES"]["BRAND"]["VALUE"])."/apply/";
					$brand_url = str_replace(" ", "%2B", $brand_url);
					$brand_name = $arResult["PROPERTIES"]["BRAND"]["VALUE"];
				}
				if($arResult["PROPERTIES"]["COLLECTIONS"]["VALUE"]){
					$collection_url = "/catalog/".$arResult["SECTION"]["PATH"]["0"]["CODE"]."/filter/brand-is-".strtolower($arResult["PROPERTIES"]["BRAND"]["VALUE"])."/collections-is-".strtolower($arResult["PROPERTIES"]["COLLECTIONS"]["VALUE"])."/apply/";
					$collection_url = str_replace(" ", "%2B", $collection_url);
					$collection_name = $arResult["PROPERTIES"]["COLLECTIONS"]["VALUE"];
				}
				?>

				<h1><?=$arResult["NAME"];?></h1>

				<?if($collection_url):?>
				<p class="look-collection">Бренд <a href="<?=$brand_url;?>"><?=$brand_name;?></a></p>
				<br>
				<p style="margin-top:0px;" class="look-collection">Коллекция <a href="<?=$collection_url;?>"><?=$collection_name;?></a></p>
				<?endif;?>
				<?if (!(array_key_exists($arResult["ID"], $_SESSION['CATALOG_COMPARE_LIST'][$arParams["IBLOCK_ID"]]['ITEMS']))):?>
				<p class="look-compare" style="width: 100%; text-align: right; margin: 0px;"><a href="?action=ADD_TO_COMPARE_LIST&id=<?echo $arResult['ID']?>" rel="nofollow"><img src="<?=SITE_TEMPLATE_PATH;?>/img/stat.png" alt="">Добавить к сравнению</a></p>
				<?else:?>
				<p class="look-compare" style="width: 100%; text-align: right; margin: 0px;"><a class="compare" href="?action=DELETE_FROM_COMPARE_LIST&id=<?echo $arResult['ID']?>" rel="nofollow"><img src="<?=SITE_TEMPLATE_PATH;?>/img/stat.png" alt="">Удалить из сравнения</a></p>
				<?endif;?>

				<h2>
					<?=$arResult['PRICES']['BASE']['ORIG_DISCOUNT_VALUE_VAT']?>
					<span><?=$arResult['PRICES']['BASE']['ORIG_CURRENCY']?></span>
				</h2>
				<a href="<?=$arResult["ADD_URL"];?> "><button class="btn-add-cart">Добавить в корзину</button></a>

				<?
				$USER_ID = CUser::GetID(); // Получаем ID текущего пользователя
				$rsUser = CUser::GetByID($USER_ID); //Вызываем пользователя по его ID
				$arUser = $rsUser->Fetch(); //Превращаем в массив его значения
				foreach($arUser["UF_WISHLIST"] as $key => $value) {
					if ($arResult["ID"] == $value) {
						$active = "да";
					}
				}
				?>
				<?if($active):?>
				<button data_id="<?=$arResult["ID"];?>" class="btn-add-like wishlist active"><img src="<?=SITE_TEMPLATE_PATH;?>/img/heart.png" alt="">В избранном</button>
				<?else:?>
				<button data_id="<?=$arResult["ID"];?>" class="btn-add-like wishlist"><img src="<?=SITE_TEMPLATE_PATH;?>/img/heart.png" alt="">В избранное</button>
				<?endif;?>

				<div class="look-size">
					<ul>
						<?if($arResult["PROPERTIES"]["WIDTH"]["VALUE"]):?><li><img src="<?=SITE_TEMPLATE_PATH;?>/img/width.png" alt=""><?=$arResult["PROPERTIES"]["WIDTH"]["NAME"];?>:<span><?=$arResult["PROPERTIES"]["WIDTH"]["VALUE"];?> <?=$arResult["PROPERTIES"]["WIDTH"]["HINT"];?></span></li><?endif;?>
						<?if($arResult["PROPERTIES"]["HEIGHT"]["VALUE"]):?><li><img src="<?=SITE_TEMPLATE_PATH;?>/img/height.png" alt=""><?=$arResult["PROPERTIES"]["HEIGHT"]["NAME"];?>:<span><?=$arResult["PROPERTIES"]["HEIGHT"]["VALUE"];?> <?=$arResult["PROPERTIES"]["HEIGHT"]["HINT"];?></span></li><?endif;?>
						<?if($arResult["PROPERTIES"]["DEPTH"]["VALUE"]):?><li><?=$arResult["PROPERTIES"]["DEPTH"]["NAME"];?>:<span><?=$arResult["PROPERTIES"]["DEPTH"]["VALUE"];?> <?=$arResult["PROPERTIES"]["DEPTH"]["HINT"];?></span></li><?endif;?>
					</ul>
				</div>

				<ul class="look-char">
					<?if($arResult["PROPERTIES"]["CITY"]["VALUE"]):?><li><?=$arResult["PROPERTIES"]["CITY"]["NAME"];?>:<span><?=$arResult["PROPERTIES"]["CITY"]["VALUE"];?></span></li><?endif;?>
					<?if($arResult["PROPERTIES"]["STYLE"]["VALUE"]):?><li><?=$arResult["PROPERTIES"]["STYLE"]["NAME"];?>:<span><?=$arResult["PROPERTIES"]["STYLE"]["VALUE"];?></span></li><?endif;?>
					<?if($arResult["PROPERTIES"]["ROOM"]["VALUE"]):?><li><?=$arResult["PROPERTIES"]["ROOM"]["NAME"];?>:<span><?=$arResult["PROPERTIES"]["ROOM"]["VALUE"];?></span></li><?endif;?>
					<?if($arResult["PROPERTIES"]["COLOR"]["VALUE"]):?><li><?=$arResult["PROPERTIES"]["COLOR"]["NAME"];?>:<span><?=$arResult["PROPERTIES"]["COLOR"]["VALUE"];?></span></li><?endif;?>
					<?if($arResult["PROPERTIES"]["MATERIAL"]["VALUE"]):?><li><?=$arResult["PROPERTIES"]["MATERIAL"]["NAME"];?>:<span><?=$arResult["PROPERTIES"]["MATERIAL"]["VALUE"];?></span></li><?endif;?>
					<?if($arResult["PROPERTIES"]["ARTICLE"]["VALUE"]):?><li><?=$arResult["PROPERTIES"]["ARTICLE"]["NAME"];?>:<span><?=$arResult["PROPERTIES"]["ARTICLE"]["VALUE"];?></span></li><?endif;?>
				</ul>

				<p class="look-descr"><?=$arResult["PREVIEW_TEXT"];?></p>

			</div>

		</div>
	</div>
</section>

<?if($arResult["SETS"]):?>
<section id="look-book">
	<div class="container">

		<h2>Предметы интерьера из Look Book</h2>

		<div class="owl-look-book">

			<?foreach($arResult["SETS"] as $arProduct): ?>
			<div class="item">
				<a href="<?=$arProduct["DETAIL_PAGE_URL"]?>">
					<div class="look-book-block">
						<!--<img class="look-book-hover" src="img/trend-like.png" alt="">-->
						<img src="<?=$arProduct["PREVIEW_PICTURE"]?>" alt="">
						<h3><?=$arProduct["NAME"]?></h3>
						<p><?=$arProduct["SECTION"]?></p>
						<h4><?=$arProduct["PRICE"]?> <span>Р</span></h4>
					</div>
				</a>
			</div>
			<?endforeach;?>

		</div>

	</div>
</section>
<?endif;?>

<section id="product-block">
	<div class="container">

		<div class="product-block-left">
			<h2>
				<img src="<?=SITE_TEMPLATE_PATH;?>/img/q.png" alt="">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/include/product/product_block_title.php",
					"EDIT_TEMPLATE" => ""
				),
				false);?>
			</h2>
			<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => "/include/product/product_block.php",
				"EDIT_TEMPLATE" => ""
			),
			false);?>
		</div>

		<div class="product-block-right">
			<h2>
				<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/include/product/plus_block_title.php",
					"EDIT_TEMPLATE" => ""
				),
				false);?>
			</h2>

			<div class="product-plus-block row">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/include/product/plus_block_1.php",
					"EDIT_TEMPLATE" => ""
				),
				false);?>
			</div>

			<div class="product-plus-block row">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/include/product/plus_block_2.php",
					"EDIT_TEMPLATE" => ""
				),
				false);?>

			</div>

			<div class="product-plus-block row">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/include/product/plus_block_3.php",
					"EDIT_TEMPLATE" => ""
				),
				false);?>
			</div>

		</div>

	</div>
</section>

<?if(($arResult["SECTION"]["VALUES"]) && ($arResult["PROPERTIES"]["MORE_PRODUCT"]["VALUES"])):?>
<section id="also">
	<div class="container">
		<div class="row">

			<div class="also-left">

				<div class="also-left-person row">

					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => "/include/product/also_diz.php",
						"EDIT_TEMPLATE" => ""
					),
					false);?>

				</div>

				<div class="owl-also-left">
					<?foreach($arResult["PROPERTIES"]["MORE_PRODUCT"]["VALUES"] as $arMore): ?>
					<a href="<?=$arMore["DETAIL_PAGE_URL"];?>">
						<div class="item">
							<img src="<?=$arMore["PREVIEW_PICTURE"];?>" alt="">
						</div>
					</a>
					<?endforeach;?>

				</div>

			</div>

			<div class="also-right">

				<h3>Ещё <?=$arResult["SECTION"]["ELEMENT_CNT"]?> продукта в этой категории</h3>
				<a href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>">Смотреть всю категорию</a>

				<div class="owl-also-right">

					<?foreach($arResult["SECTION"]["VALUES"] as $arCat): ?>
					<a href="<?=$arCat["DETAIL_PAGE_URL"];?>">
						<div class="item">
							<img src="<?=$arCat["PREVIEW_PICTURE"];?>" alt="">
						</div>
					</a>
					<?endforeach;?>


				</div>

			</div>

		</div>
	</div>
</section>
<?endif;?>

<?if(($arResult["SECTION"]["MORE_CAT"]) && ($arResult["SECTION"]["MORE_CAT"]["0"]["PICTURE"])):?>
<section id="similar-cat">
	<div class="container">

		<h2>Похожие категории</h2>

		<div class="row">
			<?
			$k="0";
			?>
			<?foreach($arResult["SECTION"]["MORE_CAT"] as $arMORE_CAT): ?>
			<?if($k < 2):?>
			<a href="<?=$arMORE_CAT["URL"];?>">
				<div class="similar-cat-block row">
					<img src="<?=$arMORE_CAT["PICTURE"]?>" alt="">
					<h3><?=$arMORE_CAT["NAME"]?></h3>
					<p><?=$arMORE_CAT["DESCR"]?></p>
				</div>
			</a>

			<?endif;?>
			<?$k++;?>
			<?endforeach;?>

		</div>

	</div>
</section>
<?endif;?>

<section id="already-look">
	<div class="container">

		<h2>Вы уже смотрели</h2>

		<div class="already-look-wrap">

			<?
			$smotri = explode(",", $_COOKIE["PRODUCT_VIEW"]);
			$k = "0";
			?>
			<?foreach($smotri as $arItem): ?>
			<?if($k < 6):?>
			<?CModule::IncludeModule('iblock');
			$getID = CIBlockElement::GetByID($arItem); //получим всё об элементе, который рулит слайдером
			$items = $getID->GetNext();
			?>
			<a href="<?=$items["DETAIL_PAGE_URL"];?>">
				<div class="bg_smotri" style="background-image: url(<?=CFile::GetPath($items["PREVIEW_PICTURE"]);?>);"></div>
			</a>
			<?endif;?>
			<?$k++;?>
			<?endforeach;?>


		</div>

	</div>
</section>

<section id="social">
	<div class="container">

		<h2>
			<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => "/include/social_title.php",
				"EDIT_TEMPLATE" => ""
			),
			false);?>
		</h2>

		<?$APPLICATION->IncludeComponent("bitrix:menu", "social", Array(
			"ROOT_MENU_TYPE" => "social",	// Тип меню для первого уровня
			"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
			"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
			"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
			"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
			"MAX_LEVEL" => "1",	// Уровень вложенности меню
			"CHILD_MENU_TYPE" => "social",	// Тип меню для остальных уровней
			"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
			"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		),
		false);?>

	</div>
</section>
