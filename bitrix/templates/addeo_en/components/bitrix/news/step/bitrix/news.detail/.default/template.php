<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


		<div class="step-block">
            <div class="row">
              <div class="step-block-number"><strong><?=$arResult["CODE"]?></strong></div>
              <div class="step-block-txt-right">
                <h2><?=$arResult["NAME"]?></h2>
                <h3><?=$arResult["PROPERTIES"]["LONG"]["VALUE"]?></h3>
                <p><?=$arResult["PREVIEW_TEXT"]?></p>
              </div>
            </div>
            <?=$arResult["DETAIL_TEXT"]?>
            
            <?
            $code = $arResult["CODE"] + 1;
            CModule::IncludeModule('iblock');
            $rsSTEP = CIBlockElement::GetList (
               Array("SORT"=>"ASC"), // сортировка
               Array("IBLOCK_ID" => $arResult["IBLOCK_ID"], "CODE" => $code), //ID инфоблока
               false,
               Array ("nTopCount" => 1), // количество
               Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "CODE")  //список полей. IBLOCK_ID и ID - обязательны.
            );
            ?>
            <?while($all = $rsSTEP-> GetNext()):?>
				<?if($all):?>
					<a href="<?=$all["DETAIL_PAGE_URL"];?>"><button class="btn-next-step">Следующий этап - <span><?=$all["NAME"];?></span></button></a>
				<?endif;?>
            <?endwhile;?>
            
            
          
          
          
          
          </div>
