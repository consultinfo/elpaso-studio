<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

      <section id="brands">
        <div class="title-wrap">

          <h1>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/en/include/brand/title.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>
          </h1>
          <p>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => "/en/include/brand/text.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?>
          </p>

        </div>

        <div class="container">


<?
        CModule::IncludeModule('iblock');
        $rsBRAND = CIBlockElement::GetList (
           Array("SORT"=>"ASC"), // сортировка
           Array("IBLOCK_ID" => 28), //ID инфоблока
           false,
           false,
           Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL")  //список полей. IBLOCK_ID и ID - обязательны.
        );
		$rsBRAND2 = CIBlockElement::GetList (
           Array("SORT"=>"ASC"), // сортировка
           Array("IBLOCK_ID" => 28), //ID инфоблока
           false,
           false,
           Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL")  //список полей. IBLOCK_ID и ID - обязательны.
        );
        ?>
        <?$k = "1";?>
        <?$ii = "0";?>
        <?while($i = $rsBRAND-> GetNext()):?>
        <?$ii++;?>
        <?endwhile;?>


        <?while($brand = $rsBRAND2-> GetNext()):?>

		<?if($k == "1"):?>
			<div class="brands-wrap row">
		<?endif;?>
		<?if($k <= "10"):?>
			 <a href="<?=$brand["DETAIL_PAGE_URL"];?>"><div class="brand-block">
              <div class="brand-block-img">
                <img src="<?=CFile::GetPath($brand["PREVIEW_PICTURE"]);?>" alt="">
              </div>
              <h2><?=$brand["NAME"];?></h2>
            </div></a>
        <?endif;?>
		<?if($k == "10"):?>
			</div>
			<div class="brand-hr"></div>
			<h3>И ещё более <span><?echo $ii-10;?> фабрик</span></h3>
			<div class="brand-also">
		<?endif;?>
		<?if($k > "10"):?>
			<a href="<?=$brand["DETAIL_PAGE_URL"];?>"><p><?=$brand["NAME"];?></p></a>
		<?endif;?>
		<?$k++;?>
        <?endwhile;?>



        	</div>




        </div>
</section>
