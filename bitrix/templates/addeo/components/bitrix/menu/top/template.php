<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true); ?>

<?if (!empty($arResult)):?>

  <?foreach($arResult as $menu):?>

    <?if ($menu["LINK"] == '/catalog/'):?>

      <li class="dropdown">

        <a href="<?=$menu["LINK"]?>"><?=$menu["TEXT"]?></a>

        <ul class="dropdown_menu">
          <li class="menu_item sub_menu"><a href="/catalog/otdelka/">Отделка</a></li>
          <li class="menu_item sub_menu"><a href="/catalog/dveri/">Двери</a></li>
          <li class="menu_item sub_menu"><a href="/catalog/mebel/">Мебель</a></li>
          <li class="menu_item sub_menu"><a href="/catalog/premium-akustika/">Премиум акустика</a></li>
          <li class="menu_item sub_menu"><a href="/catalog/svet/">Свет</a></li>
          <li class="menu_item sub_menu"><a href="/catalog/dekor/">Декор</a></li>
        </ul>

      </li>



    <?elseif ($menu["LINK"] == '/catalog/bilyardnye-stoly1/'):?>

      <li class="dropdown">

        <a href="<?=$menu["LINK"]?>"><?=$menu["TEXT"]?></a>

        <ul class="dropdown_menu">
          <li class="menu_item sub_menu"><a href="/catalog/mbmbiliardi/">MBMbiliardi</a></li>
          <li class="menu_item sub_menu"><a href="/catalog/biliardi-italia/">Biliardi Italia</a></li>
        </ul>

      </li>



    <?else:?>

      <li><a href="<?=$menu["LINK"]?>"><?=$menu["TEXT"]?></a></li>

    <?endif?>

  <?endforeach?>

<?endif?>
