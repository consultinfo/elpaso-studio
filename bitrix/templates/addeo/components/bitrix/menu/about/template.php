<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true); ?>
<?if (!empty($arResult)):?>

<?foreach($arResult as $menu):?>

<li><a href="<?=$menu["LINK"]?>"><?=$menu["TEXT"]?></a></li>

<?endforeach?>

<?endif?>
