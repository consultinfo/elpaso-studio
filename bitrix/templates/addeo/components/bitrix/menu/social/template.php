<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true); ?>

<?if (!empty($arResult)):?>
<div class="all-social">
<?foreach($arResult as $big):?>

            <a href="<?=$big["LINK"]?>"><div class="social-block"><?=$big["TEXT"]?></div></a>

<?endforeach?>
</div>
<div class="all-social-mobile">
<?foreach($arResult as $min):?>

            <a href="<?=$min["LINK"]?>"><img src="<?=SITE_TEMPLATE_PATH;?>/img/<?=$min["PARAMS"]["IMG"]?>" alt=""></a>

<?endforeach?>
</div>
<?endif?>
