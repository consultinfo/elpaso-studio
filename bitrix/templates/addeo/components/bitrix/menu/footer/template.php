<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true); ?>
<?if (!empty($arResult)):?>

<?foreach($arResult as $menu):?>

<a href="<?=$menu["LINK"]?>"><?=$menu["TEXT"]?></a>

<?endforeach?>

<?endif?>
