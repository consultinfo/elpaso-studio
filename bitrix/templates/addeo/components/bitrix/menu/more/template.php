<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true); ?>

<?if (!empty($arResult)):?>
	<?foreach($arResult as $big):?>
		<div class="about-also-a-wrap">
	         <a href="<?=$big["LINK"]?>"><img src="<?=SITE_TEMPLATE_PATH;?>/img/<?=$big["PARAMS"]["IMG"]?>" alt=""><?=$big["TEXT"]?></a>
		</div>
	<?endforeach?>

<?endif?>
