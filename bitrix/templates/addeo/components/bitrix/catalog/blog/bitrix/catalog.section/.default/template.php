<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container">
	<div class="tab-content row">
		<div class="fade in">
			<?foreach ($arResult['ITEMS'] as $key => $arItem):?>

				<a href="<?=$arItem["DETAIL_PAGE_URL"];?>">
					<div class="blog-block">
	                  <div class="blog-img-wrap" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>);">

	                  </div>
	                  <p class="cat">
		                  <?CModule::IncludeModule('iblock');
		                  $res1 = CIBlockSection::GetByID($arItem["~IBLOCK_SECTION_ID"]); //получим всё об элементе, который рулит слайдером
		                  $obRes1 = $res1->GetNext(); 
						  echo $obRes1["NAME"];
		                  ?>
	                  </p>
	                  <h2><?=$arItem["NAME"];?></h2>
	                  <h3><?=$arItem["PREVIEW_TEXT"];?></h3>
	                  <?
// FORMAT_DATETIME - константа с форматом времени сайта
$arDate = ParseDateTime($arItem["DATE_ACTIVE_FROM"], FORMAT_DATETIME);
$min_data = ToLower(GetMessage("MONTH_".intval($arDate["MM"])."_S"));
?>

<p class="date"><?echo $arDate["DD"].".".$arDate["MM"].".".$arDate["YYYY"];?></p>
	
	                </div>
				</a>

			<?endforeach;?>
		</div>
	</div>
</div>
<div class="pagination">
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<br /><?=$arResult["NAV_STRING"]?>
	<?endif;?>
</div>