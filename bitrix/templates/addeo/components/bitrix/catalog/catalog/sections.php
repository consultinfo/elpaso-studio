<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

?>


<div class="top-links">
        <div class="container">
			<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","breadcrumb",Array(
			        "START_FROM" => "0", 
			        "PATH" => "", 
			        "SITE_ID" => "s1" 
			    )
			);?>
        </div>
</div>

      <section id="catalog-all">
        <div class="container">

          <h1>Каталог</h1>
          <p>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
          		"AREA_FILE_SHOW" => "file",
          		"PATH" => SITE_TEMPLATE_PATH . "/include/catalog/text.php",
          		"EDIT_TEMPLATE" => ""
          		),
          		false
          	);?> 
          </p>

          

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => SITE_TEMPLATE_PATH . "/include/catalog_all.php",
	"EDIT_TEMPLATE" => ""
	),
	false
);?> 



        </div>
      </section>
