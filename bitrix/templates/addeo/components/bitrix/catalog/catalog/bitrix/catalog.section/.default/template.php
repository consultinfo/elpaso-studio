<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
$USER_ID = CUser::GetID(); // Получаем ID текущего пользователя
$rsUser = CUser::GetByID($USER_ID); //Вызываем пользователя по его ID
$arUser = $rsUser->Fetch(); //Превращаем в массив его значения?>

<?foreach ($arResult['ITEMS'] as $key => $arItem):?>


<div class="wishlist-block">

	<div data_id="<?=$arItem["ID"];?>" class="wishlist-hover wishlist <?foreach($arUser["UF_WISHLIST"] as $value):?><?if ($arItem["ID"] == $value):?>active<?endif;?><?endforeach;?>">
		<svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		width="44px" height="44px" viewBox="0 0 44 44" enable-background="new 0 0 44 44" xml:space="preserve">
		<circle fill="none" stroke="#433050" stroke-miterlimit="10" cx="22.376" cy="22.709" r="19.125"/>
		<g>
			<g>
				<path fill="#41314C" d="M29.306,18.351c-0.839-0.84-1.951-1.299-3.139-1.299c-1.186,0-2.302,0.463-3.141,1.302l-0.438,0.438
				l-0.445-0.445c-0.84-0.839-1.958-1.305-3.145-1.305c-1.183,0-2.298,0.462-3.135,1.298c-0.84,0.839-1.302,1.954-1.298,3.141
				c0,1.187,0.465,2.3,1.306,3.138l6.383,6.385c0.089,0.088,0.208,0.137,0.323,0.137c0.116,0,0.234-0.047,0.323-0.132l6.396-6.375
				c0.841-0.84,1.303-1.955,1.303-3.142C30.605,20.305,30.146,19.189,29.306,18.351z M28.653,23.982l-6.075,6.049l-6.061-6.06
				c-0.666-0.665-1.033-1.549-1.033-2.491c0-0.941,0.363-1.825,1.029-2.488c0.663-0.663,1.548-1.029,2.485-1.029
				c0.941,0,1.829,0.366,2.494,1.033l0.769,0.769c0.181,0.18,0.469,0.18,0.65,0l0.761-0.762c0.667-0.667,1.554-1.033,2.491-1.033
				c0.938,0,1.822,0.366,2.489,1.029c0.665,0.667,1.029,1.551,1.029,2.492C29.686,22.434,29.318,23.316,28.653,23.982z"/>
			</g>
		</g>
		<polygon fill="transparent" points="30.209,22.5 29.75,19.5 27.875,17.875 25.375,17.458 22.458,19.25 19.208,17.375 16.292,18.375
		15.208,20.833 15.042,22.708 16.375,24.417 22.542,30.709 28.584,24.75 "/></svg>
	</div>

	<?
	$file_wm = CFile::ResizeImageGet(
		$arItem["PREVIEW_PICTURE"]["ID"],
		array(),  // без изменения размера
		BX_RESIZE_IMAGE_PROPORTIONAL,
		false,
		$arFilterss = Array(array("name" => "watermark", "position" => "topright", "size"=>"real", "file"=>$_SERVER['DOCUMENT_ROOT']."/bitrix/templates/addeo/img/watermark.png"))
	);
	?>

	<a href="<?=$arItem["DETAIL_PAGE_URL"];?>">
		<div class="bg-img-div" style="background-image: url(<?=$file_wm['src']?>);"></div>
		<h2><?=$arItem["NAME"];?></h2>
		<!--<p>Мебель для гостинной</p>-->
		<p class="price"><?=ceil($arItem['PRICES']['BASE']['PRINT_VALUE']);?> <span>Р</span></p>
	</a>
</div>

<?endforeach;?>


<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
