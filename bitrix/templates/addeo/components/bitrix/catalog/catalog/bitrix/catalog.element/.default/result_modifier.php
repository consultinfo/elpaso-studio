<?if($arResult["CATALOG_TYPE"] > 1):?>
	<?
	$arSets = CCatalogProductProvider::GetSetItems($arResult["ID"], "1", array("NAME"));
	$N="0";	
	?>
	<?foreach($arSets as $arSets1): ?>
		<?foreach($arSets1["ITEMS"] as $arItem): ?>
			<?
			$get_SECT = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]); 
			$obRes1 = $get_SECT->GetNext(); 
			$arResult["SETS"][$N]["ID"] = $arItem["ITEM_ID"];
			$arResult["SETS"][$N]["NAME"] = $arItem["NAME"];
			$arResult["SETS"][$N]["QUANTITY"] = $arItem["QUANTITY"];
			$Price = CPrice::GetBasePrice($arItem["ITEM_ID"]);
			$arResult["SETS"][$N]["PRICE"] = $Price["PRICE"];
			$arResult["SETS"][$N]["DETAIL_PAGE_URL"] = $arItem["DETAIL_PAGE_URL"];
			$arResult["SETS"][$N]["SECTION"] = $obRes1["NAME"];
			$arResult["SETS"][$N]["PREVIEW_PICTURE"] = CFile::GetPath($arItem["PREVIEW_PICTURE"]);
			$N++;
			?>
		<?endforeach;?>
	<?endforeach;?>
<?endif;?>

<?if(is_array($arResult["PROPERTIES"]["MORE_PRODUCT"]["VALUE"])):?>
	<?$arResult['NEED_MORE'] = true?>
<?foreach($arResult["PROPERTIES"]["MORE_PRODUCT"]["VALUE"] as $ID=>$arMore): ?>

<?CModule::IncludeModule('iblock');
$get_MORE = CIBlockElement::GetByID($arMore); 
$more = $get_MORE->GetNext();
?>

<?
$arResult["PROPERTIES"]["MORE_PRODUCT"]["VALUES"][$ID]["ID"] = $more["ID"];
$arResult["PROPERTIES"]["MORE_PRODUCT"]["VALUES"][$ID]["NAME"] = $more["NAME"];
$arResult["PROPERTIES"]["MORE_PRODUCT"]["VALUES"][$ID]["PREVIEW_PICTURE"] = CFile::GetPath($more["PREVIEW_PICTURE"]);
$arResult["PROPERTIES"]["MORE_PRODUCT"]["VALUES"][$ID]["DETAIL_PAGE_URL"] = $more["DETAIL_PAGE_URL"];
?>

<?endforeach;?>
<?else:?>
	<?$arResult['NEED_MORE'] = false?>
<?endif?>

<?
$sect = CIBlockSection::GetList(
    Array("sort"=>"asc", 'name'=>'asc'), 
    Array(
        'IBLOCK_ID'=>$arResult['IBLOCK_ID'], 
        'ID'=>$arResult["SECTION"]['ID'],
        'GLOBAL_ACTIVE'=>"Y",
        'CNT_ACTIVE'=>true
    ), 
    true, 
    array()
);
while($el = $sect->Fetch()):
    $count += $el["ELEMENT_CNT"];
endwhile;
$arResult["SECTION"]['ELEMENT_CNT'] = $count;

?>


<?
$rsLIST = CIBlockElement::GetList (
   Array("SORT"=>"ASC"), // сортировка
   Array("IBLOCK_ID" => $arResult['IBLOCK_ID'], "!ID" => $arResult['ID'], "!PREVIEW_PICTURE" => false), //ID инфоблока
   false,
   Array ("nTopCount" => 20), // количество
   Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL")  //список полей. IBLOCK_ID и ID - обязательны.
);
$K="0";
?>
<?while($all = $rsLIST-> GetNext()):?>
<?
	$arResult["SECTION"]["VALUES"][$K]["NAME"] = $all["NAME"];
	$arResult["SECTION"]["VALUES"][$K]["PREVIEW_PICTURE"] = CFile::GetPath($all["PREVIEW_PICTURE"]);
	$arResult["SECTION"]["VALUES"][$K]["DETAIL_PAGE_URL"] = $all["DETAIL_PAGE_URL"];
	$K++;
?>
<?endwhile;?>




<?
CModule::IncludeModule('iblock');
$getSection = CIBlockSection::GetList (
		Array("SORT"=>"ASC"), // сортировка
		Array("IBLOCK_ID" => $arResult['IBLOCK_ID'], "ID" => $arResult["SECTION"]['ID']), //ID инфоблока
		true, // считать ли количество вложенных элементов
		Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT", "PICTURE", "SECTION_PAGE_URL", "UF_MORE_CAT"), // фильтр
		false
	);

$n="0";
?>
<?while($all = $getSection-> GetNext()):?>

	<?foreach($all["UF_MORE_CAT"] as $arItem): ?>
	
		<?CModule::IncludeModule('iblock');
		$res1 = CIBlockSection::GetByID($arItem); //получим всё об элементе, который рулит слайдером
		$obRes1 = $res1->GetNext(); 
		?>
		
		<?
		$arResult["SECTION"]["MORE_CAT"][$n]["NAME"] = $obRes1["NAME"];
		$arResult["SECTION"]["MORE_CAT"][$n]["PICTURE"] = CFile::GetPath($obRes1["PICTURE"]);
		$arResult["SECTION"]["MORE_CAT"][$n]["URL"] = $obRes1["SECTION_PAGE_URL"];
		$arResult["SECTION"]["MORE_CAT"][$n]["DESCR"] = $obRes1["PREVIEW_TEXT"];
		$n++;
		
		?>
	<?endforeach;?>

<?endwhile;?>
<?shuffle($arResult["SECTION"]["MORE_CAT"]);?>


