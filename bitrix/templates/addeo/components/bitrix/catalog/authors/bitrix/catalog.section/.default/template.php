<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container">
	<div class="tab-content row">
		<div class="fade in">
			<?foreach ($arResult['ITEMS'] as $key => $arItem):?>
						<div class="author-block">
		                  <div class="row">
		                    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="">
		                    <h2><?=$arItem["NAME"];?></h2>
		                    <h3><?=$arItem["PREVIEW_TEXT"];?></h3>
		                  </div>
		                  <p><?=$arItem["DETAIL_TEXT"];?></p>
		                  <?if($arItem["PROPERTIES"]["LINK"]["VALUE"]):?>
		                  	<a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"];?>">
		                  		<button class="btn-author"><?=GetMessage("MORE");?></button>
		                  	</a>
		                  <?endif;?>
							<a class="hide_more_text" href="#">Развернуть</a>
		                </div>

			<?endforeach;?>
		</div>
	</div>
</div>
<div class="pagination">
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<br /><?=$arResult["NAV_STRING"]?>
	<?endif;?>
</div>