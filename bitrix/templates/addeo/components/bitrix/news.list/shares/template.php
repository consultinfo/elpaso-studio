<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
		<div class="shares-block row">
            <div class="share-img-wrap">
              <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="">

              <?if($arItem["PROPERTIES"]["DISCOUNT"]["VALUE"]):?>
	              <img class="img-price-off" src="<?=SITE_TEMPLATE_PATH;?>/img/priceoff.png" alt="">
	              <p><?=$arItem["PROPERTIES"]["DISCOUNT"]["VALUE"];?></p>
              <?endif;?>
            </div>
            <div class="shares-right">
              <?if($arItem["PROPERTIES"]["TIME"]["VALUE"]):?>
              	<h4><?=$arItem["PROPERTIES"]["TIME"]["VALUE"]?></h4>
              <?endif;?>
              <h3><?=$arItem["NAME"];?></h3>
              <p><?=$arItem["PREVIEW_TEXT"];?></p>
              <?if($arItem["PROPERTIES"]["LINK"]["VALUE"]):?>
              	<a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"];?>"><button class="btn-shares"><?=GetMessage("MORE");?></button></a>
              <?endif;?>
            </div>
          </div>
<?endforeach;?>
<div class="pagination">
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
