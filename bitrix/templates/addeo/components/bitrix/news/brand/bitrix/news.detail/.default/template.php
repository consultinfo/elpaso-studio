<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section id="brand-page">
        <div class="container">
	        <h1><?=$arResult["NAME"];?></h1>
	        <p><?=$arResult["PREVIEW_TEXT"];?></p>
			<?=$arResult["DETAIL_TEXT"];?>
			<a class="btn-brand-all" href="<?=$arResult["PROPERTIES"]["LINK"]["VALUE"];?>">Смотреть все товары бренда</a>
        </div>
</section>
<?if($arResult["PROPERTIES"]["COL"]["VALUE"]):?>
<section id="f-col">
        <div class="container">
	          <h2>Коллекции фабрики</h2>
	          <?foreach($arResult["PROPERTIES"]["COL"]["VALUE"] as $arItem): ?>
	          	<?CModule::IncludeModule('iblock');
	          	$res1 = CIBlockElement::GetByID($arItem); //получим всё об элементе, который рулит слайдером
	          	$obRes1 = $res1->GetNext();
	          	?>
	          	  <div class="f-col-block">
		            <img src="<?=CFile::GetPath($obRes1["PREVIEW_PICTURE"]);?>" alt="">
		            <h3><?=$obRes1["NAME"];?></h3>
		            <p><?=$obRes1["PREVIEW_TEXT"];?></p>
		          </div>
	          <?endforeach;?>

        </div>
</section>
<?endif;?>
