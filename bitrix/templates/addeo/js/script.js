// разбивка суммы на пробелы

if ( $('div').is('.look-right') ){
    var sum = $('.look-right h2').html();
    sum = sum.replace(/(\d{1,3})(?=((\d{3})*([^\d]|$)))/g, " $1 ");
    $('.look-right h2').html(sum);
}

if ( $('div').is('.wishlist-block') ){

    $('.wishlist-block').each(function(){

        var sum = $(this).find('p.price').html();
        sum = sum.replace(/(\d{1,3})(?=((\d{3})*([^\d]|$)))/g, " $1 ");
        $(this).find('p.price').html(sum);

    });

}

if ( $('div').is('.trend-block') ){

    $('.trend-block').each(function(){

        var sum = $(this).find('h4').html();
        sum = sum.replace(/(\d{1,3})(?=((\d{3})*([^\d]|$)))/g, " $1 ");
        $(this).find('h4').html(sum);

    });

}



$('.owl-header').owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    dots: true,
    items: 1
});


$('.ltl').owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    navText: '',
    dots: true,
    items: 1
});

$('.lb').owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    navText: '',
    dots: true,
    items: 1
});

$('.top-right').owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    navText: '',
    dots: true,
    items: 1
});

$('.owl-banner').owlCarousel({
    loop: false,
    margin: 0,
    nav: true,
    navText: '',
    dots: true,
    items: 1,
    autoplay: true
});

$('.owl-header .owl-controls').append('<div class="container" />');
$('.owl-header .owl-dots').appendTo('.owl-header .owl-controls .container');

$('.owl-news').owlCarousel({
    loop: false,
    margin: 0,
    nav: true,
    responsive:{
        0:{
            items:1,
            center: true,
            stagePadding: 10,
            loop: true
        },
        480:{
            items:1,
            center: true,
            stagePadding: 90,
            loop: true
        },
        768:{
            items:3
        }
    }
});

$('.owl-news').hover(
    function () {
        $(this).find('.owl-prev').css({'display' : 'block', 'opacity' : '1'});
        $(this).find('.owl-next').css({'display' : 'block', 'opacity' : '1'});
    },
    function () {
        $(this).find('.owl-prev').css({'display' : 'none', 'opacity' : '0'});
        $(this).find('.owl-next').css({'display' : 'none', 'opacity' : '0'});
    }
);

$('.tab-block').hover(
    function () {
        $(this).find('.img-bac').css({'box-shadow' : '4px 4px 10px rgba(0,0,0,0.36)', 'opacity' : '0.5'});
    },
    function () {
        $(this).find('.img-bac').css({'box-shadow' : 'none', 'opacity' : '1'});
    }
);

$('.owl-steps').owlCarousel({
    loop: false,
    margin: 0,
    nav: true,
    dots: false,
    items: 1,
    URLhashListener:true,
    startPosition: 'URLHash'
});

$('.owl-steps .owl-controls').append('<div class="container" />');
$('.owl-steps .owl-nav').appendTo('.owl-steps .owl-controls .container');

$('.steps-dots a').click(function(){
    $('.steps-dots').find('a.active').removeClass('active');
    $(this).addClass('active');
});

$('.owl-steps').on('changed.owl.carousel', function(event) {
    var currentStep = event.item.index;
    $('.steps-dots').find('a.active').removeClass('active');
    $('.steps-dots a').eq(currentStep).addClass('active');
})

$('.owl-hits').owlCarousel({
    loop: false,
    margin: 10,
    nav: true,
    dots: false,
    responsive:{
        0:{
            items:1,
            stagePadding: 30,
            dots: true
        },
        480:{
            items:1,
            stagePadding: 70
        },
        768:{
            items:4
        }
    }
});

$('.owl-hits').hover(
    function () {
        $(this).find('.owl-prev').css({'display' : 'block', 'opacity' : '1'});
        $(this).find('.owl-next').css({'display' : 'block', 'opacity' : '1'});
    },
    function () {
        $(this).find('.owl-prev').css({'display' : 'block', 'opacity' : '0'});
        $(this).find('.owl-next').css({'display' : 'block', 'opacity' : '0'});
    }
);

$('.trend-block').hover(
    function () {
        $(this).find('.trend-hover-img').css({'display' : 'block', 'opacity' : '1'});
    },
    function () {
        $(this).find('.trend-hover-img').css({'display' : 'block', 'opacity' : '0'});
    }
);

$('.owl-look-big').owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    items: 1,
    URLhashListener:true,
    startPosition: 'URLHash',
    responsive:{
        0:{
            dots: true
        },
        768:{
            dots: false
        }
    }
});

$('.look-left-small a').click(function(){
    $('.look-left-small').find('a.active').removeClass('active');
    $(this).addClass('active');
});

$('.owl-look-book').owlCarousel({
    loop: false,
    margin: 30,
    nav: true,
    dots: false,
    responsive:{
        0:{
            items:1,
            dots: true
        },
        480:{
            items:2,
            dots: true
        },
        992:{
            items:4
        }
    },
    URLhashListener:true,
    startPosition: 'URLHash'
});

$('.owl-look-book').hover(
    function () {
        $(this).find('.owl-prev').css({'display' : 'block', 'opacity' : '1'});
        $(this).find('.owl-next').css({'display' : 'block', 'opacity' : '1'});
    },
    function () {
        $(this).find('.owl-prev').css({'display' : 'none', 'opacity' : '0'});
        $(this).find('.owl-next').css({'display' : 'none', 'opacity' : '0'});
    }
);

$('.look-book-block').hover(
    function () {
        $(this).find('.look-book-hover').css({'display' : 'block', 'opacity' : '1'});
    },
    function () {
        $(this).find('.look-book-hover').css({'display' : 'block', 'opacity' : '0'});
    }
);

$('.owl-also-left').owlCarousel({
    loop: false,
    margin: 30,
    nav: true,
    dots: false,
    responsive:{
        0:{
            items: 1,
            stagePadding: 50
        },
        768:{
            items: 3
        }
    }
});

$('.owl-also-left').hover(
    function () {
        $(this).find('.owl-prev').css({'display' : 'block', 'opacity' : '1'});
        $(this).find('.owl-next').css({'display' : 'block', 'opacity' : '1'});
    },
    function () {
        $(this).find('.owl-prev').css({'display' : 'none', 'opacity' : '0'});
        $(this).find('.owl-next').css({'display' : 'none', 'opacity' : '0'});
    }
);
$('.owl-also-right.new').owlCarousel({
    loop: false,
    margin: 30,
    nav: true,
    dots: false,
    responsive:{
        0:{
            items: 1,
            stagePadding: 50
        },
        480:{
            items: 3
        },
        996:{
            items: 6
        }
    }
});
$('.owl-also-right').owlCarousel({
    loop: false,
    margin: 30,
    nav: true,
    dots: false,
    responsive:{
        0:{
            items: 1,
            stagePadding: 50
        },
        768:{
            items: 3
        }
    }
});


$('.owl-also-right').hover(
    function () {
        $(this).find('.owl-prev').css({'display' : 'block', 'opacity' : '1'});
        $(this).find('.owl-next').css({'display' : 'block', 'opacity' : '1'});
    },
    function () {
        $(this).find('.owl-prev').css({'display' : 'none', 'opacity' : '0'});
        $(this).find('.owl-next').css({'display' : 'none', 'opacity' : '0'});
    }
);

$('.author-block').hover(
    function () {
        $(this).find('.btn-author').css({'background-color' : '#42304F' , 'color' : '#fff', 'box-shadow' : '4px 4px 11px rgba(0,0,0,0.32)'});
        $(this).find('h2').css({'text-decoration' : 'underline', 'color' : '#42304f'});
    },
    function () {
        $(this).find('.btn-author').css({'background-color' : 'transparent', 'color' : '#42304f', 'box-shadow' : 'none'});
        $(this).find('h2').css({'text-decoration' : 'none', 'color' : '#000'});
    }
);

$('.blog-block').hover(
    function () {
        $(this).find('h2').css({'color' : '#83304d'});
        $(this).find('img').css({'box-shadow' : '4px 4px 10px rgba(0,0,0,0.36)', 'opacity' : '0.5'});
    },
    function () {
        $(this).find('h2').css({'color' : '#000'});
        $(this).find('img').css({'box-shadow' : 'none', 'opacity' : '1'});
    }
);

$('.brand-block').hover(
    function () {
        $(this).find('h2').css({'color' : '#83304d'});
        $(this).find('img').css({'opacity' : '0.7'});
    },
    function () {
        $(this).find('h2').css({'color' : '#000'});
        $(this).find('img').css({'opacity' : '1'});
    }
);

$('.btn-vacancy').click(function(){
    if ( $(this).hasClass('active') ) {
        $(this).parent('.vacancy-block').children('.vacancy-hidden').fadeOut();
        $(this).html('Подробнее').removeClass('active');
    }
    else {
        $(this).parent('.vacancy-block').children('.vacancy-hidden').fadeIn();
        $(this).html('Закрыть').addClass('active');
    }
});

$('.wishlist-block').hover(
    function () {
        $(this).css({'box-shadow' : '4px 4px 15px rgba(0,0,0,0.13)'});
        $(this).find('.wishlist-hover').css({'display' : 'block', 'opacity' : '1'});
    },
    function () {
        $(this).css({'box-shadow' : 'none'});
        $(this).find('.wishlist-hover').css({'display' : 'block', 'opacity' : '0'});
    }
);

$('.catalog-all-block-title').hover(
    function () {
        $(this).css({'box-shadow' : '3px 3px 7px rgba(0,0,0,0.75)'});
    },
    function () {
        $(this).css({'box-shadow' : 'none'});
    }
);

$('.catalog-left > .collapse > a').click(function(){
    if ( $(this).attr('aria-expanded') == 'false' ){
        $(this).css({'color' : '#83304d', 'font-family' : '"AcromMedium"'});
    }
    else{
        $(this).css({'color' : '#000', 'font-family' : '"AcromRegular"'});
    }
});

$('.catalog-left a').click(function(){
    if ( $(this).find('img').attr('src') == '/bitrix/templates/addeo/img/plus-cat.png' ){
        $(this).find('img').attr('src', $(this).find('img').attr('src').replace('plus', 'minus'));
    }
    else{
        $(this).find('img').attr('src', $(this).find('img').attr('src').replace('minus', 'plus'));
    }
});

$('.owl-catalog-slider').owlCarousel({
    loop: false,
    margin: 0,
    nav: true,
    items: 1
});
$('.owl-catalog-slider').hover(
    function () {
        $(this).find('.owl-prev').css({'display' : 'block', 'opacity' : '0.7'});
        $(this).find('.owl-next').css({'display' : 'block', 'opacity' : '0.7'});
    },
    function () {
        $(this).find('.owl-prev').css({'display' : 'block', 'opacity' : '0'});
        $(this).find('.owl-next').css({'display' : 'block', 'opacity' : '0'});
    }
);

$('.owl-catalog-slider .owl-next').hover(
    function () {
        $(this).css({'opacity' : '1'});
    },
    function () {
        $(this).css({'opacity' : '0.7'});
    }
);
$('.owl-catalog-slider .owl-prev').hover(
    function () {
        $(this).css({'opacity' : '1'});
    },
    function () {
        $(this).css({'opacity' : '0.7'});
    }
);

$('.portfolio-block').hover(
    function () {
        $(this).find('.img-bac').css({'opacity' : '0.5'});
    },
    function () {
        $(this).find('.img-bac').css({'opacity' : '1'});
    }
);

$('.owl-portfolio').owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    items: 1
});

$('.owl-portfolio').hover(
    function () {
        $(this).find('.owl-prev').css({'display' : 'block', 'opacity' : '1'});
        $(this).find('.owl-next').css({'display' : 'block', 'opacity' : '1'});
    },
    function () {
        $(this).find('.owl-prev').css({'display' : 'block', 'opacity' : '0'});
        $(this).find('.owl-next').css({'display' : 'block', 'opacity' : '0'});
    }
);

$('.dropdown-toggle').click(function(){
    $('body').addClass('modal-open');
    $('.mobile-menu').fadeIn();
});

$('.menu-img-search').click(function(){
    $('body').addClass('modal-open');
    $('.mobile-menu').fadeIn();
});

$('.close-menu').click(function(){
    $('body').removeClass('modal-open');
    $('.mobile-menu').fadeOut();
});

$(window).on('load resize',function(){
    if ( window.innerWidth < 768 );
});

$('.owl-service').owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    dots: true,
    responsive:{
        0:{
            items: 1
        }
    }
});

$('.owl-order').owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    dots: true,
    responsive:{
        0:{
            items: 1
        }
    }
});

$('.owl-advantages').owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    dots: true,
    responsive:{
        0:{
            items: 1
        }
    }
});

$('.owl-news .owl-item').hover(
    function () {
        $(this).find('img').css({'opacity' : '0.6'});
        $(this).find('h3').css({'color' : '#9a2d4d'});
    },
    function () {
        $(this).find('img').css({'opacity' : '1'});
        $(this).find('h3').css({'color' : '#000'});
    }
);

$('.banner-right-block').hover(
    function () {
        $(this).find('h2').css({'bottom' : '65px', 'opacity' : '1'});
        $(this).find('p').css({'bottom' : '21px', 'opacity' : '1'});
        $(this).find('.black-bg').fadeIn();
    },
    function () {
        $(this).find('h2').css({'bottom' : '60px', 'opacity' : '1'});
        $(this).find('p').css({'bottom' : '16px', 'opacity' : '1'});
        $(this).find('.black-bg').fadeOut();
    }
);

$('.owl-hits .item').hover(
    function () {
        $(this).css({'box-shadow' : '4px 4px 15px rgba(0,0,0,0.13)'});
        $(this).find('.wishlist-hover').css({'display' : 'block', 'opacity' : '1'});
    },
    function () {
        $(this).css({'box-shadow' : 'none'});
        $(this).find('.wishlist-hover').css({'display' : 'block', 'opacity' : '0'});
    }
);

$('.owl-also-left .item').hover(
    function () {
        $(this).css({'box-shadow' : '4px 4px 15px rgba(0,0,0,0.13)'});
    },
    function () {
        $(this).css({'box-shadow' : 'none'});
    }
);

$('.owl-also-right .item').hover(
    function () {
        $(this).css({'box-shadow' : '4px 4px 15px rgba(0,0,0,0.13)'});
    },
    function () {
        $(this).css({'box-shadow' : 'none'});
    }
);

$('#already-look img').hover(
    function () {
        $(this).css({'box-shadow' : '4px 4px 15px rgba(0,0,0,0.13)'});
    },
    function () {
        $(this).css({'box-shadow' : 'none'});
    }
);

$('.also-article-block').hover(
    function () {
        $(this).find('img').css({'opacity' : '0.7'});
        $(this).find('h4').css({'color' : '#83304d'});
    },
    function () {
        $(this).find('img').css({'opacity' : '1'});
        $(this).find('h4').css({'color' : '#000'});
    }
);






$('.modal-close').click(function() {
    $('.modal-wrap').fadeOut();
    $('body').removeClass('modal-open');
});

$('.login_in').click(function(){
    $('.modal-wrap-registration').fadeOut();
    $('.modal-wrap-login').fadeIn();
    $('body').addClass('modal-open');
});

$('.reg_in').click(function(){
    $('.modal-wrap-login').fadeOut();
    $('.modal-wrap-registration').fadeIn();
    $('body').addClass('modal-open');
});

$('.expert-button').click(function(){
    $('.modal-wrap-expert').fadeIn();
    $('body').addClass('modal-open');
});


$('.questions_button').click(function(){
    $('.modal-wrap-callback').fadeIn();
    $('body').addClass('modal-open');
});




$('.registration_ajax').click(function(){
    // собираем данные с формы
    var name    = $('#user_name').val();
    var email    = $('#user_email').val();
    var pass    = $('#user_pass').val();

    // отправляем данные
    $.ajax({
        url: "/ajax/registration.php", // куда отправляем
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        data: { // что отправляем
            "name":    name,
            "email":    email,
            "pass":    pass
        },
        // после получения ответа сервера
        success: function(data){
            $('.messages').html(data.result);
            if(data.reload){
                location.reload();
            }
        }

    });
    location.reload();
});



$('.form_ajax').click(function(){
    // собираем данные с формы
    var name    = $('#form_name').val();
    var phone    = $('#form_phone').val();
    // отправляем данные
    $.ajax({
        url: "/ajax/form.php", // куда отправляем
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        data: { // что отправляем
            "name":    name,
            "phone":    phone
        },
        // после получения ответа сервера
        success: function(data){
            $('.messages2').html(data.result);
        }

    });
});

$('.expert_ajax').click(function(){
    // собираем данные с формы
    var name    = $('#expert_name').val();
    var phone    = $('#expert_phone').val();
    var comment    = $('#expert_comment').val();
    // отправляем данные
    $.ajax({
        url: "/ajax/expert.php", // куда отправляем
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        data: { // что отправляем
            "name":    name,
            "phone":    phone,
            "comment":    comment
        },
        // после получения ответа сервера
        success: function(data){
            $('.messages2').html(data.result);
        }

    });
});


$('.wishlist').click(function(){
    // собираем данные с формы

    if( $(this).hasClass('active') ){
        var type = "1";
        $(this).removeClass("active");
    }else{
        var type = "0";
        $(this).addClass("active");
    }
    var id = $(this).attr("data_id");



    // отправляем данные
    $.ajax({
        url: "/ajax/wishlist.php", // куда отправляем
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        data: { // что отправляем
            "type"  :  type,
            "id"    :  id
        }

    });


});




$('.cart-delete').click(function(){
    // собираем данные с формы
    var id = $(this).attr("data-id");
    var count    = $('#count_product').html() - 1;
    var price    = parseFloat($(this).attr("data-price"));
    var quantity = parseFloat($("#quantity"+id).val());
    var all_price    = parseFloat($('#all_price').html()) - (price*quantity);
    // отправляем данные
    $.ajax({
        url: "/ajax/cart_delete.php", // куда отправляем
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        data: { // что отправляем
            "id"    :  id
        }

    });
    $('#id'+id).remove();
    if($('.cart-block').length < 1){
        location.reload();
    }
    $('#count_product').html(count);
    $('#all_price').html(all_price);


});


$('.cart_update').click(function(){
    // собираем данные с формы
    var id = $(this).attr("data-id");
    var price    = parseFloat($(this).attr("data-price"));
    var quantity = parseFloat($("#quantity"+id).val());
    var quantity_max = parseFloat($("#quantity"+id).attr("max-quantity"));


    if($(this).hasClass("btn-cart-minus")){
        if(quantity > 1){
            var new_quantity = quantity - 1;
            var all_price    = parseFloat($('#all_price').html()) - price;
            $('#all_price').html(all_price);
            // отправляем данные
            $.ajax({
                url: "/ajax/cart_update.php", // куда отправляем
                type: "post", // метод передачи
                dataType: "json", // тип передачи данных
                data: { // что отправляем
                    "id"    :  id,
                    "new_quantity"    :  new_quantity
                }

            });
        }else{
            var new_quantity = quantity;
        }
    }

    if($(this).hasClass("btn-cart-plus")){
        if(quantity < quantity_max){
            var new_quantity = quantity + 1;
            var all_price    = parseFloat($('#all_price').html()) + price;
            $('#all_price').html(all_price);
            // отправляем данные
            $.ajax({
                url: "/ajax/cart_update.php", // куда отправляем
                type: "post", // метод передачи
                dataType: "json", // тип передачи данных
                data: { // что отправляем
                    "id"    :  id,
                    "new_quantity"    :  new_quantity
                }

            });
        }else{
            var new_quantity = quantity;
        }
    }

    $("#quantity"+id).attr('value', new_quantity);
    $("#quantity_text"+id).html(new_quantity);




});

$('#OrderIsSite').on('submit', function(e){

    e.preventDefault();
    var $that = $(this),
    formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
    $.ajax({
        url: $that.attr('action'),
        type: $that.attr('method'),
        contentType: false, // важно - убираем форматирование данных по умолчанию
        processData: false, // важно - убираем преобразование строк по умолчанию
        data: formData,
        dataType: 'json',
        success: function(json){

            if(json.yes == "1"){
                $('.all_page').html(json.result);
            }else {
                $('.error_form').html(json.result);
                var target = ".error_form";
                $('html, body').animate({scrollTop: $(target).offset().top}, 800);
                return false;
            }
            //$('.error_forms').html(json.results);
        }
    });
});


/* фильтры */

$('.catalog-filter > .row').click(function(){

    if ( $('.catalog-filter').hasClass('active') ){
        $('.catalog-filter-open').fadeOut();
        $('.catalog-filter').removeClass('active');
    }
    else{
        $('.catalog-filter-open').fadeIn();
        $('.catalog-filter').addClass('active');
    }

});

$('.catalog-filter-open-block').each(function(){

    if( $(this).find('li').length > 5 ){
        for ( var i = 5; i <= $(this).find('li').length; i++ ){
            $(this).find('li').eq(i).css('display', 'none').addClass('filter-hidden');
        }
        $(this).find('ul').append('<li class="filter-more"><a>Показать больше</a></li')
    }

});

$('.filter-more').click(function(){

    if ( $(this).hasClass('active') ){

        $(this).html('<a>Показать больше</a>').removeClass('active');

        $(this).closest('ul').find('li.filter-hidden').each(function(){
            $(this).fadeOut();
        });

    }
    else{

        $(this).html('<a>Скрыть</a>').addClass('active');

        $(this).closest('ul').find('li.filter-hidden').each(function(){
            $(this).fadeIn();
        });

    }

});

$('.catalog-filter-open-block-collection ul li').each(function(){

    if( !$(this).hasClass('brand-first') ){
        $(this).css('display', 'none');
    }

});

var dataBrand;
var dataCollection = [];
var i;

$('.catalog-filter-open-block-brand ul li label').click(function(){

    var collectionEmpty = 0;

    dataBrand = $(this).closest('li').find('input').attr('data-brand-main');

    if ( $(this).closest('li').find('input').is(':checked') ){

        $(this).closest('li').find('input').prop('checked', true);
        $('.catalog-filter-open-block-collection ul li').each(function(){

            if ( !$(this).hasClass('brand-first') ){

                dataCollection = $(this).attr('data-brand').split(',');

                for ( i = 0; i < dataCollection.length; i++ ) {

                    if ( dataBrand == dataCollection[i] ){
                        $(this).css('display', 'none');
                    }

                }
            }

        });

        $('.catalog-filter-open-block-collection ul li').each(function(){

            if ( !$(this).hasClass('brand-first') && $(this).is(":visible") ){
                collectionEmpty = 1;
            }

        });

        if (collectionEmpty == 0){
            $('.brand-first').fadeIn();
        }

    }
    else{

        $(this).closest('li').find('input').prop('checked', false);
        $('.catalog-filter-open-block-collection ul li').each(function(){

            if ( !$(this).hasClass('brand-first') ){

                dataCollection = $(this).attr('data-brand').split(',');

                for ( i = 0; i < dataCollection.length; i++ ) {

                    if ( dataBrand == dataCollection[i] ){
                        $(this).fadeIn();
                        $(this).find('input').prop('checked', false);
                    }

                }
            }

        });

        $('.brand-first').css('display', 'none');

    }

});


$('.check input[type=checkbox]').click(function(){
    if($(this).prop('checked')){
        var id = $(this).attr("id");
        var section_code = $(this).attr("section_code");
        var smart_filter = $(this).attr("smart_filter");
        var filter = $(this).attr("filter");
        if(smart_filter == ""){
            var new_filter = filter;
        }else{
            var new_filter = smart_filter+"/"+filter;
        }

        location.href = "/catalog/"+section_code+"/filter/"+new_filter+"/apply/";
    }else {
        if($(this).attr("smart_filter") || $(this).attr("smart_filter") == "clear"){
            var section_code = $(this).attr("section_code");
            var smart_filter = $(this).attr("smart_filter");
            var filter = $(this).attr("filter");
            var new_filter = smart_filter.replace(filter, '');
            if(!new_filter){
                new_filter = "clear";
            }
            location.href = "/catalog/"+section_code+"/filter/"+new_filter+"/apply/";
        }else{
            var section_code = $(this).attr("section_code");
            location.href = "/catalog/"+section_code+"/filter/clear/apply/";
        }
    }
});

$('.hide_more_text').click(function(){

    if( $(this).hasClass('open') ){
        $(this).removeClass('open').html('Развернуть');
        $(this).closest('.author-block').find('p').css('height', '300px');
    }
    else{
        $(this).addClass('open').html('Свернуть');
        $(this).closest('.author-block').find('p').css('height', 'auto');
    }

    return false;

});

// цена по запросу в карточке товара

if( $('div').is('.look-right') ){

    var cardPrice = $('.look-right h2').html();
    cardPrice = cardPrice.replace(/[^-0-9]/gim,'');

    if(cardPrice == 0){

        $('.look-right h2').hide();
        $('.btn-add-cart').html('Цена по запросу');
        $('.btn-add-cart').closest('a').removeAttr('href');
        $('.btn-add-cart').closest('a').css('cursor', 'default');
        $('.btn-add-cart').css('cursor', 'default');

    }

}

// цена по запросу в каталоге

if( $('div').is('.wishlist-block') ){

    $('.wishlist-block').each(function(){

        var catalogPrice = $(this).find('p.price').html();
        catalogPrice = catalogPrice.replace(/[^-0-9]/gim,'');

        if(catalogPrice == 0){

            $(this).find('p.price').html('Цена по запросу');

        }


    });

}

// поиск

$(document).mouseup(function (e){
    if (!$('.navbar-top-search').is(e.target) && $('.navbar-top-search').has(e.target).length === 0) {
        $('.search-block').fadeOut();
    }
});

$(document).mouseup(function (e){
    if (!$('.mobile-menu-search').is(e.target) && $('.mobile-menu-search').has(e.target).length === 0) {
        $('mobile-menu-search .search-block').fadeOut();
    }
});


if ( $(window).width() > 1200 ){

    $('.navbar-top-search input[type="search"]').focus(function(){

        $(this).css('width', '300px');
        $('.navbar-top-right li a').css('padding', '13px 12px');

    });

    $('.navbar-top-search input[type="search"]').focusout(function(){

        if( $(this).val() == '' ){
            $(this).css('width', '200px');
            $('.navbar-top-right li a').css('padding', '13px 20px');
        }

    });

}
else{
    $('.navbar-top-search input[type="search"]').css('width', '160px');
}

// выпадающий список аяксом

$('#desk_search_value').keyup(function()
{

    // нажатие клавиши enter

    if(event.keyCode==13)
    {
        $('#desk_search_send').click();
        return false;
    }

    var term = $('#desk_search_value').val();
    if(term.length < 3) return true;
    $.get("/search/?q="+term, function(data, status)
    {
        $('#desk_search_block').html('<ul id="desk_search_items"></ul>');
        var set = Number($(data).find('#total_items').html());
        if(set > 0) // есть результаты
        {
            var idx = set-1;
            if(idx > 4){
                idx = 4;
            }
            for(var i = 0; i <= idx; i++)
            {
                var title = $(data).find('.itemd-'+i).html();
                var link = $(data).find('.itemd-'+i).attr('href');
                $('#desk_search_items').append("<li><a href='"+link+"'>"+title+"</a></li>");
            }
        }
        else // нет результатов
        {
            $('#desk_search_items').append("<li>Результаты отсутствуют.</li>");
        }
        $('#desk_search_block').fadeIn();
    });
});

// нажатие на кнопку поиска

$('#desk_search_send').click(function(){

    if ( $('#desk_search_value').val() != '' ){

        var term = $('#desk_search_value').val();

        window.location.href = '/search/?q=' + term;

    }

});


$('#mobile_search_value').keyup(function()
{

    // нажатие клавиши enter

    if(event.keyCode==13)
    {
        $('#mobile_search_send').click();
        return false;
    }

    var term = $('#mobile_search_value').val();
    if(term.length < 3) return true;
    $.get("/search/?q="+term, function(data, status)
    {
        $('#mobile_search_block').html('<ul id="mobile_search_items"></ul>');
        var set = Number($(data).find('#total_items').html());
        if(set > 0) // есть результаты
        {
            var idx = set-1;
            if(idx > 4){
                idx = 4;
            }
            for(var i = 0; i <= idx; i++)
            {
                var title = $(data).find('.itemd-'+i).html();
                var link = $(data).find('.itemd-'+i).attr('href');
                $('#mobile_search_items').append("<li><a href='"+link+"'>"+title+"</a></li>");
            }
        }
        else // нет результатов
        {
            $('#mobile_search_items').append("<li>Результаты отсутствуют.</li>");
        }
        $('#mobile_search_block').fadeIn();
    });
});

// нажатие на кнопку поиска

$('#mobile_search_send').click(function(){

    if ( $('#mobile_search_value').val() != '' ){

        var term = $('#mobile_search_value').val();

        window.location.href = '/search/?q=' + term;

    }

});
