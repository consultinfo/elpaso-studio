<?
$MESS["DEF_TEMPLATE_NF"]="Шаблон не найден. Установите шаблон для данного сайта.";
$MESS["DEF_TEMPLATE_NF_SET"]="Установить шаблон";
$MESS["LOGIN"]="Войти";
$MESS["STUDIA"]="Студия";
$MESS["AUTHORS"]="Авторы";
$MESS["CATALOG"]="Каталог";
$MESS["PORTFOLIO"]="Портфолио";
$MESS["ADDEO"]="Дизайн и разработка";
$MESS["CONTACTS"]="Контакты";
$MESS["SLIDER_MORE"]="Смотреть все";
$MESS["REG"]="Регистрация";
$MESS["REG_TEXT"]="Это займёт всего 15 секунд";
$MESS["OR"]="или";
$MESS["NAME"]="Ваше имя";
$MESS["EMAIL"]="Эл. почта";
$MESS["PASS"]="Пароль";
?>
