<?
$MESS["DEF_TEMPLATE_NF"]="Template is not found. Please set template for this site.";
$MESS["DEF_TEMPLATE_NF_SET"]="Set template";
$MESS["LOGIN"]="Login";
$MESS["STUDIA"]="Студия";
$MESS["AUTHORS"]="Authors";
$MESS["CATALOG"]="Catalog";
$MESS["PORTFOLIO"]="Portfolio";
$MESS["ADDEO"]="Design and Development";
$MESS["CONTACTS"]="Contacts";
$MESS["SLIDER_MORE"]="More";
$MESS["REG"]="Registration";
$MESS["REG_TEXT"]="It takes only 15 seconds";
$MESS["OR"]="or";
$MESS["NAME"]="Name";
$MESS["EMAIL"]="Email";
$MESS["PASS"]="Password";
?>