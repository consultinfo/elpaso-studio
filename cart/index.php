<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require($_SERVER["DOCUMENT_ROOT"]."/cart/function.php");
$APPLICATION->SetTitle("Корзина");
?>

<div class="top-links">
        <div class="container">
          		<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","breadcrumb",Array(
					        "START_FROM" => "0", 
					        "PATH" => "", 
					        "SITE_ID" => "s1" 
					    )
					);?>
        </div>
      </div>

      <section id="cart">

        <div class="title-wrap">
          <h1>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => "/include/cart/title.php",
				"EDIT_TEMPLATE" => ""
				),
				false
			);?>
          </h1>
          <p>
          	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => "/include/cart/text.php",
				"EDIT_TEMPLATE" => ""
				),
				false
			);?> 
          </p>
        </div>          


        <div class="cart-wrap">
          <div class="container">

			<?if($count < 1):?>
				<h2 style="color: #9a2d4d; font-weight: bold;">В корзине нет товаров</h2>
			<?else:?>
	            <h2>В корзине <span><strong id="count_product"><?=$count;?></strong> товара</span></h2>
	
	            <div class="cart">
	              <div class="cart-head row">
	                <div class="name">Товар</div>
	                <div class="number">Количество</div>
	                <div class="price">Цена</div>
	              </div>
					
					
					<?foreach($arBasketItems as $arItem): ?>
						  <div class="cart-block row" id="id<?=$arItem["ID"]?>">
			                <img data-price="<?=$arItem["PRICE"];?>" data-id="<?=$arItem["ID"]?>" class="cart-delete" src="<?=SITE_TEMPLATE_PATH;?>/img/close.png" alt="">
			                <div class="cart-block-name">
			                  <div class="cart-block-name-img" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]?>);"></div>
			                  <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><h3><?=$arItem["NAME"]?></h3></a>
			                </div>
			                <div class="cart-block-number">
			                    <button data-price="<?=$arItem["PRICE"]?>" data-id="<?=$arItem["ID"]?>" class="btn-cart-minus cart_update"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart-minus.png" alt=""></button>
			                    <input name="quantity<?=$arItem["ID"]?>" max-quantity="<?//=$arItem["PRODUCT_QUANTITY"];?>100" id="quantity<?=$arItem["ID"]?>" type="hidden" value="<?=$arItem["QUANTITY"]?>" />
			                    <p id="quantity_text<?=$arItem["ID"]?>"><?=$arItem["QUANTITY"]?></p>
			                    <button data-price="<?=$arItem["PRICE"]?>" data-id="<?=$arItem["ID"]?>" class="btn-cart-plus cart_update"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart-plus.png" alt=""></button>
			                </div>
			                
			                <div class="cart-block-price">
			                  	<?=$arItem["PRICE"];?> <span>Р</span> 
			                </div>
			              </div>
					<?endforeach;?>
	
	
	              <div class="cart-footer row">
	                <h5>Итого: <strong id="all_price"><?=$PRICE;?></strong> Р</h5>
	                <a href="/order/"><button class="btn-cart">Оформление заказа</button></a>
	              </div>
	            </div>
            <?endif;?>


          </div>

        </div>
      </section>


		<section id="cart-hit">
        <div class="container">

          <h2>Вам может понравиться</h2>

          <div class="owl-hits">

          <?
          CModule::IncludeModule('iblock');
          $rsHIT = CIBlockElement::GetList (
             Array("SORT"=>"ASC"), // сортировка
             Array("IBLOCK_ID" => 5, "PROPERTY_HIT_VALUE" => "Да"), //ID инфоблока
             false,
             Array ("nTopCount" => 15), // количество
             Array("IBLOCK_ID", "ID", "IBLOCK_SECTION_ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_HIT")  //список полей. IBLOCK_ID и ID - обязательны.
          );
          ?> 
          <?while($arProduct = $rsHIT-> GetNext()):?>
	          <div class="item">
	              <a href="<?=$arProduct["DETAIL_PAGE_URL"]?>">
		              <div class="look-book-block">

		                <div class="look-book-img" style="background-image: url(<?=CFile::GetPath($arProduct["PREVIEW_PICTURE"]);?>);"></div>
			                  
		                <h3><?=$arProduct["NAME"]?></h3>
		                <?$get_SECT = CIBlockSection::GetByID($arProduct["IBLOCK_SECTION_ID"]); 
						$obRes1 = $get_SECT->GetNext(); 
						$arProduct["SECTION"] = $obRes1["NAME"];?>
		                <p><?=$arProduct["SECTION"]?></p>
		                <?
		                /*$Price = CPrice::GetBasePrice($arProduct["ID"]);
						$arProduct["PRICE"] = $Price["PRICE"];*/
						$Price = GetCatalogProductPrice($arProduct["ID"], 1);
						$arProduct["PRICE"] = CCurrencyRates::ConvertCurrency($Price["PRICE"], "EUR", "RUB");
						
						?>
		                <h4><?=ceil($arProduct["PRICE"]);?> <span>Р</span></h4>
		              </div>
	              </a>
	            </div>
          <?endwhile;?>

          

        </div>


        </div>
      </section>
      

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>