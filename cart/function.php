<?php
$arBasketItems = array();

CModule::IncludeModule("sale");
     $dbBasketItems = CSaleBasket::GetList(
     array(
          "NAME" => "ASC",
          "ID" => "ASC"
          ),
     array(
         "FUSER_ID" => CSaleBasket::GetBasketUserID(),
         "LID" => SITE_ID,
         "ORDER_ID" => "NULL"
          ),
        false,
        false,
        array("ID", "PRODUCT_ID", "QUANTITY")
                );

while ($arItems = $dbBasketItems->Fetch())
{
    $arBasketItems[] = $arItems;
}
?>

<?php
$PRICE_ALL = array();
foreach ($arBasketItems as $key => $arProduct) {
	CModule::IncludeModule('iblock');
	$res1 = CIBlockElement::GetByID($arProduct["PRODUCT_ID"]); //получим всё об элементе, который рулит слайдером
	$obRes1 = $res1->GetNext();
	$arBasketItems[$key]["NAME"] = $obRes1["NAME"];
	$arBasketItems[$key]["DETAIL_PAGE_URL"] = $obRes1["DETAIL_PAGE_URL"];
	$base_price[$key] = CPrice::GetBasePrice($obRes1["ID"]);
	if ($base_price[$key]["CURRENCY"] !== "RUB") {
		$arBasketItems[$key]["PRICE"] = ceil(CCurrencyRates::ConvertCurrency($base_price[$key]["PRICE"], "EUR", "RUB"));
	} else {
		$arBasketItems[$key]["PRICE"] = ceil($base_price[$key]['PRICE']);
	}
	$arBasketItems[$key]["PRODUCT_QUANTITY"] = $base_price[$key]["PRODUCT_QUANTITY"];
	$arBasketItems[$key]["PREVIEW_PICTURE"] = CFile::GetPath($obRes1["PREVIEW_PICTURE"]);
	$PRICE_ALL[$key] = ceil($arBasketItems[$key]["PRICE"]) * $arBasketItems[$key]["QUANTITY"];
} ?>


<?php

$i = "0";
foreach($PRICE_ALL as $arPrice){
	if($i == "0"){
		$PRICE = $arPrice;
	}else{
		$PRICE = $PRICE + $arPrice;
	}
	$i++;
}


$count = count($arBasketItems);
?>
