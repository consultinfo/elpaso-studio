<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "История Elpaso");
$APPLICATION->SetPageProperty("keywords", "История Elpaso");
$APPLICATION->SetPageProperty("description", "История Elpaso");
$APPLICATION->SetTitle("history");
?><div class="container">
	<div class="row">
		<div class="col-md-9 col-sm-6 col-xs-12" style="margin-top:14px;">
			<p style="text-align: center;">
 <b><span style="font-size: 16pt; color: #450e61;">Логотип компании Elpaso – две горы в одном ландшафте,&nbsp;</span></b>
			</p>
			<p style="text-align: center;">
 <span style="color: #450e61;"> </span>
			</p>
 <span style="color: #450e61;"> </span>
			<p style="text-align: center;">
 <span style="color: #450e61;"> </span><b><span style="font-size: 16pt; color: #450e61;">а</span></b><b style="text-align: center;"><span style="font-size: 16pt; color: #450e61;">&nbsp;наш слоган:&nbsp;</span></b><span style="font-size: 16pt; color: #450e61;"> </span><span style="color: #450e61;"> </span>
			</p>
 <span style="color: #450e61;"> </span>
			<p style="text-align: center;">
 <span style="color: #450e61;"> </span><span style="font-size: 16pt; color: #450e61;"> </span><span style="color: #450e61;"> </span>
			</p>
 <span style="color: #450e61;"> </span>
			<p style="text-align: center;">
 <span style="color: #450e61;"> </span><span style="font-size: 16pt; color: #450e61;"> </span><b style="text-align: center;"><span style="font-size: 16pt; color: #450e61;">"К ВЕРШИНАМ СТИЛЯ"!</span></b>
			</p>
			<p style="text-align: center;">
 <span style="color: #450e61;"> </span><span style="font-size: 16pt; color: #450e61;"> </span><b style="text-align: center;"><span style="font-size: 16pt; color: #450e61;">Главное стремление команды:</span></b>
			</p>
			<p style="text-align: center;">
 <b style="text-align: center;"><span style="font-size: 16pt; color: #450e61;">подниматься вверх в своей профессии.</span></b>
			</p>
		</div>
 <img width="231" src="/upload/medialibrary/6b9/6b92b41b590f4a9776c334e4438999ab.jpg" class="col-md-3 col-sm-6 col-xs-12" style="display:block; margin:0;" alt="Логотип" title="Логотип">
	</div>
	<p style="text-align: justify;">
 <br>
 <b>Elpaso - это своеобразный мост, соединяющий интересы клиентов&nbsp; России и стран&nbsp;СНГ с фабриками мира. САМЫЙ ЛУЧШИЙ ВИД – С ВЕРШИНЫ ГОРЫ.</b>
	</p>
	<div class="col-md-3 col-sm-4 col-xs-12" style="margin:5px 0;">
 <img width="100%" alt="Ольга Ефремова" src="/upload/medialibrary/254/254dbd27a54753832ac024c5fe78f53e.jpg" style="display:block; width:100%;" title="Ольга Ефремова">
	</div>
	<p style="text-align: justify;">
		 Партнёры и друзья компании Elpaso – люди проверенные временем, мудрые и надёжные, талантливые и способные, много лет успешные и&nbsp;яркие, –&nbsp; самые выдающиеся в своём направлении. У каждого бизнеса, как и у Человека, – своя судьба. Компания ЭЛЬПАСО за годы работы приобрела главный ресурс- своих партнёров, которые судьбоносно повлияли на развитие нашей фирмы.&nbsp;
	</p>
 <br>
	<div class="col-md-4 col-sm-4 col-xs-12" style="margin: 3px 0 3px 5px; float:right;">
 <img width="100%" alt="sotrudnichestvo_6.jpg" src="/upload/medialibrary/7d8/7d80a3408a99545b828904ffa70c58e4.jpg" title="Эмилио Реверт (Испания)и Ольга Ефремова (Россия)" style="display:block; width:100%;">
	</div>
	<p style="text-align: justify;">
		 С момента основания – 2005-ого года, каждый партнёр - это как своя планета в нашей "солнечной системе "ELPASO".&nbsp;"Ваше Солнце – это "ЭЛЬПАСО" - говорил первый водитель фирмы, зная все движения, встречи,&nbsp;цели и планы в адрес директора Ефремовой Ольги.&nbsp;И это так.&nbsp;Именно из большой любви к своему делу, через её "лучи" – поступки и эмоции, притягиваются правильные люди. Сложные периоды каждого бизнеса, если осмыслены верно, – лишь помогают преодолеть себя и пойти дальше, став более сильными.&nbsp;Бойцовские качества тоже важны для взаимовыгодного партнёрства и безопасности соглашений. Чтобы защищать интересы своей команды от манипуляций и давления, избежать неверного курса действий,&nbsp; убеждаем партнёров, аргументируя свои позиции&nbsp;честно и открыто.
	</p>
	<p style="text-align: justify;">
 <br>
 <span style="color: #450e61;"> <b>"МЫ ДОЛЖНЫ ЗАМЕНИТЬ МАНИПУЛИРОВАНИЕ ЛЮДЬМИ АКТИВНЫМ И РАЗУМНЫМ СОТРУДНИЧЕСТВОМ"</b> </span>-&nbsp;применяем этот принцип&nbsp;Эриха Фромма&nbsp;на деле.&nbsp;Сами не поддаёмся манипулированию в сотрудничестве. Манипуляторам и "предпринимателям" односторонних взглядов на выгоду- не к нам.
	</p>
	<p style="text-align: justify;">
		 Когда в нашу компанию&nbsp;приходят из других стран с жёсткими&nbsp;схемами быстро и много закупить, открыть совместные предприятия и за наш счёт шоу-румы, мы тщательно взвешиваем с юристами&nbsp;все риски и отвечаем: "С Россией надо играть честно, либо не играть!". Мы- патриоты своей Великой Страны.&nbsp;&nbsp;Желаем и находим для России самых достойных партнёров с лучшей продукцией в мире.
	</p>
	<div class="col-md-12 col-sm-12 col-xs-12" style="margin:5px 0;">
 <img width="100%" alt="sotrudnichestvo_19f.jpg" src="/upload/medialibrary/6da/6dace25beb130742f8024fcbf85aa569.jpg" title="Под флагами Италии и России президент МВМbiliardi и команда Эльпасо на выставке Крокус Экспо 2017" style="display:block; width:100%;">
	</div>
	<p style="text-align: justify;">
 <span style="text-align: justify;">В компании "ЭЛЬПАСО" научились&nbsp;&nbsp;выбирать,&nbsp;беречь и ценить партнёрские отношения. Всегда надеемся на двухсторонний порядок в этом.&nbsp;От разочарований не останавливаемся.&nbsp;</span>
	</p>
	<p style="text-align: justify;">
		 Создавая красоту интерьеров очень важно любить своё дело, быть преданным ему и в счастливые, и в сложные времена. Самому быть эмоционально тёплым человеком, обладать и вкусом, и чутьём. Это важные качества для привлечения новых направлений бизнеса, предлагая клиентам идеи дизайна, отделку, фактуры, мебель, светильники, красивые ткани- всё лучшее для личных и деловых пространств Жизни.&nbsp;Холодные люди долго не уживаются в&nbsp;команде "ЭЛЬПАСО". Остаются те, кто испытывает настоящую страсть к дизайну, порядку и красоте, создаваемым вместе.
	</p>
	<div class="row">
		<div style="margin: 20px 0px;" class="coop-img-1">
 <img alt="sotrudnichestvo_41.jpg" src="/upload/medialibrary/424/424ecaef3ecc081f2c4218457a15e375.jpg" title="Создаем интерьеры с Coleccion Alexandra и Tecni Nova">
		</div>
		<div style="margin: 20px 0px;" class="coop-img-2">
 <img alt="sotrudnichestvo_151.jpg" src="/upload/medialibrary/7e8/7e833dbbcf17e341c4699bda4211413f.jpg" title=" С коллегами в испанском городе Бурриана и Милане">
		</div>
	</div>
	<p style="text-align: justify;">
 <b><span style="color: #450e61;">Ценители красивых стратегий и последовательных решений - наши партнёры.&nbsp;</span></b>
	</p>
	<p style="text-align: justify;">
		 Первые и фундаментальные из них- это руководители промышленных и банковских предприятий столицы Урала. В Екатеринбурге -&nbsp;1999- ый год -&nbsp; был открыт&nbsp;наш первый салон "Деловая мебель" с экспозицией кабинетов для руководителей от испанской фабрики OfiFran и уральских фабрик "Карат Е" и "Лиал".<span style="text-align: center;">&nbsp;</span>
	</p>
	<div class="tab-block" style="margin: 7px 3px 7px 13px;">
 <img width="100%" alt="sotrudnichestvo_111.jpg" src="/upload/medialibrary/83d/83d643e87b381579ea89d4bd10086cec.jpg" style="width:100%;" title="Руководитель отдела снабжения ЦБРФ со специалистами Эльпасо">
	</div>
	<div class="tab-block" style="margin: 7px 3px;">
 <img width="100%" alt="sotrudnichestvo_113.jpg" src="/upload/medialibrary/53b/53bec4c5ba07e483fabf16cb3236fd9d.jpg" style="width:100%;" title="Уральский губернатор Россель Э.Э. ">
	</div>
	<div class="tab-block" style="margin: 7px 3px;">
 <img width="100%" alt="sotrudnichestvo_112.jpg" src="/upload/medialibrary/2bb/2bbec747d01fe2e2d7dc1c01ebe717aa.jpg" style="width:100%;" title="Визит представителя ЦБРФ к руководству компаний Эльпасо и Ofifran">
	</div>
	<p style="text-align: justify;">
		 Поездка на первую выставку в Испанию&nbsp;Нabitat Valencia в 2000-ом году, в самом начале нового тысячелетия, стала главным вектором правильного выбора партнёров на долгую перспективу сотрудничества.<br>
		 На выставке познакомились и сформировали основу для больших отношений с руководителем московского филиала по обеспечению внутренних нужд <br>
	</p>
	<p style="text-align: justify; margin: 10px;">
 <img alt="Реализованные рекламные идеи Ольги Ефремовой" src="/upload/medialibrary/e76/e76dcf22356e451b27908caf1ffa8d5b.jpg" style="width:100%;" title="Реализованные рекламные идеи Ольги Ефремовой (реальные съёмки) для мебельной фабрики Карат Е с командой дрессировщиков Вольтера Запашного и Воробьёвых , 1998">
	</p>
	<p style="text-align: justify;">
		 ОАО "Мосэнерго" - Товкач Марией Владимировной, владельцами фабрики Coleccion Alexandra -&nbsp;Maria Jose Guinot и её сыновьями, владельцем фабрики OfiFran - Juan Bta. Franco Sanchez и главой российского представительства фабрки OfiFran-&nbsp;Patricio Toribio Villanueva. С OfiFran мы выполнили на Урале и в Ямало-Ненецком округе ряд проектов для предприятий:&nbsp;ОАО «Уралэлектромедь», ОАО «Уралтрансмаш», ОАО «Североуральский новотрубный завод»,&nbsp;ОАО «Верхнесалдинский&nbsp; титановый комбинат», OOO "Газпром трансгаз Екатеринбург", Администрации и Департамента имущества г. Новый Уренгой, БТИ Нового Уренгоя, Управления культуры и других государственных ведомств города Новый Уренгой.&nbsp;Самое начало нашего профессионального пути&nbsp;началось с Уральского федерального округа и в самом центре полярного круга- в городе Салехард , что позволило приобрести хорошую закалку и приступить к работе в столице России с определённым опытом, не "с нуля".&nbsp;
	</p>
	<p style="text-align: justify;">
		 С наиболее мудрыми и честными из первых партнёров мы успешно сотрудничаем по сей день.&nbsp;Некоторые из них стали&nbsp;друзьями, что так же соответствует нашему принципу:<span style="color: #450e61;">&nbsp;<span style="color: #450e61;"><b>"ЛУЧШЕ ДРУЖБА, ОСНОВАННАЯ НА БИЗНЕСЕ, ЧЕМ БИЗНЕС ОСНОВАННЫЙ НА ДРУЖБЕ". </b></span></span><br>
	</p>
	<div class="tab-block" style="margin: 7px 3px 7px 13px;">
 <img width="100%" alt="sotrudnichestvo_332.jpg" src="/upload/medialibrary/fbb/fbb8cb748d9d289414b1519b00442134.jpg" style="width:100%;" title="Ольга Ефремова и владелец фабрики Vicent Montoro">
	</div>
	<div class="tab-block" style="margin: 7px 3px;">
 <img width="100%" alt="sotrudnichestvo_333.jpg" src="/upload/medialibrary/670/670791406ace9666cc178c5b9db2ff64.jpg" style="width:100%;" title="Ефремова Ольга Юрьевна и Товкач Мария Владимировна ТПП Энерготорг (ОАО Мосэнерго)">
	</div>
	<div class="tab-block" style="margin: 7px 3px;">
 <img alt="sotrudnichestvo_331.jpg" src="/upload/medialibrary/e8b/e8b99df57d386db8bccbecab5a974799.jpg" style="width:100%;" title="Зотова Татьяна Александровна ГУП глава АПУ.">
	</div>
	<p style="text-align: justify;">
		 Первые московские партнёры - это акционеры и руководители филиалов ОАО "Мосэнерго"-&nbsp; Салтыков Валерий Михайлович и Товкач Мария Владимировна. Закрепляем им благодарность на этой странице сайта за ту школу и закалку с 2002-ого по 2007 годы сотрудничества в ТПП "Энерготорг".&nbsp;В те годы именно они заложили основу новых партнёрских связей, правил делового этикета и ведения бизнеса для основателя компании "ЭЛЬПАСО". Проведена бесценная по опыту работа в должности руководителя и подчинённого одновременно- директором ведомственного салона "ЭЛИТМЕБЕЛЬ" по адресу: Москва, Зеленодольская 31 и менеджера поставок мебели ТПП "Энерготорг" - филиала ОАО "Мосэнерго". В салоне "ЭЛИТМЕБЕЛЬ" формировалась своя команда, а результаты оценивались руководством филиала. За пятилетний период работы комплектовались&nbsp; мебелью: генеральная дирекция ОАО "Мосэнерго" на Раушской набережной, по заявкам РАО ЕЭС России и&nbsp;ОАО «Мосэнерго» оборудованы мебелью конференц-залы, кинотеатры, гостиницы, концертные залы, кабинеты в здании генеральной дирекции ОАО «Мосэнерго». Выполнены контракты на разработку дизайн-проектов и&nbsp; поставку мебели&nbsp; для бюджетных организаций:&nbsp; Дома правительства РФ, Префектуры ВАО г. Москвы,&nbsp;Главного архитектурно-планировочного управления ЮВАО. Нашими значимыми клиентами стали талантливые управленцы- Зотова Татьяна Александровна - руководитель ГБУ "ГлаваАПУ" тогда, Голованова Наталия Михайловна - глава Управы района&nbsp; Ивановское ВАО, Саликов Валерий Михайлович - заместитель директора ТЭЦ 22&nbsp; ОАО "Мосэнерго"&nbsp; в то время. Мы отмечаем эти имена, - именно&nbsp;они повлияли на становление&nbsp; компании "ЭЛЬПАСО" в Москве и щедро поделились с нашей командой своими связями и опытом, позволили приобрести гораздо больше, чем выполнение заказов. Сработал ещё один&nbsp;постоянный принцип сотрудничества:&nbsp;<span style="color: #450e61;"><b>"НЕ ТОТ ХОРОШ, КТО С ЛЮДЬМИ ДРУЖИТ, А ТОТ, КТО ИМ СЛУЖИТ".</b></span>
	</p>
	<div class="row">
		<div style="margin: 20px 0px;" class="coop-img-1">
 <img alt="estvo_11.jpg" src="/upload/medialibrary/4af/4afa3f8afc5cdc9c58dd8e6b15592729.jpg" title="Барельеф Schuller. Валенсия 2017./ Руководитель строительной компании Конда - Федоренко В.Н (справа) с коллегами">
		</div>
		<div style="margin: 20px 0px;" class="coop-img-2">
 <img alt="3c6bbe7.jpg" src="/upload/medialibrary/d31/d316ca542305c0adc38599b9c7f4a814.jpg" title="Татьяна Сапешко (слева) и Ольга Ефремова (справа) с партнерами фабрики Schuller (Испания,2014) / Время на часах от Schuller,Валенсия 2017 год.">
		</div>
	</div>
	<p style="text-align: justify;">
		 Эти качества и подходы в нашей работе оценил основатель компании "КОНДА" - Василий Николаевич Федоренко- наш стратегический партнёр с 2005-ого года и по-настоящее время. На базе его огромного опыта в делопроизводстве и практики компании "ЭЛЬПАСО" в сфере интерьеров, вместе мы выполнили ряд проектов для государственных корпораций: РОСТЕХНОЛОГИИ, ЦБРФ, Управы района Перово.&nbsp;
	</p>
	<p style="text-align: justify;">
		 С 2011-ого по 2016 годы в нашей команде&nbsp;трудилась&nbsp;талантливая дочь Василия Николаевича -Татьяна Васильевна Сапешко.&nbsp;В эти годы Татьяна Васильевна занималась розничными заказами клиентов. <br>
		 За пять лет сотрудничества и приобретённого партнёрского опыта, полученного в компании "ЭЛЬПАСО", открылось второе независимое направление под маркой компании "КОНДА", интернет-заказы и поставки самостоятельно ведёт Татьяна Сапешко.&nbsp;
	</p>
	<p style="text-align: justify;">
	</p>
	<p style="text-align: justify;">
		 В этом сотрудничестве сработал принцип: <span style="color: #450e61;"><b>"НАШИ ИСТИННЫЕ УЧИТЕЛЯ— ОПЫТ И ЧУВСТВА".&nbsp;</b></span>
	</p>
	<div class="tab-block" style="margin: 7px 3px 7px 13px;">
 <img alt="sotrudnichestvo_221.jpg" src="/upload/medialibrary/0f7/0f71505402c8a4bc2aad1e40a869bde3.jpg" style="width:100%;" title="Арт-менеджер компании Эльпасо Влад Галузинский на съемке интервью с владельцами Coleccion Alexandra.">
	</div>
	<div class="tab-block" style="margin: 7px 3px;">
 <img width="880" alt="sotrudnichestvo_223.jpg" src="/upload/medialibrary/309/3092020bf3d8dd88378aecc1565f9414.jpg" style="width:100%;" title="Ирэн Кузнецова и Ольга Ефремова выбирают арт объекты от Boffi, Италия, 2017 год">
	</div>
	<div class="tab-block" style="margin: 7px 3px;">
 <img width="880" alt="sotrudnichestvo_222.jpg" src="/upload/medialibrary/7be/7be5d98f69ba890a84d84c662dceb698.jpg" style="width:100%;" title="Бильярдный стол Class закрытый крышками в студии интерьеров Эльпасо">
	</div>
	<p style="text-align: justify;">
	</p>
	<p style="text-align: justify;">
		 В&nbsp;команде компании "Эльпасо" приветствуются молодые сотрудники, которые способны работать и расти в проектах по созданию красивых интерьеров, разработке дизайн-проектов, художники, мастера и те специалисты, которые могут принимать участие в&nbsp;Social Media Marketing наших сайтов, сетей, блогов и тематических интернет-ресурсах.&nbsp;
	</p>
	<p style="text-align: justify;">
	</p>
	<p style="text-align: justify;">
 <span style="color: #4b0048;">Компанией&nbsp; </span><b><span style="color: #4b0048;">"Эльпасо"</span></b><span style="color: #4b0048;"> заключён договор о совместной деятельности с Архитектурным бюро </span><b><span style="color: #4b0048;">"На Смоленке"</span></b><span style="color: #4b0048;"> г. Москвы.</span> Мы сотрудничаем с его основателем - <b><span style="color: #450e61;">Святославом Николаевичем Иванченко</span></b> - профессиональным и состоявшимся Архитектором России. Выполнен ряд совместных проектов в <b><span style="color: #450e61;">ЖК "Доминион", ЖК "Мосфильмовский"</span></b><span style="color: #450e61;">,</span><b><span style="color: #450e61;"> ЖК "Соловьиная роща"</span></b><span style="color: #450e61;">&nbsp;,&nbsp;</span><b><span style="color: #450e61;">ЖК "Новоясеневский", реализован ряд идей для государственной корпорации РОСТЕХ и Центрального Банка России</span></b><span style="color: #450e61;">.</span> Обоюдно вкусы,&nbsp;взгляды и подходы к созданию стилистики интерьеров совпадают в сложившейся системе деловых и д<span style="color: #450e61;">оверительных отношений </span><b><span style="color: #450e61;">"Эльпасо"</span></b><span style="color: #450e61;"> и Мастерской&nbsp;</span><b><span style="color: #450e61;">"На Смоленке"</span></b><span style="color: #450e61;">. Наши консультации и переговоры с Заказчиками могут проходить по двум адресам исходя из более&nbsp; территориального&nbsp;удобства встречи: Смоленская площадь (3-4 минуты доступности от метро "Смоленская" в особнячок переулка Каменная Слобода), либо на улице Зеленодольская - в шаговой доступности от метро "Кузьминки").</span><br>
 <span style="color: #450e61;"> </span>
	</p>
 <span style="color: #450e61;"> </span>
	<div class="row" style="margin:10px 0; ">
 <span style="color: #450e61;"> </span>
		<div class="col-md-4 col-sm-4 col-xs-12">
 <span style="color: #450e61;"> </span><img alt="sotrudnichestvo_21.jpg" src="/upload/medialibrary/042/04235c69a277031b2c9da25451630e9b.jpg" title="Благодарная клиентка - Диана С. обнимает директора компании Эльпасо" style="display:block; Width:100%;">
		</div>
		<div style="text-align: center;" class="col-md-4 col-sm-4 col-xs-12">
 <b><span style="font-size: 13pt;"><br>
 </span></b><b><span style="font-size: 12pt; font-family: AcromRegular; color: #450e61;">Созданные нами интерьеры положительно отмечают&nbsp;заказчики, и наши взаимотношения продолжаются после завершения проектов. </span></b><b><span style="font-size: 13pt;"><span style="font-size: 12pt; font-family: AcromRegular; color: #450e61;">Мы поддерживаем общение с клиентами - для нас очень значимо, что многие из них обращаются с&nbsp;повторными заказами для своих новых объектов.</span><br>
 </span></b><b><span style="font-size: 13pt;"><br>
 </span></b>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
 <img alt="sotrudnichestvo_1.jpg" src="/upload/medialibrary/e55/e557aa3975771ea71f7155808800bd34.jpg" title="Руководители Арх бюро на Смоленке и студии дизайна Эльпасо в кабинете клиента" style="display:block; Width:100%;">
		</div>
	</div>
	<p style="text-align: justify;">
 <b><span style="color: #450e61;">Вместе с архитектурной Мастерской "На Смоленке"&nbsp;компания "Эльпасо"&nbsp;имеет преимущества в статусе официального дилера испанской фабрики Coleccion Alexandra.</span></b><span style="color: #450e61;">&nbsp;</span>Мы&nbsp;представляем в Москве изделия из уникальных&nbsp; коллекций мебели Coleccion Alexandra.&nbsp;В наших экспозициях, в наличии: дизайнерские комоды, мягкая мебель и оригинальные столы фабрики.
	</p>
	<p style="text-align: justify;">
		 Ряд дизайн-проектов выполняем в сотрудничестве с талантливыми архитекторами: <b><span style="color: #450e61;">Еленой Потёмкиной</span></b> - она начинала свою карьеру в нашей компании,<span style="color: #450e61;">&nbsp;</span><b><span style="color: #450e61;">Иваном Пантелеевым&nbsp;</span></b>- истоки наших совместных работ в ОАО "Мосэнерго"&nbsp;и<span style="color: #450e61;">&nbsp;</span><b><span style="color: #450e61;">Александрой Петропавловской</span>&nbsp;</b>-&nbsp;она уже руководитель собственного бюро дизайна и просто умница в дизайне интерьеров. <br>
	</p>
	<div class="row">
		<div style="margin: 20px 0px;" class="coop-img-1">
 <img alt="sotrudnichestvo_17.jpg" src="/upload/medialibrary/f4c/f4cbb08b77738a3f7e70582878440872.jpg" title="В переговорной студии интерьеров Эльпасо">
		</div>
		<div style="margin: 20px 0px;" class="coop-img-2">
 <img alt="b8.jpg" src="/upload/medialibrary/5a5/5a5e8a5c651cfa4ab259e03ab0841f47.jpg" title="Мы открыты к бизнес диалогу с Вами - команда компании Эльпасо">
		</div>
	</div>
	<p style="text-align: justify;">
 <span style="color: #30004a;"><b>Наши партнёры - замечательные мастера и&nbsp;художники - </b></span><b><span style="color: #30004a;">Ольга Гавришева</span></b><span style="color: #30004a;"><b>, </b></span><b><span style="color: #30004a;">Алексей Филатов</span></b><span style="color: #30004a;"><b>, </b></span><b><span style="color: #30004a;">Рима Владюк</span></b><span style="color: #30004a;"><b>&nbsp;и </b></span><b><span style="color: #30004a;">Зинаида Чернышёва</span></b><span style="color: #30004a;"><b>. Мы благодарим Вас всех за сотрудничество!!!&nbsp;</b></span><span style="color: #30004a;"><b><span style="color: #30004a;">Вместе мы выполнили необычные и креативные запросы наших клиентов на базе компании ELPASO, привнесли в новые интерьеры частичку Вашей искры, таланта и души. </span></b></span><br>
 <br>
	</p>
	<p style="text-align: center;">
 <span style="font-size: 14pt;"><span style="color: #450e61;">Компания </span><b><span style="color: #450e61;">ELPASO</span></b><span style="color: #450e61;"> выделяет основные испанские фабрики и&nbsp;&nbsp;является официальными дилером:</span></span><span style="color: #450e61;"> </span>
	</p>
 <span style="color: #450e61;"> </span>
	<p style="text-align: center;">
 <span style="color: #450e61;"> </span><span style="font-size: 14pt;"><b><span style="color: #450e61;">OfiFran, COLECCION ALEXANDRA, LLASS, MARINER, SOHER, ТECNI NOVA, VALENTI, SCHULLER, ARTE ROMERA.&nbsp; </span></b></span><b><span style="color: #450e61;"> </span><br>
 <span style="color: #450e61;"> </span><span style="font-size: 14pt; color: #450e61;"> </span></b><span style="color: #450e61;"> </span><br>
 <span style="color: #450e61;"> </span><span style="font-size: 14pt;"><b><span style="color: #450e61;"> </span></b><span style="color: #450e61;">Рекомендуем и поставляем продукцию итальянских фабрик:</span></span><span style="color: #450e61;"> </span>
	</p>
 <span style="color: #450e61;"> </span>
	<p style="text-align: center;">
 <span style="color: #450e61;"> </span>
	</p>
 <span style="color: #450e61;"> </span>
	<p style="text-align: center;">
 <span style="color: #450e61;"> </span><span style="font-size: 14pt;"><b><span style="color: #450e61;">MASCHERONI, TURRI, FORMITALIA, ReDeco, SMANIA, MARIANI, VISMARA&nbsp;</span></b></span><span style="color: #450e61;"> </span>
	</p>
 <span style="color: #450e61;"> </span>
	<p style="text-align: center;">
 <span style="color: #450e61;"> </span>
	</p>
 <span style="color: #450e61;"> </span>
	<p style="text-align: center;">
 <span style="color: #450e61;"> </span><span style="font-size: 14pt; color: #450e61;">и&nbsp; представляем ещё более семидесяти самых достойных фабрик Италии.&nbsp;</span>
	</p>
	<p style="text-align: center;">
 <span style="font-size: 14pt;"><b> <br>
 </b></span>
	</p>
	<p style="text-align: justify;">
	</p>
	<p style="text-align: justify;">
	</p>
	<p>
 <span style="font-size: 13pt;"><b><span style="color: #42304f;"> </span></b><span style="color: #42304f;">В 2017-ом году компанией Elpaso подписан контракт с владельцем фабрики MBMbiliardi Мариано Маджио. Главным доверенным лицом Кав. Маджио на российском рынке является директор ООО «Эльпасо» Ольга Ефремова. Первую совместную выставку в Крокус-Экспо партнеры провели в год подписания контракта. Об этой первой знаменательной выставке снят фильм. <br>
 <br>
 <a href="http://youtu.be/-YrDhl2qPEo">Исторические кадры</a> в "Крокус-экспо", встречи и контакты двух компаний стали начальным этапом для фундамента большого сотрудничества после обучения сотрудников.<br>
 <br>
		 Премиальные бильярды Вы можете выбрать в нашем <a href="/catalog/bilyardnye-stoly1/">каталоге</a>. Узнайте больше о&nbsp;<a href="/brand/19269/">фабрике MBMbiliardi</a> и возможностях для клиентов в России!<br>
 <br>
		 Мы приглашаем к сотрудничеству профессионалов интерьерной сферы, проектные бюро, архитекторов и дизайнеров. Гарантируем&nbsp;наиболее выгодные условия поставок для Вас во всём, что позволяет наш опыт, для Ваших проектов.</span></span>
	</p>
	<p style="text-align: justify;">
	</p>
	<p style="text-align: justify;">
	</p>
	<p style="text-align: justify;">
	</p>
	<p style="text-align: justify;">
	</p>
	<p style="text-align: justify;">
	</p>
	<p>
	</p>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>