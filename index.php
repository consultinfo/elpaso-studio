<?
$page_index = "Y";
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "интерьеры, Elpaso, Дизайн студия Эльпасо, дизайнерская мебель, MBM Biliardi, Coleccion Alexandra, Shuller, Mariner, Soher, Mugali, Llass, поставки дизайнерской мебели");
$APPLICATION->SetPageProperty("title", "Дизайн студия \"Эльпасо\"");
$APPLICATION->SetPageProperty("description", "Компания Elpaso с 2002 года создаёт интерьеры и ведёт поставки дизайнерской мебели.  Дистрибьютор фабрики MBM Biliardi в России и странах CНГ. Официальный дилер фабрик Coleccion Alexandra, Shuller, Mariner, Soher, Mugali, Llass. Клиенты получают премиум-сервис и условия поставок по цене фабрик.");
$APPLICATION->SetTitle("Дизайн студия \"Эльпасо\"");
?><section id="banner" class="big-banner">
	<div class="container">
		<div class="row">
			<div class="banner-left">
				<div class="owl-banner">
					<?
					CModule::IncludeModule('iblock');

					$arSelect = Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
					$arFilter = Array("IBLOCK_ID" => 16, "SECTION_ID" => 85, "ACTIVE"=>"Y");
					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>15), $arSelect);

					while($ob = $res->GetNextElement()):?>
					<?
					$arFields = $ob->GetFields();
					$arProps = $ob->GetProperties();
					?>

					<div class="item" style="position: relative;">
						<a href="<?=$arProps["LINK"]["VALUE"]?>">
							<div class="text"><?=$arFields["PREVIEW_TEXT"];?></div>
							<img src="<?=CFile::GetPath($arFields["PREVIEW_PICTURE"]);?>" alt="" />
						</a>
					</div>

					<?endwhile;?>
				</div>
			</div>
			<!--<div class="banner-right">
				<?/*
				CModule::IncludeModule('iblock');
				$rsBANNERS = CIBlockElement::GetList (
					Array("SORT"=>"ASC"), // сортировка
					Array("IBLOCK_ID" => 16, "SECTION_ID" => 87), //ID инфоблока
					false,
					Array ("nTopCount" => 2), // количество
					Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_LINK")  //список полей. IBLOCK_ID и ID - обязательны.
				);
				*/?>

				<?/*while($banners = $rsBANNERS-> GetNext()):*/?>

				<a href="<?/*=$banners["PROPERTY_LINK_VALUE"];*/?>">
					<div class="banner-right-block row" style="background-image: url(<?/*=CFile::GetPath($banners["PREVIEW_PICTURE"]);*/?>);">
						<div class="black-bg"></div>
						<h2><?/*=$banners["NAME"];*/?></h2>
						<p><?/*=$banners["PREVIEW_TEXT"];*/?></p>
					</div>
				</a>

				<?/*endwhile;*/?>
			</div>-->
		</div>
	</div>
</section>

<section id="banner-marketing">
    <div class="container">
        <div class="row">
            <div class="banner-marketing">
                <a href="https://elpaso-studio.ru/catalog/bilyardnye-stoly1/filter/brand-is-mbmbiliardi/apply/" class="banner-marketing__logo">
                    <img src="<?=SITE_TEMPLATE_PATH;?>/img/mbmbiliardi.png" alt="">
                </a>
                <div class="banner-marketing__desc">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/include/main/banner_marketing.php"
                        )
                    );?>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="portfolio" class="portfolio-index">
    <div class="container">
        <h3>
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                ".default",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/include/main/portfolio_title.php"
                )
            );?>
		</h3>
        <h2>
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                ".default",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/include/main/portfolio_mintitle.php"
                )
            );?>
        </h2>
        <p>
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                ".default",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/include/main/portfolio_text.php"
                )
            );?>
        </p>
    </div>
     <?$APPLICATION->IncludeComponent(
        "addeo:list",
        ".default",
        array(
            "COMPONENT_TEMPLATE" => ".default",
            "COUNT" => "6",
            "IBLOCK_ID" => "8",
            "IBLOCK_TYPE" => "content",
            "SECTIONS" => array(
                0 => "67",
                1 => "68",
                2 => "69",
                3 => "70",
                4 => "72",
                5 => "73",
            ),
            "COMPOSITE_FRAME_MODE" => "A",
            "COMPOSITE_FRAME_TYPE" => "AUTO"
        ),
        false
    );?>

    <div class="container">
        <a href="/portfolio/">
            <button class="btn-portfolio">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/include/main/portfolio_more.php"
                    )
                );?>
                <img src="/bitrix/templates/addeo/img/arrow-portfolio-right.png" alt="">
            </button>
        </a>
    </div>
</section>

<section id="steps">
	<div class="owl-steps">
		<?
		CModule::IncludeModule('iblock');
		$rsSTEPS = CIBlockElement::GetList (
			Array("SORT"=>"ASC"), // сортировка
			Array("IBLOCK_ID" => 16, "SECTION_ID" => 86), //ID инфоблока
			false,
			Array ("nTopCount" => 5), // количество
			Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_LINK")  //список полей. IBLOCK_ID и ID - обязательны.
		);
		$k="1";
		?>

		<?while($step = $rsSTEPS-> GetNext()):?>
		<?
		$URL[] = $k;
		?>
		<div class="item" data-hash="step-<?=$k;?>" style="background-image: url(<?=CFile::GetPath($step["PREVIEW_PICTURE"]);?>);">
			<p class="steps-more">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/include/main/step_url.php"
					)
				);?>
			</p>
			<h2><?=$step["NAME"]?></h2>
			<p><?=$step["PREVIEW_TEXT"]?></p>
		</div>
		<?$k++;?>
		<?endwhile;?>
	</div>

	<div class="steps-dots">
		<?$count = count($URL);?>
		<?foreach($URL as $url): ?>
		<?if($url < $count):?>
			<a <?if($url==1):?>class="active"<?endif;?> href="#step-<?=$url;?>"><div class="step"><?=$url;?></div></a>
			<div class="steps-line"></div>
		<?else:?>
			<a href="#step-<?=$url;?>"><div class="step"><?=$url;?></div></a>
		<?endif;?>
		<?endforeach;?>
	</div>

	<div class="steps-bottom"></div>
</section>

<section id="hits">
	<div class="container">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/include/bestseller_title_h2.php"
			)
		);?>
		<div class="owl-hits">
			<?
			CModule::IncludeModule('iblock');
			$rsHIT = CIBlockElement::GetList (
				Array("SORT"=>"ASC"), // сортировка
				Array("IBLOCK_ID" => 5, "PROPERTY_HIT_VALUE" => "Да"), //ID инфоблока
				false,
				Array ("nTopCount" => 15), // количество
				Array("IBLOCK_ID", "ID", "IBLOCK_SECTION_ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_HIT")  //список полей. IBLOCK_ID и ID - обязательны.
			);
			?>
			<?while($arProduct = $rsHIT-> GetNext()):?>
			<div class="item">
				<a href="<?=$arProduct["DETAIL_PAGE_URL"]?>">
					<div class="look-book-block">
						<!--<img class="look-book-hover" src="img/trend-like.png" alt="">-->
						<div class="bg-img-div" style="background-image: url(<?=CFile::GetPath($arProduct["PREVIEW_PICTURE"]);?>);"></div>
						<h3><?=$arProduct["NAME"]?></h3>
						<?$get_SECT = CIBlockSection::GetByID($arProduct["IBLOCK_SECTION_ID"]);
						$obRes1 = $get_SECT->GetNext();
						$arProduct["SECTION"] = $obRes1["NAME"];?>
						<p><?=$arProduct["SECTION"]?></p>
						<?
						/*$Price = CPrice::GetBasePrice($arProduct["ID"]);
						$arProduct["PRICE"] = $Price["PRICE"];*/
						$Price = GetCatalogProductPrice($arProduct["ID"], 1);
						$arProduct["PRICE"] = CCurrencyRates::ConvertCurrency($Price["PRICE"], "EUR", "RUB");
						?>
						<?php if (ceil($arProduct["PRICE"]) == 0): ?>
							<h4>Цена по запросу</h4>
						<?php else: ?>
							<h4><?=ceil($arProduct["PRICE"]);?> Р</h4>
						<?php endif; ?>
					</div>
				</a>
			</div>
			<?endwhile;?>
		</div>
	</div>
</section>

<section id="trend">
	<div class="container">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/include/trands_title_h2.php"
			)
		);?>
		<div class="row">
			<?
			CModule::IncludeModule('iblock');
			$rsTREND = CIBlockElement::GetList (
				Array("SORT"=>"ASC", "ID"=>"ASC"), // сортировка
				Array("IBLOCK_ID" => 5, "PROPERTY_TRAND_VALUE" => "Да"), //ID инфоблока
				false,
				Array ("nTopCount" => 10), // количество
				Array("IBLOCK_ID", "ID", "IBLOCK_SECTION_ID", "NAME", "PREVIEW_TEXT" , "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_TRAND")  //список полей. IBLOCK_ID и ID - обязательны.
			);
			$k="1";
			?>

			<?while($trend = $rsTREND-> GetNext()):?>

			<?
			if(($k == "1") || ($k == "6")){
				$big = "yes";
				$t = CFile::ResizeImageGet($trend["PREVIEW_PICTURE"], array("width" => "600", "height" => "300"), BX_RESIZE_IMAGE_EXACT);
				$picture = $t["src"];
			}else{
				$big = "no";
				$picture = CFile::GetPath($trend["PREVIEW_PICTURE"]);
			}
			?>

			<a href="<?=$trend["DETAIL_PAGE_URL"]?>"><div class="trend-block <?if($big == "yes"):?>trend-block-big<?endif;?>">
				<div class="bg-img-div" style="background-image: url(<?=$picture;?>);"></div>
				<h3><?=$trend["NAME"]?></h3>
				<?$get_SECT = CIBlockSection::GetByID($trend["IBLOCK_SECTION_ID"]);
				$obRes1 = $get_SECT->GetNext(); ?>
				<p><?=$obRes1["NAME"];?></p>
				<?
				$Price = CPrice::GetBasePrice($trend["ID"]);
				/*$Price = GetCatalogProductPrice($trend["ID"], 1);*/
				$arProduct["PRICE"] = CCurrencyRates::ConvertCurrency($Price["PRICE"], "EUR", "RUB");

				?>
				<?php if (ceil($arProduct["PRICE"]) == 0): ?>
					<h4>Цена по запросу</h4>
				<?php else: ?>
					<h4><?=ceil($arProduct["PRICE"]);?> Р</h4>
				<?php endif; ?>
			</div></a>
			<?$k++;?>
			<?endwhile;?>
		</div>
		<div class="row">
			<a href="/catalog/">
				<button class="btn-trend">
					<?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/include/main/trend_more.php"
                        )
                    );?>
                    <img src="/bitrix/templates/addeo/img/arrow-btn-trend.png" alt="">
                    <span class="color_white">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            ".default",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => "/include/main/trend_info.php"
                            )
                        );?>
                    </span>
                </button>
            </a>
        </div>
    </div>
</section>

<section id="top">
    <div class="container">
        <h2>
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                ".default",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/include/main/top_title.php"
                )
            );?>
        </h2>
        <div class="row">
            <div class="top-left">
                <div class="row">
                    <div class="ltl">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "main_slider_left_top",
                            Array(
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_TYPE" => "A",
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "N",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "FIELD_CODE" => array("",""),
                                "FILTER_NAME" => "",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "IBLOCK_ID" => "38",
                                "IBLOCK_TYPE" => "main_sliders",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "INCLUDE_SUBSECTIONS" => "N",
                                "MESSAGE_404" => "",
                                "NEWS_COUNT" => "10",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => ".default",
                                "PAGER_TITLE" => "Новости",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "PROPERTY_CODE" => array("LINK",""),
                                "SET_BROWSER_TITLE" => "N",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "N",
                                "SET_META_KEYWORDS" => "N",
                                "SET_STATUS_404" => "N",
                                "SET_TITLE" => "N",
                                "SHOW_404" => "N",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER1" => "DESC",
                                "SORT_ORDER2" => "ASC"
                            )
                        );?>
                    </div>
                    <div class="lb">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "main_slider_left_bottom",
                            Array(
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_TYPE" => "A",
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "N",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "FIELD_CODE" => array("",""),
                                "FILTER_NAME" => "",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "IBLOCK_ID" => "39",
                                "IBLOCK_TYPE" => "main_sliders",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "INCLUDE_SUBSECTIONS" => "N",
                                "MESSAGE_404" => "",
                                "NEWS_COUNT" => "10",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => ".default",
                                "PAGER_TITLE" => "Новости",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "PROPERTY_CODE" => array("LINK",""),
                                "SET_BROWSER_TITLE" => "N",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "N",
                                "SET_META_KEYWORDS" => "N",
                                "SET_STATUS_404" => "N",
                                "SET_TITLE" => "N",
                                "SHOW_404" => "N",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER1" => "DESC",
                                "SORT_ORDER2" => "ASC"
                            )
                        );?>
                    </div>
                </div>
            </div>
            <div class="top-right">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "main_slider_right",
                    Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "N",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => array("",""),
                        "FILTER_NAME" => "",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => "40",
                        "IBLOCK_TYPE" => "main_sliders",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "INCLUDE_SUBSECTIONS" => "N",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "10",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => array("LINK",""),
                        "SET_BROWSER_TITLE" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC"
                    )
                );?>
            </div>
        </div>
    </div>
</section>

<section id="news">
    <div class="container">
        <div class="news-title row">
            <h2>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/include/main/news_title.php"
                    )
                );?>
            </h2>
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                ".default",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/include/main/news_all.php"
                )
            );?>
        </div>
        <div class="owl-news">
            <?
            CModule::IncludeModule('iblock');
            $rsNEWS = CIBlockElement::GetList (
                Array("SORT"=>"ASC", "ID"=>"DESK"), // сортировка
                Array("IBLOCK_ID" => 18), //ID инфоблока
                false,
                Array ("nTopCount" => 10), // количество
                Array("IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "PROPERTY_LINK", "PROPERTY_TYPE")  //список полей. IBLOCK_ID и ID - обязательны.
            );
            ?>
            <?while($news = $rsNEWS-> GetNext()):?>
                <div class="item">
                    <a href="<?=$news["PROPERTY_LINK_VALUE"];?>">
                        <img src="<?=CFile::GetPath($news["PREVIEW_PICTURE"]);?>" alt="">
                        <div class="owl-news-txt">
                            <h3><?=$news["NAME"];?></h3>
                            <p><?=$news["PROPERTY_TYPE_VALUE"];?></p>
                        </div>
                    </a>
                </div>
            <?endwhile;?>
        </div>
        <div class="news-bottom">
            <div class="mobile-a">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/include/main/news_all.php"
                    )
                );?>
            </div>
            <img src="/bitrix/templates/addeo/img/fb-news.png" alt="">
            <p>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/include/main/news_text.php"
                    )
                );?>
            </p>
        </div>
    </div>
</section>

<section id="social">
	<div class="container">
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu",
			"social",
			Array(
				"ALLOW_MULTI_SELECT" => "N",
				"CHILD_MENU_TYPE" => "social",
				"MAX_LEVEL" => "1",
				"MENU_CACHE_GET_VARS" => "",
				"MENU_CACHE_TIME" => "36000000",
				"MENU_CACHE_TYPE" => "Y",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"ROOT_MENU_TYPE" => "social",
				"USE_EXT" => "N"
			),false
		);?>
	</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
