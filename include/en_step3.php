<?$this->setFrameMode(true);?>
<div class="step-block">

<div class="row">
              <div class="step-block-number"><strong>3</strong></div>
              <div class="step-block-txt-right">
                <h2>Design Implementation</h2>
                <h3></h3>
                <p></p><p style="margin: 30px 0 30px;">
	Design Implementation is done by a team of highly skilled professionals with their foreman <a href="/eng/authors/Professionals/">Dmitry Oleynik</a> according to the cost sheet approved by the Client and the contract for construction and finishing works. The architect who created your design supervises personally its implementation. <a href="/eng/authors/arkhitektory/">Svyatoslav Ivanchenko</a>, an experienced and accomplished architect, oversees design projects for buildings, houses, and flats. Materials, doors, furniture, lighting, and textile deliveries are managed by Elpaso’s leading experts with <a href="/eng/authors/elpaso/">Olga Efremova</a> as their head. We guarantee the best products from trustworthy manufacturers and provide quality control as stipulated in the contract.
</p><p></p>
              </div>
            </div>

<div class="row">
	<div style="margin: 20px 0px;" class="col-md-12">
 <img alt="1459977188_2486_zhk_dominion_1j.jpg" src="/upload/medialibrary/fe3/fe3d6a92a8fc1dc835dc4c2296f2fb7a.jpg" style="width:100%;" title="ЖК &quot;Доминион&quot;">
	</div>
	<div style="margin: 20px 0px;" class="col-md-12">
 <img alt="d1925e9aea8feb32cba70dc3ec8f6d1c.jpg" src="/upload/medialibrary/bac/bac90a3fe029bd7ad563f0b79a5e49a7.jpg" style="width:100%;" title="ЖК &quot;Мосфильмовский, 88&quot;">
	</div>
	<div style="margin: 20px 0px;" class="col-md-12">
 <img alt="logo.jpg" src="/upload/medialibrary/a9b/a9baf493bd4f47a6d87a16893641af6a.jpg" style="width:100%;" title="ЖК &quot;Новоясеневский&quot;">
	</div>
	<div style="margin: 20px 0px;" class="col-md-12">
 <img alt="Q_GlpyxxKyI.jpg" src="/upload/medialibrary/f89/f896f865cb5a11db394d0f48aa8c6751.jpg" style="width:100%;" title="ЖК &quot;Некрасовка Парк&quot;">
	</div>
</div>
<p style="margin: 30px 0 20px;">
 <a href="http://www.krost.ru/privilege/"><img src="/upload/medialibrary/989/989ff88d9190154fea59b30a7169993d.jpg" style="display:block; width:40%;margin:10px;" title="Крост" align="left"></a><br>Elpaso completed projects in major housing estates built by Moscow’s leading property developers, for example, housing estates Dominion, Mosfilmovsky, Solov’inaya Roscha (in Kurkino), Novoyasenevsky, Triumph Palace, and Nekrasovka-park. Elpaso is a part of the “GOLDEN RATIO” programme for the owners of real estate built by the KROST concern. As such, we have best offers for materials, furniture, and lighting. We offer you direct shipping and manufacturer’s suggested price.</span>
</p>
<p style="margin: 20px 0 30px;">
 <span style="text-align: justify;">Elpaso is an authorized dealer and distributor of world leading manufacturers and provide a full range of services, including shipping, loading/unloading, assembly, and warranty repairs. When you get an Elpaso interior, you don’t have to pay for a costly rent of commercial property. We have our own spaces; that is why you only pay for the goods and delivery plus your personal discount. Thanks to experts you will avoid useless corrections and will save time, trouble, and money. Elpaso was born in 2005 and since that moment you can find it at 36/2 Zelenodolskaya Street, Moscow. If necessary, we can sign personal guarantees that insure deliveries. Our clients get their projects done and ready to move in.</span>
</p>
<div class="row">
	<div style="margin: 20px 0px;" class="col-md-12">
 <img alt="DSC6316_1.jpg" src="/upload/medialibrary/16c/16cdfb0525fb78dc09c2f0cf7c10d5ec.jpg" style="width:100%;">
	</div>
</div>
<div class="row">
	<div style="margin: 20px 0px;" class="col-md-6">
 <img alt="_DSC3360_ как смарт-объект-1.jpg" src="/upload/medialibrary/897/897b9478ffebc13872dc0e171d229009.jpg" style="width:100%;">
	</div>
	<div style="margin: 20px 0px;" class="col-md-6">
 <img alt="_DSC3237_ как смарт-объект-1.jpg" src="/upload/medialibrary/a9e/a9edb167cfe6ee5ab3a9ed8b73a2bea5.jpg" style="width:100%;">
	</div>
</div>
<div class="row">
	<div style="margin: 20px 0px;" class="col-md-12">
 <img alt="DSC3237.jpg" src="/upload/medialibrary/057/057b9ef811709faf2d87db383e6796be.jpg" style="width:100%;">
	</div>
</div>
<br>
<a href="/eng/step/4/"><button class="btn-next-step">The next stage is <span>Final Review</span></button></a>
</div><br>
