<?$this->setFrameMode(true);?>
<div class="step-block">


<div class="row">
          <div class="step-block-number"><strong>1</strong></div>
          <div class="step-block-txt-right">
            <h2>Schematic Design</h2>
            <h3></h3>
            <p></p>
            <p style="margin: 30px 0 30px;">
Schematic Design reflects the client’s wishes and helps to understand what image and style the space will have.
            </p>
            <p></p>
          </div>
        </div>
    <div class="row">
    <div style="margin: 20px 0px;" class="col-md-6">
 <img alt="18.jpg" src="/upload/medialibrary/394/394e250139609a110238549e22d46106.jpg" title="Schematic Design" style="width:100%;">
    </div>
    <div style="margin: 20px 0px;" class="col-md-6">
 <img alt="19.jpg" src="/upload/medialibrary/be1/be1ba2adad2da619b1059eb14b91c105.jpg" title="Schematic Design" style="width:100%;">
    </div>
</div>
<h2 style="margin: 30px 0 15px;">Schematic design includes</h2>
<p style="margin-bottom: 45px;">
1. Terms of Reference.
<br>
2. Floor plans with measurements and utility mains.
<br>
3. Space planning options according to the approved floor plans.
<br>
4. Schematic furniture layout.
</p>
<div class="row">
 <img alt="3232.jpg" src="/upload/medialibrary/d42/d428cb74a808378d6921f1826ad0e598.jpg" style="width:100%;" title="Project plan">
</div>
<p style="margin: 30px 0 30px;">
     5. Floor plan detailing the flooring.<br>
     6. Reflected ceiling plan without specs. <br>
     7. Plumbing fixtures locations without plumbing layout. <br>
     8. 3D models of two or three rooms to get an idea of the design.
</p>
<div class="row">
    <div style="margin: 20px 0px;" class="col-md-6">
 <img alt="20.jpg" src="/upload/medialibrary/c42/c42e0cf34db1ca6b0b9aaa78c72f724d.jpg" title="Schematic Design" style="width:100%;">
    </div>
    <div style="margin: 20px 0px;" class="col-md-6">
 <img alt="21.jpg" src="/upload/medialibrary/d0b/d0b64837928adfb38aff6f08ce442be7.jpg" title="Schematic Design" style="width:100%;">
    </div>
</div>
<p style="margin: 30px 0 30px;">
     9. Selection of finishes, doors, furniture, lighting, plumbing fixtures, flooring, wall and ceiling coverings.
</p>
<h3>Price:</h3>
<p>
     The price starts at 1,000 roubles for square metre. <br>
     Sketch designs for offices, malls or large spaces are priced separately.
</p>
<br>
<a href="/eng/step/2/"><button class="btn-next-step">The next stage is <span>Design Development</span></button></a>

</div>
