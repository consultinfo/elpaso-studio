<?$this->setFrameMode(true);?>
<div class="step-block">


    <div class="row">
                  <div class="step-block-number"><strong>2</strong></div>
                  <div class="step-block-txt-right">
                    <h2>Design Development</h2>
                    <h3></h3>
                    <p></p><p style="text-align: justify;">
    	 Design Development is based on the SD (Schematic Design). DD includes drawings for finishing works and the approved schematic designed. After the Client has signed the documents of the architectural album (it includes the sketches of partition walls to de demolished and built, flooring, wall coverings, utility lines and electrical layout) a constructor team proceeds with work. The foreman takes responsibility for complying with the contract and schedule.
    </p><p></p>
                  </div>
                </div>
<div class="row">
	<div style="margin: 20px 0px;" class="col-md-6">
 <img alt="Robproekt_1.jpg" src="/upload/medialibrary/25a/25a744da4f42aa0c2059ff7132d07358.jpg" style="width:100%;">
	</div>
	<div style="margin: 20px 0px;" class="col-md-6">
 <img alt="Robproekt_2.jpg" src="/upload/medialibrary/631/631b42444357a8fcec73eee3e9d56b6e.jpg" style="width:100%;">
	</div>
</div>
<p style="margin-bottom: 25px; text-align: justify; width:100%;">
	If necessary Elpaso ELABORATES and GETS APPROVED SPECIFIC electrical layout DOCUMENTATION and obtains lab results. If there is a need for remodelling we sign a contract for technical design estimation with Moscow Research and Design Institute of Typology and Experimental Design and necessary documents and building journals with Moscow State Housing Inspectorate. We also work on HVAC (Heating, ventilation, and air conditioning) and WSS (Water supply and sanitation) and get the approval of maintenance companies.
</p>
<div class="row">
	<div style="margin: 20px 0px;" class="col-md-6">
 <img alt="Robproekt_3.jpg" src="/upload/medialibrary/2bb/2bb30f79445d4ff443f22634322eab9f.jpg" style="width:100%;">
	</div>
	<div style="margin: 20px 0px;" class="col-md-6">
 <img alt="Robproekt_4.jpg" src="/upload/medialibrary/31a/31ae26cb81cac8488523f3b85f7fbba5.jpg" style="width:100%;" title="Robproekt_4.jpg">
	</div>
</div>
<p style="margin-bottom: 25px;">
1. Detailed design project with explanatory notes.<br>
2.  Floor plans with measurements and utility mains.<br>
3. Plan of demolishing partition walls and utility mains.<br>
4. Plan of building new partition walls with window and door openings with size and material specs.<br>
5. Furniture layout. <br>
6. Plumbing fixtures locations with plumbing layout.<br>
7. Reflected ceiling plan with specs of materials and the ceiling itself (the number of drawings depends on the complexity of the ceiling). <br>
8. Lighting layout with specs of switches locations. <br>
9. Specifications of lighting, such as its type, wattage, etc.<br>
10. Plan of socket outlets and electrical leads locations with size indication.<br>
11.  Radiant floor layout with a control panel.<br>
12. Floor plan with specs on its level, flooring, pattern and measurements. Floor cross section with indications of each layer (the number of drawings depends on the complexity of the floor).<br>
13. Legend that describes the flooring, its surface and the article of material chosen.<br>
14. Walls cross section views and plans with decorative elements and their specifications (the number of drawings depends on the number of decorative elements).<br>
15. Wall plans with tiling layout with specifications of its size, article and surface.<br>
16. Drawing of ordered items (if necessary).<br>
17. Finishes progress report.
</p>
<div class="row">
	<div style="margin: 20px 0px;" class="col-md-12">
 <img src="/upload/medialibrary/015/015e2511faa093fbbfb4f512df02e7c7.jpg" style="width:100%;">
	</div>
	<div style="margin: 20px 0px;" class="col-md-12">
 <img src="/upload/medialibrary/2f5/2f50271f32b31313fbd90ccc30b15fab.jpg" style="width:100%;">
	</div>
	<div style="margin: 20px 0px;" class="col-md-12">
 <img src="/upload/medialibrary/760/76085a79deaeb797fbd7f98a85cf9d02.jpg" style="width:100%;">
	</div>
</div>
<p style="margin-bottom: 45px;">
	We use ArchiCad and AutoCAD for design development documentation. Visualization and interior modelling are made in 3D Max.
</p>
<br>
<a href="/eng/step/3/"><button class="btn-next-step">The next stage is <span>Design Implementation</span></button></a>
</div>
