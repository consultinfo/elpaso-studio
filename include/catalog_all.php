<?CModule::IncludeModule('iblock');?>
<?$this->setFrameMode(true);?>

<?
$SectionFilter = Array("IBLOCK_ID" => 5, "DEPTH_LEVEL" => 1);
$getSection = CIBlockSection::GetList(Array("SORT"=>"ASC"), $SectionFilter, true, false, false);
?>

<?while($section = $getSection-> GetNext()):?>

	<div class="catalog-all-block row">
		<div class="catalog-all-block-title" style="background-image: url(<?=CFile::GetPath($section['DETAIL_PICTURE'])?>)">
        	<a href="<?=$section["SECTION_PAGE_URL"]?>"><h2><?=$section["NAME"]?></h2></a>
        </div>

		<?$raz = $section["RIGHT_MARGIN"] - $section["LEFT_MARGIN"];?>
		<?if($raz != "1"):?>
			<?getMenuSect(5, 2, $section["LEFT_MARGIN"], $section["RIGHT_MARGIN"], $section["ID"]);?>
		<?endif;?>

	</div>
<?endwhile;?>

<?
function getMenuSect($IBLOCK_ID, $DEPTH_LEVEL, $LEFT_MARGIN, $RIGHT_MARGIN, $ID){
		$SectionFilter =Array("IBLOCK_ID" => $IBLOCK_ID, "DEPTH_LEVEL" => $DEPTH_LEVEL, ">LEFT_MARGIN" => $LEFT_MARGIN, "<RIGHT_MARGIN" => $RIGHT_MARGIN);
		$getSection = CIBlockSection::GetList(Array("SORT"=>"ASC"), $SectionFilter, true, false, false);
		while($section = $getSection-> GetNext()){
			echo "<div class=\"catalog-all-block-ul\">";
            	echo "<a href=\"".$section["SECTION_PAGE_URL"]."\"><h3>".$section["NAME"]."</h3></a>";
				$raz = $section["RIGHT_MARGIN"] - $section["LEFT_MARGIN"];
				if($raz != "1"){
					$DEPTH_LEVEL = $DEPTH_LEVEL +1;
					getMenuSect2(5, $DEPTH_LEVEL, $section["LEFT_MARGIN"], $section["RIGHT_MARGIN"], $section["ID"]);
					$DEPTH_LEVEL = $DEPTH_LEVEL -1;
				}
			echo "</div>";
		}
}

function getMenuSect2($IBLOCK_ID, $DEPTH_LEVEL, $LEFT_MARGIN, $RIGHT_MARGIN, $ID){
		echo "<ul>";
		$SectionFilter =Array("IBLOCK_ID" => $IBLOCK_ID, "DEPTH_LEVEL" => $DEPTH_LEVEL, ">LEFT_MARGIN" => $LEFT_MARGIN, "<RIGHT_MARGIN" => $RIGHT_MARGIN);
		$getSection = CIBlockSection::GetList(Array("SORT"=>"ASC"), $SectionFilter, true, false, false);
		while($section = $getSection-> GetNext()){?>
			<li><a href="<?=$section["SECTION_PAGE_URL"]?>"><?=$section["NAME"]?></a></li>
		<?}
		echo "</ul>";
}
?>
