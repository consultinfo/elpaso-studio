<?$this->setFrameMode(true);?> <b><span style="font-size: 16pt; color: #450e61;">"В одиночку мы так мало можем сделать.&nbsp;</span></b><b><span style="font-size: 16pt; color: #450e61;">Вместе мы способны свернуть горы."</span></b>
<p>
</p>
<p style="text-align: center;">
 <img width="100%" alt="gori_2.jpg" src="/upload/medialibrary/fec/fecee87c9ab72c6b2484a09bbda4edcd.jpg" style="width:100%; margin:5px 0;" title="К вершинам стиля"> <br>
</p>
<p style="text-align: justify;">
 <b><span style="font-size: 14pt;">Команда компании Elpaso предлагает сотрудничество:</span></b>
</p>
<p style="text-align: left;">
 <br>
	 ⚪ Профессионалам интерьерной сферы, архитекторам и дизайнерам <br>
	 ⚪ Строительным компаниям и управляющим новостроек<br>
	 ⚪ Отделам проектирования и снабжения<br>
	 ⚪ Специалистам мира бильярдов.<br>
	 ⚪ Департаментам закупок государственных корпораций.<br>
	 ⚪ Отелям, домам отдыха и SPA-комплексам.<br>
 <br>
</p>
<p style="text-align: left;">
	 Еlpaso располагает эксклюзивными условиями поставок по испанской мебели для общественных и частных интерьеров. <br>
 <br>
	 С 2017-ого года на основе контракта с итальянской фабрикой MBMbiliardi мы ведём дистрибьюцию дизайнерских бильярдов в России, странах СНГ, Украине и Грузии. <br>
 <br>
</p>
<p style="text-align: center;">
 <span style="color: #4b0048;"> <b><span style="font-size: 14pt;">Приглашаем Вас на партию к сотрудничеству! </span></b></span><br>
 <span style="font-size: 14pt;"> </span><b><span style="font-size: 14pt;"> </span><span style="color: #4b0048; font-size: 14pt;">
	Наши возможности по реализации дизайн-проектов и бильярдных объединяют интересы компаний. </span></b><br>
 <br>
</p>
<p style="text-align: left;">
	 Выполняем поставки на основе конкурсных торгов и котировок. Компания "Elpaso" акредитована на ведущих площадках государственных и аукционных закупок России.<br>
 <br>
	 Располагаем опытом комплектации отелей: <br>
	 ⚪ ведомственные гостиницы ОАО "Мосэнерго", <br>
	 ⚪ гостиница "Замок" в р-не Лазаревское, Головинка (Сочи) <br>
	 ⚪VIP- и бизнес залы аэропорта г. Сочи/Адлер (меблировка до сих пор актуальна и служит с 2015 года) <br>
 <br>
	 Нам доверяют сильнейшие специалисты сферы снабжения: <br>
	 ⚪ Владельцы отелей, коттеджных посёлков и курортных комплексов.<br>
	 ⚪ ГК "Ростех", ЦБРФ и ОАО "Мосэнерго".<br>
	 ⚪ Управы и Префектуры Москвы.<br>
 <br>
	 Для создания офисных интерьеров выделяем наш проект <a href="http://office-cabinet.ru">Office-cabinet</a>. Это универсальная площадка для оснащения делового пространства, где Вы можете выбрать мебель для бизнеса европейского класса:<br>
 <br>
	 ⚪ Кабинеты руководителей <br>
	 ⚪ Переговорные <br>
	 ⚪ Зоны рецепшн <br>
	 ⚪ Офисные кресла <br>
	 ⚪ Рабочие места персонала <br>
	 ⚪ Контрактную мебель на основе государственных заказов<br>
 <br>
 <img alt="Office-cabinet" src="/upload/medialibrary/824/824560802c7856cee4317b253969b06e.jpg" style="width:100%; margin:5px 0;" title="Office-cabinet"> <br>
 <br>
	 Мы предлагаем сервис:<br>
	 ⚪ по меблировке (корпусная и мягкая мебель, в том числе по индивидуальным проектам),<br>
	 ⚪ декору (зеркала, часы, картины, барельефы, арт-объекты, мозаичные панно и др.) и освещению интерьеров (люстры, бра, торшеры, точечные и трековые светильники).<br>
	 ⚪ ведём поставки мебели на основе прямых контрактов с фабриками Испании, Италии, Голландии, Франции, Германии и России.<br>
 <br>
	 За долгие годы работы с фабриками-изготовителями мебели, дверей и светильников, мы не допустили ни одного возврата по рекламации и ни одной «острой» ситуации в работе с Заказчиками. Это результат грамотной работы наших юристов по составлению и ведению договоров, а также - регулярному контролю финансовой надёжности контрагентов и подрядчиков.
</p>
<p>
</p>
 <a class="btn-brand-all" style="margin-top: 20px;" href="/Elpaso Presentation.doc">Письмо-презентация</a>